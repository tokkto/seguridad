package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Provincia.
 */
@Entity
@Table(name = "sitb_distrito", schema="SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Distrito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "v_coddep")
    private String vCoddep;
    
    @Column(name = "v_codpro")
    private String vCodpro;

    @Id
    @Column(name = "v_coddis")
    private String vCoddis;

    @Column(name = "v_desdis", length = 50)
    private String vDesdis;

    @Column(name = "v_abrdis", length = 3)
    private String vAbrdis;

    @Column(name = "v_codreg", length = 2)
    private String vCodreg;

    @Column(name = "v_codzon", length = 2)
    private String vCodzon;

    @Size(max = 2)
    @Column(name = "v_coddepren", length = 2)
    private String vCoddepren;

    @Size(max = 2)
    @Column(name = "v_codproren", length = 2)
    private String vCodproren;

    @Size(max = 2)
    @Column(name = "v_coddisren", length = 2)
    private String vCoddisren;

    @Column(name = "v_flgact", length = 1)
    private String vFlgact;

    @ManyToOne
    private Provincia provincia;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public String getVCoddep() {
        return vCoddep;
    }
    public void setVCoddep(String vCoddep) {
        this.vCoddep = vCoddep;
    }

    public String getVCodpro() {
        return vCodpro;
    }
    public void setVCodpro(String vCodpro) {
        this.vCodpro = vCodpro;
    }

    public String getVCoddis() {
        return vCoddis;
    }
    public void setVCoddis(String vCoddis) {
        this.vCoddis = vCoddis;
    }

    public String getVDesdis() {
        return vDesdis;
    }
    public void setVDesdis(String vDesdis) {
        this.vDesdis = vDesdis;
    }

    public String getVAbrdis() {
        return vAbrdis;
    }
    public void setVAbrdis(String vAbrdis) {
        this.vAbrdis = vAbrdis;
    }

    public String getVCodreg() {
        return vCodreg;
    }
    public void setVCodreg(String vCodreg) {
        this.vCodreg = vCodreg;
    }

    public String getVCodzon() {
        return vCodzon;
    }
    public void setVCodzon(String vCodzon) {
        this.vCodzon = vCodzon;
    }

    public String getVCoddepren() {
        return vCoddepren;
    }
    public void setVCoddepren(String vCoddepren) {
        this.vCoddepren = vCoddepren;
    }

    public String getVCodproren() {
        return vCodproren;
    }
    public void setVCodproren(String vCodproren) {
        this.vCodproren = vCodproren;
    }

    public String getVCoddisren() {
        return vCoddisren;
    }
    public void setVCoddisren(String vCoddisren) {
        this.vCoddisren = vCoddisren;
    }

    public String getVFlgact() {
        return vFlgact;
    }
    public void setVFlgact(String vFlgact) {
        this.vFlgact = vFlgact;
    }

    public Provincia getProvincia() {
        return provincia;
    }
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Distrito distrito = (Distrito) o;
        if (distrito.getVCoddis() == null || getVCoddis() == null) {
            return false;
        }
        return Objects.equals(getVCoddis(), distrito.getVCoddis());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getVCoddis());
    }

    @Override
    public String toString() {
        return "Distrito" +
            "id=" + getVCoddis() +
            ", descripcion='" + getVDesdis() + "'" +
            ", codigopren='" + getVCoddisren() + "'" +
            ", flag='" + getVFlgact() + "'" +
            "}";
    }
}
