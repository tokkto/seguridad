package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ModuloEntidad.
 */
@Entity
@Table(name = "sgl_tm_modulo_entidad")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "moduloentidad")
public class ModuloEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @ManyToOne
    private Modulo modulo;

    @ManyToOne
    private Entidad entidad;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public ModuloEntidad varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public ModuloEntidad datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public ModuloEntidad numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public ModuloEntidad modulo(Modulo modulo) {
        this.modulo = modulo;
        return this;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public ModuloEntidad entidad(Entidad entidad) {
        this.entidad = entidad;
        return this;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModuloEntidad moduloEntidad = (ModuloEntidad) o;
        if (moduloEntidad.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), moduloEntidad.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModuloEntidad{" +
            "id=" + getId() +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
