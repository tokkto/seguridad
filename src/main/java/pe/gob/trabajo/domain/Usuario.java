package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Usuario.
 */
@Entity
@Table(name = "sgl_tm_usuario")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usuario")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_usuario", length = 100)
    private String varNomUsuario;

    @Size(max = 100)
    @Column(name = "var_login_usuario", length = 100)
    private String varLoginUsuario;

    @Size(max = 100)
    @Column(name = "var_inicial_usuario", length = 100)
    private String varInicialUsuario;

    @Column(name = "num_est_usuario")
    private Integer numEstUsuario;

    @Size(max = 300)
    @Column(name = "var_motiv_usuario", length = 300)
    private String varMotivUsuario;

    @Size(max = 300)
    @Column(name = "var_doc_contacto", length = 300)
    private String varDocContacto;

    @Size(max = 300)
    @Column(name = "var_nom_contacto", length = 300)
    private String varNomContacto;

    @Size(max = 300)
    @Column(name = "var_ref_contacto", length = 300)
    private String varRefContacto;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @Size(max = 20)
    @Column(name = "var_direccion_ip", length = 20)
    private String varDireccionIp;

    @Size(max = 50)
    @Column(name = "var_mac_address", length = 50)
    private String varMacAddress;

    @Size(max = 20)
    @Column(name = "var_nombre_pc", length = 20)
    private String varNombrePc;

    @Size(max = 150)
    @Column(name = "var_cod_seguridad", length = 150)
    private String varCodSeguridad;

    @Column(name = "dat_fec_termino")
    private LocalDate datFecTermino;

    @Column(name = "var_pto_control")
    private Integer varPtoControl;

    @ManyToOne
    private TipoUsuario tipoUsuario;

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UsuarioGrupo> usuarioGrupos = new HashSet<>();

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UsuarioHorario> usuarioHorarios = new HashSet<>();

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UsuPer> usuPers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomUsuario() {
        return varNomUsuario;
    }

    public Usuario varNomUsuario(String varNomUsuario) {
        this.varNomUsuario = varNomUsuario;
        return this;
    }

    public void setVarNomUsuario(String varNomUsuario) {
        this.varNomUsuario = varNomUsuario;
    }

    public String getVarLoginUsuario() {
        return varLoginUsuario;
    }

    public Usuario varLoginUsuario(String varLoginUsuario) {
        this.varLoginUsuario = varLoginUsuario;
        return this;
    }

    public void setVarLoginUsuario(String varLoginUsuario) {
        this.varLoginUsuario = varLoginUsuario;
    }

    public String getVarInicialUsuario() {
        return varInicialUsuario;
    }

    public Usuario varInicialUsuario(String varInicialUsuario) {
        this.varInicialUsuario = varInicialUsuario;
        return this;
    }

    public void setVarInicialUsuario(String varInicialUsuario) {
        this.varInicialUsuario = varInicialUsuario;
    }

    public Integer getNumEstUsuario() {
        return numEstUsuario;
    }

    public Usuario numEstUsuario(Integer numEstUsuario) {
        this.numEstUsuario = numEstUsuario;
        return this;
    }

    public void setNumEstUsuario(Integer numEstUsuario) {
        this.numEstUsuario = numEstUsuario;
    }

    public String getVarMotivUsuario() {
        return varMotivUsuario;
    }

    public Usuario varMotivUsuario(String varMotivUsuario) {
        this.varMotivUsuario = varMotivUsuario;
        return this;
    }

    public void setVarMotivUsuario(String varMotivUsuario) {
        this.varMotivUsuario = varMotivUsuario;
    }

    public String getVarDocContacto() {
        return varDocContacto;
    }

    public Usuario varDocContacto(String varDocContacto) {
        this.varDocContacto = varDocContacto;
        return this;
    }

    public void setVarDocContacto(String varDocContacto) {
        this.varDocContacto = varDocContacto;
    }

    public String getVarNomContacto() {
        return varNomContacto;
    }

    public Usuario varNomContacto(String varNomContacto) {
        this.varNomContacto = varNomContacto;
        return this;
    }

    public void setVarNomContacto(String varNomContacto) {
        this.varNomContacto = varNomContacto;
    }

    public String getVarRefContacto() {
        return varRefContacto;
    }

    public Usuario varRefContacto(String varRefContacto) {
        this.varRefContacto = varRefContacto;
        return this;
    }

    public void setVarRefContacto(String varRefContacto) {
        this.varRefContacto = varRefContacto;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Usuario varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Usuario datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Usuario numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public String getVarDireccionIp() {
        return varDireccionIp;
    }

    public Usuario varDireccionIp(String varDireccionIp) {
        this.varDireccionIp = varDireccionIp;
        return this;
    }

    public void setVarDireccionIp(String varDireccionIp) {
        this.varDireccionIp = varDireccionIp;
    }

    public String getVarMacAddress() {
        return varMacAddress;
    }

    public Usuario varMacAddress(String varMacAddress) {
        this.varMacAddress = varMacAddress;
        return this;
    }

    public void setVarMacAddress(String varMacAddress) {
        this.varMacAddress = varMacAddress;
    }

    public String getVarNombrePc() {
        return varNombrePc;
    }

    public Usuario varNombrePc(String varNombrePc) {
        this.varNombrePc = varNombrePc;
        return this;
    }

    public void setVarNombrePc(String varNombrePc) {
        this.varNombrePc = varNombrePc;
    }

    public String getVarCodSeguridad() {
        return varCodSeguridad;
    }

    public Usuario varCodSeguridad(String varCodSeguridad) {
        this.varCodSeguridad = varCodSeguridad;
        return this;
    }

    public void setVarCodSeguridad(String varCodSeguridad) {
        this.varCodSeguridad = varCodSeguridad;
    }

    public LocalDate getDatFecTermino() {
        return datFecTermino;
    }

    public Usuario datFecTermino(LocalDate datFecTermino) {
        this.datFecTermino = datFecTermino;
        return this;
    }

    public void setDatFecTermino(LocalDate datFecTermino) {
        this.datFecTermino = datFecTermino;
    }

    public Integer getVarPtoControl() {
        return varPtoControl;
    }

    public Usuario varPtoControl(Integer varPtoControl) {
        this.varPtoControl = varPtoControl;
        return this;
    }

    public void setVarPtoControl(Integer varPtoControl) {
        this.varPtoControl = varPtoControl;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public Usuario tipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
        return this;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Set<UsuarioGrupo> getUsuarioGrupos() {
        return usuarioGrupos;
    }

    public Usuario usuarioGrupos(Set<UsuarioGrupo> usuarioGrupos) {
        this.usuarioGrupos = usuarioGrupos;
        return this;
    }

    public Usuario addUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        this.usuarioGrupos.add(usuarioGrupo);
        usuarioGrupo.setUsuario(this);
        return this;
    }

    public Usuario removeUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        this.usuarioGrupos.remove(usuarioGrupo);
        usuarioGrupo.setUsuario(null);
        return this;
    }

    public void setUsuarioGrupos(Set<UsuarioGrupo> usuarioGrupos) {
        this.usuarioGrupos = usuarioGrupos;
    }

    public Set<UsuarioHorario> getUsuarioHorarios() {
        return usuarioHorarios;
    }

    public Usuario usuarioHorarios(Set<UsuarioHorario> usuarioHorarios) {
        this.usuarioHorarios = usuarioHorarios;
        return this;
    }

    public Usuario addUsuarioHorario(UsuarioHorario usuarioHorario) {
        this.usuarioHorarios.add(usuarioHorario);
        usuarioHorario.setUsuario(this);
        return this;
    }

    public Usuario removeUsuarioHorario(UsuarioHorario usuarioHorario) {
        this.usuarioHorarios.remove(usuarioHorario);
        usuarioHorario.setUsuario(null);
        return this;
    }

    public void setUsuarioHorarios(Set<UsuarioHorario> usuarioHorarios) {
        this.usuarioHorarios = usuarioHorarios;
    }

    public Set<UsuPer> getUsuPers() {
        return usuPers;
    }

    public Usuario usuPers(Set<UsuPer> usuPers) {
        this.usuPers = usuPers;
        return this;
    }

    public Usuario addUsuPer(UsuPer usuPer) {
        this.usuPers.add(usuPer);
        usuPer.setUsuario(this);
        return this;
    }

    public Usuario removeUsuPer(UsuPer usuPer) {
        this.usuPers.remove(usuPer);
        usuPer.setUsuario(null);
        return this;
    }

    public void setUsuPers(Set<UsuPer> usuPers) {
        this.usuPers = usuPers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Usuario usuario = (Usuario) o;
        if (usuario.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usuario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Usuario{" +
            "id=" + getId() +
            ", varNomUsuario='" + getVarNomUsuario() + "'" +
            ", varLoginUsuario='" + getVarLoginUsuario() + "'" +
            ", varInicialUsuario='" + getVarInicialUsuario() + "'" +
            ", numEstUsuario='" + getNumEstUsuario() + "'" +
            ", varMotivUsuario='" + getVarMotivUsuario() + "'" +
            ", varDocContacto='" + getVarDocContacto() + "'" +
            ", varNomContacto='" + getVarNomContacto() + "'" +
            ", varRefContacto='" + getVarRefContacto() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            ", varDireccionIp='" + getVarDireccionIp() + "'" +
            ", varMacAddress='" + getVarMacAddress() + "'" +
            ", varNombrePc='" + getVarNombrePc() + "'" +
            ", varCodSeguridad='" + getVarCodSeguridad() + "'" +
            ", datFecTermino='" + getDatFecTermino() + "'" +
            ", varPtoControl='" + getVarPtoControl() + "'" +
            "}";
    }
}
