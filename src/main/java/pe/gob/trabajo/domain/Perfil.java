package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Perfil.
 */
@Entity
@Table(name = "sgl_tm_perfil")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "perfil")
public class Perfil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_perfil", length = 100)
    private String varNomPerfil;

    @Size(max = 300)
    @Column(name = "var_desc_perfil", length = 300)
    private String varDescPerfil;

    @Column(name = "num_est_perfil")
    private Integer numEstPerfil;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @ManyToOne
    private Modulo modulo;

    @OneToMany(mappedBy = "perfil")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UsuPer> usuPers = new HashSet<>();

    @OneToMany(mappedBy = "perfil")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MenuPerfil> menuPerfils = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomPerfil() {
        return varNomPerfil;
    }

    public Perfil varNomPerfil(String varNomPerfil) {
        this.varNomPerfil = varNomPerfil;
        return this;
    }

    public void setVarNomPerfil(String varNomPerfil) {
        this.varNomPerfil = varNomPerfil;
    }

    public String getVarDescPerfil() {
        return varDescPerfil;
    }

    public Perfil varDescPerfil(String varDescPerfil) {
        this.varDescPerfil = varDescPerfil;
        return this;
    }

    public void setVarDescPerfil(String varDescPerfil) {
        this.varDescPerfil = varDescPerfil;
    }

    public Integer getNumEstPerfil() {
        return numEstPerfil;
    }

    public Perfil numEstPerfil(Integer numEstPerfil) {
        this.numEstPerfil = numEstPerfil;
        return this;
    }

    public void setNumEstPerfil(Integer numEstPerfil) {
        this.numEstPerfil = numEstPerfil;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Perfil varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Perfil datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Perfil numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public Perfil modulo(Modulo modulo) {
        this.modulo = modulo;
        return this;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public Set<UsuPer> getUsuPers() {
        return usuPers;
    }

    public Perfil usuPers(Set<UsuPer> usuPers) {
        this.usuPers = usuPers;
        return this;
    }

    public Perfil addUsuPer(UsuPer usuPer) {
        this.usuPers.add(usuPer);
        usuPer.setPerfil(this);
        return this;
    }

    public Perfil removeUsuPer(UsuPer usuPer) {
        this.usuPers.remove(usuPer);
        usuPer.setPerfil(null);
        return this;
    }

    public void setUsuPers(Set<UsuPer> usuPers) {
        this.usuPers = usuPers;
    }

    public Set<MenuPerfil> getMenuPerfils() {
        return menuPerfils;
    }

    public Perfil menuPerfils(Set<MenuPerfil> menuPerfils) {
        this.menuPerfils = menuPerfils;
        return this;
    }

    public Perfil addMenuPerfil(MenuPerfil menuPerfil) {
        this.menuPerfils.add(menuPerfil);
        menuPerfil.setPerfil(this);
        return this;
    }

    public Perfil removeMenuPerfil(MenuPerfil menuPerfil) {
        this.menuPerfils.remove(menuPerfil);
        menuPerfil.setPerfil(null);
        return this;
    }

    public void setMenuPerfils(Set<MenuPerfil> menuPerfils) {
        this.menuPerfils = menuPerfils;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Perfil perfil = (Perfil) o;
        if (perfil.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), perfil.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Perfil{" +
            "id=" + getId() +
            ", varNomPerfil='" + getVarNomPerfil() + "'" +
            ", varDescPerfil='" + getVarDescPerfil() + "'" +
            ", numEstPerfil='" + getNumEstPerfil() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
