package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Entidad.
 */
@Entity
@Table(name = "sgl_tm_entidad")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "entidad")
public class Entidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 300)
    @Column(name = "var_rsocial_entidad", length = 300)
    private String varRsocialEntidad;

    @Size(max = 20)
    @Column(name = "var_ruc_entidad", length = 20)
    private String varRucEntidad;

    @Size(max = 300)
    @Column(name = "var_direc_entidad", length = 300)
    private String varDirecEntidad;

    @Column(name = "num_cod_departamento")
    private String numCodDepartamento;

    @Column(name = "num_cod_provincia")
    private String numCodProvincia;

    @Column(name = "num_cod_distrito")
    private String numCodDistrito;

    @Size(max = 50)
    @Column(name = "var_telef_entidad", length = 50)
    private String varTelefEntidad;

    @Size(max = 50)
    @Column(name = "var_fax_entidad", length = 50)
    private String varFaxEntidad;

    @Size(max = 100)
    @Column(name = "var_email_entidad", length = 100)
    private String varEmailEntidad;

    @Size(max = 300)
    @Column(name = "var_pagweb_entidad", length = 300)
    private String varPagwebEntidad;

    @NotNull
    @Column(name = "var_usuario_log", nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @Size(max = 8)
    @Column(name = "s_codigoentcer", length = 8)
    private String sCodigoentcer;

    @Size(max = 50)
    @Column(name = "latitud", length = 50)
    private String latitud;

    @Size(max = 50)
    @Column(name = "longitud", length = 50)
    private String longitud;

    @ManyToOne
    private TipoEntidad tipoEntidad;

    @OneToMany(mappedBy = "entidad")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ModuloEntidad> moduloEntidads = new HashSet<>();

    @OneToMany(mappedBy = "entidad")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Grupo> grupos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarRsocialEntidad() {
        return varRsocialEntidad;
    }

    public Entidad varRsocialEntidad(String varRsocialEntidad) {
        this.varRsocialEntidad = varRsocialEntidad;
        return this;
    }

    public void setVarRsocialEntidad(String varRsocialEntidad) {
        this.varRsocialEntidad = varRsocialEntidad;
    }

    public String getVarRucEntidad() {
        return varRucEntidad;
    }

    public Entidad varRucEntidad(String varRucEntidad) {
        this.varRucEntidad = varRucEntidad;
        return this;
    }

    public void setVarRucEntidad(String varRucEntidad) {
        this.varRucEntidad = varRucEntidad;
    }

    public String getVarDirecEntidad() {
        return varDirecEntidad;
    }

    public Entidad varDirecEntidad(String varDirecEntidad) {
        this.varDirecEntidad = varDirecEntidad;
        return this;
    }

    public void setVarDirecEntidad(String varDirecEntidad) {
        this.varDirecEntidad = varDirecEntidad;
    }

    public String getNumCodDepartamento() {
        return numCodDepartamento;
    }

    public Entidad numCodDepartamento(String numCodDepartamento) {
        this.numCodDepartamento = numCodDepartamento;
        return this;
    }

    public void setNumCodDepartamento(String numCodDepartamento) {
        this.numCodDepartamento = numCodDepartamento;
    }

    public String getNumCodProvincia() {
        return numCodProvincia;
    }

    public Entidad numCodProvincia(String numCodProvincia) {
        this.numCodProvincia = numCodProvincia;
        return this;
    }

    public void setNumCodProvincia(String numCodProvincia) {
        this.numCodProvincia = numCodProvincia;
    }

    public String getNumCodDistrito() {
        return numCodDistrito;
    }

    public Entidad numCodDistrito(String numCodDistrito) {
        this.numCodDistrito = numCodDistrito;
        return this;
    }

    public void setNumCodDistrito(String numCodDistrito) {
        this.numCodDistrito = numCodDistrito;
    }

    public String getVarTelefEntidad() {
        return varTelefEntidad;
    }

    public Entidad varTelefEntidad(String varTelefEntidad) {
        this.varTelefEntidad = varTelefEntidad;
        return this;
    }

    public void setVarTelefEntidad(String varTelefEntidad) {
        this.varTelefEntidad = varTelefEntidad;
    }

    public String getVarFaxEntidad() {
        return varFaxEntidad;
    }

    public Entidad varFaxEntidad(String varFaxEntidad) {
        this.varFaxEntidad = varFaxEntidad;
        return this;
    }

    public void setVarFaxEntidad(String varFaxEntidad) {
        this.varFaxEntidad = varFaxEntidad;
    }

    public String getVarEmailEntidad() {
        return varEmailEntidad;
    }

    public Entidad varEmailEntidad(String varEmailEntidad) {
        this.varEmailEntidad = varEmailEntidad;
        return this;
    }

    public void setVarEmailEntidad(String varEmailEntidad) {
        this.varEmailEntidad = varEmailEntidad;
    }

    public String getVarPagwebEntidad() {
        return varPagwebEntidad;
    }

    public Entidad varPagwebEntidad(String varPagwebEntidad) {
        this.varPagwebEntidad = varPagwebEntidad;
        return this;
    }

    public void setVarPagwebEntidad(String varPagwebEntidad) {
        this.varPagwebEntidad = varPagwebEntidad;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Entidad varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Entidad datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Entidad numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public String getsCodigoentcer() {
        return sCodigoentcer;
    }

    public Entidad sCodigoentcer(String sCodigoentcer) {
        this.sCodigoentcer = sCodigoentcer;
        return this;
    }

    public void setsCodigoentcer(String sCodigoentcer) {
        this.sCodigoentcer = sCodigoentcer;
    }

    public String getLatitud() {
        return latitud;
    }

    public Entidad latitud(String latitud) {
        this.latitud = latitud;
        return this;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public Entidad longitud(String longitud) {
        this.longitud = longitud;
        return this;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public TipoEntidad getTipoEntidad() {
        return tipoEntidad;
    }

    public Entidad tipoEntidad(TipoEntidad tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
        return this;
    }

    public void setTipoEntidad(TipoEntidad tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

    public Set<ModuloEntidad> getModuloEntidads() {
        return moduloEntidads;
    }

    public Entidad moduloEntidads(Set<ModuloEntidad> moduloEntidads) {
        this.moduloEntidads = moduloEntidads;
        return this;
    }

    public Entidad addModuloEntidad(ModuloEntidad moduloEntidad) {
        this.moduloEntidads.add(moduloEntidad);
        moduloEntidad.setEntidad(this);
        return this;
    }

    public Entidad removeModuloEntidad(ModuloEntidad moduloEntidad) {
        this.moduloEntidads.remove(moduloEntidad);
        moduloEntidad.setEntidad(null);
        return this;
    }

    public void setModuloEntidads(Set<ModuloEntidad> moduloEntidads) {
        this.moduloEntidads = moduloEntidads;
    }

    public Set<Grupo> getGrupos() {
        return grupos;
    }

    public Entidad grupos(Set<Grupo> grupos) {
        this.grupos = grupos;
        return this;
    }

    public Entidad addGrupo(Grupo grupo) {
        this.grupos.add(grupo);
        grupo.setEntidad(this);
        return this;
    }

    public Entidad removeGrupo(Grupo grupo) {
        this.grupos.remove(grupo);
        grupo.setEntidad(null);
        return this;
    }

    public void setGrupos(Set<Grupo> grupos) {
        this.grupos = grupos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Entidad entidad = (Entidad) o;
        if (entidad.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), entidad.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Entidad{" +
            "id=" + getId() +
            ", varRsocialEntidad='" + getVarRsocialEntidad() + "'" +
            ", varRucEntidad='" + getVarRucEntidad() + "'" +
            ", varDirecEntidad='" + getVarDirecEntidad() + "'" +
            ", numCodDepartamento='" + getNumCodDepartamento() + "'" +
            ", numCodProvincia='" + getNumCodProvincia() + "'" +
            ", numCodDistrito='" + getNumCodDistrito() + "'" +
            ", varTelefEntidad='" + getVarTelefEntidad() + "'" +
            ", varFaxEntidad='" + getVarFaxEntidad() + "'" +
            ", varEmailEntidad='" + getVarEmailEntidad() + "'" +
            ", varPagwebEntidad='" + getVarPagwebEntidad() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            ", sCodigoentcer='" + getsCodigoentcer() + "'" +
            ", latitud='" + getLatitud() + "'" +
            ", longitud='" + getLongitud() + "'" +
            "}";
    }
}
