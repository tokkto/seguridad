package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A UsuPer.
 */
@Entity
@Table(name = "sgl_tm_usu_per")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usuper")
public class UsuPer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @ManyToOne
    private Usuario usuario;

    @ManyToOne
    private UsuarioGrupo usuarioGrupo;

    @ManyToOne
    private Permiso permiso;

    @ManyToOne
    private Perfil perfil;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public UsuPer varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public UsuPer datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public UsuPer numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public UsuPer usuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioGrupo getUsuarioGrupo() {
        return usuarioGrupo;
    }

    public UsuPer usuarioGrupo(UsuarioGrupo usuarioGrupo) {
        this.usuarioGrupo = usuarioGrupo;
        return this;
    }

    public void setUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        this.usuarioGrupo = usuarioGrupo;
    }

    public Permiso getPermiso() {
        return permiso;
    }

    public UsuPer permiso(Permiso permiso) {
        this.permiso = permiso;
        return this;
    }

    public void setPermiso(Permiso permiso) {
        this.permiso = permiso;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public UsuPer perfil(Perfil perfil) {
        this.perfil = perfil;
        return this;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UsuPer usuPer = (UsuPer) o;
        if (usuPer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usuPer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UsuPer{" +
            "id=" + getId() +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
