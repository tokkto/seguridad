package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Grupo.
 */
@Entity
@Table(name = "sgl_tm_grupo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "grupo")
public class Grupo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "num_cod_local")
    private Integer numCodLocal;

    @Column(name = "num_est_grupo")
    private Integer numEstGrupo;

    @Size(max = 100)
    @Column(name = "var_nom_grupo", length = 100)
    private String varNomGrupo;

    @Size(max = 300)
    @Column(name = "var_desc_grupo", length = 300)
    private String varDescGrupo;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @ManyToOne
    private Entidad entidad;

    @OneToMany(mappedBy = "grupo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UsuarioGrupo> usuarioGrupos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumCodLocal() {
        return numCodLocal;
    }

    public Grupo numCodLocal(Integer numCodLocal) {
        this.numCodLocal = numCodLocal;
        return this;
    }

    public void setNumCodLocal(Integer numCodLocal) {
        this.numCodLocal = numCodLocal;
    }

    public Integer getNumEstGrupo() {
        return numEstGrupo;
    }

    public Grupo numEstGrupo(Integer numEstGrupo) {
        this.numEstGrupo = numEstGrupo;
        return this;
    }

    public void setNumEstGrupo(Integer numEstGrupo) {
        this.numEstGrupo = numEstGrupo;
    }

    public String getVarNomGrupo() {
        return varNomGrupo;
    }

    public Grupo varNomGrupo(String varNomGrupo) {
        this.varNomGrupo = varNomGrupo;
        return this;
    }

    public void setVarNomGrupo(String varNomGrupo) {
        this.varNomGrupo = varNomGrupo;
    }

    public String getVarDescGrupo() {
        return varDescGrupo;
    }

    public Grupo varDescGrupo(String varDescGrupo) {
        this.varDescGrupo = varDescGrupo;
        return this;
    }

    public void setVarDescGrupo(String varDescGrupo) {
        this.varDescGrupo = varDescGrupo;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Grupo varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Grupo datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Grupo numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public Grupo entidad(Entidad entidad) {
        this.entidad = entidad;
        return this;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public Set<UsuarioGrupo> getUsuarioGrupos() {
        return usuarioGrupos;
    }

    public Grupo usuarioGrupos(Set<UsuarioGrupo> usuarioGrupos) {
        this.usuarioGrupos = usuarioGrupos;
        return this;
    }

    public Grupo addUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        this.usuarioGrupos.add(usuarioGrupo);
        usuarioGrupo.setGrupo(this);
        return this;
    }

    public Grupo removeUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        this.usuarioGrupos.remove(usuarioGrupo);
        usuarioGrupo.setGrupo(null);
        return this;
    }

    public void setUsuarioGrupos(Set<UsuarioGrupo> usuarioGrupos) {
        this.usuarioGrupos = usuarioGrupos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Grupo grupo = (Grupo) o;
        if (grupo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), grupo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Grupo{" +
            "id=" + getId() +
            ", numCodLocal='" + getNumCodLocal() + "'" +
            ", numEstGrupo='" + getNumEstGrupo() + "'" +
            ", varNomGrupo='" + getVarNomGrupo() + "'" +
            ", varDescGrupo='" + getVarDescGrupo() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
