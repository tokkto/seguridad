package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.HashSet;

/**
 * A Departamento.
 */
@Entity
@Table(name = "sitb_departamento", schema="SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Departamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "v_coddep")
    private String vCoddep;

    @Column(name = "v_desdep", length = 100)
    private String vDesdep;

    @Size(max = 100)
    @Column(name = "v_coddepren", length = 2)
    private String vCoddepren;

    @Size(max = 300)
    @Column(name = "v_flgact", length = 1)
    private String vFlgact;

    @OneToMany(mappedBy = "departamento")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Provincia> provincias = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public String getVCoddep() {
        return vCoddep;
    }
    public void setVCoddep(String vCoddep) {
        this.vCoddep = vCoddep;
    }

    public String getVDesdep() {
        return vDesdep;
    }
    public void setVDesdep(String vDesdep) {
        this.vDesdep = vDesdep;
    }

    public String getVCoddepren() {
        return vCoddepren;
    }
    public void setVCoddepren(String vCoddepren) {
        this.vCoddepren = vCoddepren;
    }

    public String getVFlgact() {
        return vFlgact;
    }
    public void setVFlgact(String vFlgact) {
        this.vFlgact = vFlgact;
    }
    
    public Set<Provincia> getProvincia() {
        return provincias;
    }
    public void setProvincia(Set<Provincia> provincias) {
        this.provincias = provincias;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Departamento departamento = (Departamento) o;
        if (departamento.getVCoddep() == null || getVCoddep() == null) {
            return false;
        }
        return Objects.equals(getVCoddep(), departamento.getVCoddep());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getVCoddep());
    }

    @Override
    public String toString() {
        return "Departamento{" +
            "id=" + getVCoddep() +
            ", descripcion='" + getVDesdep() + "'" +
            ", codigopren='" + getVCoddepren() + "'" +
            ", flag='" + getVFlgact() + "'" +
            "}";
    }
}
