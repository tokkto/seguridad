package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A TipoEntidad.
 */
@Entity
@Table(name = "sgl_tm_tipo_entidad")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tipoentidad")
public class TipoEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_tpentidad", length = 100)
    private String varNomTpentidad;

    @Size(max = 300)
    @Column(name = "var_desc_tpentidad", length = 300)
    private String varDescTpentidad;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @OneToMany(mappedBy = "tipoEntidad")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Entidad> entidads = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomTpentidad() {
        return varNomTpentidad;
    }

    public TipoEntidad varNomTpentidad(String varNomTpentidad) {
        this.varNomTpentidad = varNomTpentidad;
        return this;
    }

    public void setVarNomTpentidad(String varNomTpentidad) {
        this.varNomTpentidad = varNomTpentidad;
    }

    public String getVarDescTpentidad() {
        return varDescTpentidad;
    }

    public TipoEntidad varDescTpentidad(String varDescTpentidad) {
        this.varDescTpentidad = varDescTpentidad;
        return this;
    }

    public void setVarDescTpentidad(String varDescTpentidad) {
        this.varDescTpentidad = varDescTpentidad;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public TipoEntidad varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public TipoEntidad datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public TipoEntidad numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Set<Entidad> getEntidads() {
        return entidads;
    }

    public TipoEntidad entidads(Set<Entidad> entidads) {
        this.entidads = entidads;
        return this;
    }

    public TipoEntidad addEntidad(Entidad entidad) {
        this.entidads.add(entidad);
        entidad.setTipoEntidad(this);
        return this;
    }

    public TipoEntidad removeEntidad(Entidad entidad) {
        this.entidads.remove(entidad);
        entidad.setTipoEntidad(null);
        return this;
    }

    public void setEntidads(Set<Entidad> entidads) {
        this.entidads = entidads;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoEntidad tipoEntidad = (TipoEntidad) o;
        if (tipoEntidad.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoEntidad.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoEntidad{" +
            "id=" + getId() +
            ", varNomTpentidad='" + getVarNomTpentidad() + "'" +
            ", varDescTpentidad='" + getVarDescTpentidad() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
