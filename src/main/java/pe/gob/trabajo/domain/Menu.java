package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Menu.
 */
@Entity
@Table(name = "sgl_tm_menu")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_menu", length = 100)
    private String varNomMenu;

    @Column(name = "num_orden_item")
    private Integer numOrdenItem;

    @Size(max = 300)
    @Column(name = "var_url_menu", length = 300)
    private String varUrlMenu;

    @Column(name = "num_opcion")
    private Integer numOpcion;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @ManyToOne
    private Modulo modulo;

    @ManyToOne
    private Menu menu;

    @OneToMany(mappedBy = "menu")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Menu> menus = new HashSet<>();

    @OneToMany(mappedBy = "menu")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MenuPerfil> menuPerfils = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomMenu() {
        return varNomMenu;
    }

    public Menu varNomMenu(String varNomMenu) {
        this.varNomMenu = varNomMenu;
        return this;
    }

    public void setVarNomMenu(String varNomMenu) {
        this.varNomMenu = varNomMenu;
    }

    public Integer getNumOrdenItem() {
        return numOrdenItem;
    }

    public Menu numOrdenItem(Integer numOrdenItem) {
        this.numOrdenItem = numOrdenItem;
        return this;
    }

    public void setNumOrdenItem(Integer numOrdenItem) {
        this.numOrdenItem = numOrdenItem;
    }

    public String getVarUrlMenu() {
        return varUrlMenu;
    }

    public Menu varUrlMenu(String varUrlMenu) {
        this.varUrlMenu = varUrlMenu;
        return this;
    }

    public void setVarUrlMenu(String varUrlMenu) {
        this.varUrlMenu = varUrlMenu;
    }

    public Integer getNumOpcion() {
        return numOpcion;
    }

    public Menu numOpcion(Integer numOpcion) {
        this.numOpcion = numOpcion;
        return this;
    }

    public void setNumOpcion(Integer numOpcion) {
        this.numOpcion = numOpcion;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Menu varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Menu datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Menu numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public Menu modulo(Modulo modulo) {
        this.modulo = modulo;
        return this;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public Menu getMenu() {
        return menu;
    }

    public Menu menu(Menu menu) {
        this.menu = menu;
        return this;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Set<Menu> getMenus() {
        return menus;
    }

    public Menu menus(Set<Menu> menus) {
        this.menus = menus;
        return this;
    }

    public Menu addMenu(Menu menu) {
        this.menus.add(menu);
        menu.setMenu(this);
        return this;
    }

    public Menu removeMenu(Menu menu) {
        this.menus.remove(menu);
        menu.setMenu(null);
        return this;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    public Set<MenuPerfil> getMenuPerfils() {
        return menuPerfils;
    }

    public Menu menuPerfils(Set<MenuPerfil> menuPerfils) {
        this.menuPerfils = menuPerfils;
        return this;
    }

    public Menu addMenuPerfil(MenuPerfil menuPerfil) {
        this.menuPerfils.add(menuPerfil);
        menuPerfil.setMenu(this);
        return this;
    }

    public Menu removeMenuPerfil(MenuPerfil menuPerfil) {
        this.menuPerfils.remove(menuPerfil);
        menuPerfil.setMenu(null);
        return this;
    }

    public void setMenuPerfils(Set<MenuPerfil> menuPerfils) {
        this.menuPerfils = menuPerfils;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Menu menu = (Menu) o;
        if (menu.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), menu.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Menu{" +
            "id=" + getId() +
            ", varNomMenu='" + getVarNomMenu() + "'" +
            ", numOrdenItem='" + getNumOrdenItem() + "'" +
            ", varUrlMenu='" + getVarUrlMenu() + "'" +
            ", numOpcion='" + getNumOpcion() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
