package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A TipoUsuario.
 */
@Entity
@Table(name = "sgl_tm_tipo_usuario")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tipousuario")
public class TipoUsuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_tpusuario", length = 100)
    private String varNomTpusuario;

    @Size(max = 300)
    @Column(name = "var_desc_tpusuario", length = 300)
    private String varDescTpusuario;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @OneToMany(mappedBy = "tipoUsuario")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Usuario> usuarios = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomTpusuario() {
        return varNomTpusuario;
    }

    public TipoUsuario varNomTpusuario(String varNomTpusuario) {
        this.varNomTpusuario = varNomTpusuario;
        return this;
    }

    public void setVarNomTpusuario(String varNomTpusuario) {
        this.varNomTpusuario = varNomTpusuario;
    }

    public String getVarDescTpusuario() {
        return varDescTpusuario;
    }

    public TipoUsuario varDescTpusuario(String varDescTpusuario) {
        this.varDescTpusuario = varDescTpusuario;
        return this;
    }

    public void setVarDescTpusuario(String varDescTpusuario) {
        this.varDescTpusuario = varDescTpusuario;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public TipoUsuario varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public TipoUsuario datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public TipoUsuario numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Set<Usuario> getUsuarios() {
        return usuarios;
    }

    public TipoUsuario usuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
        return this;
    }

    public TipoUsuario addUsuario(Usuario usuario) {
        this.usuarios.add(usuario);
        usuario.setTipoUsuario(this);
        return this;
    }

    public TipoUsuario removeUsuario(Usuario usuario) {
        this.usuarios.remove(usuario);
        usuario.setTipoUsuario(null);
        return this;
    }

    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoUsuario tipoUsuario = (TipoUsuario) o;
        if (tipoUsuario.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoUsuario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoUsuario{" +
            "id=" + getId() +
            ", varNomTpusuario='" + getVarNomTpusuario() + "'" +
            ", varDescTpusuario='" + getVarDescTpusuario() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
