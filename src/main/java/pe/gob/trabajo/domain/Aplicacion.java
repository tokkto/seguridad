package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Aplicacion.
 */
@Entity
@Table(name = "sgl_tm_aplicacion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "aplicacion")
public class Aplicacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_app", length = 100)
    private String varNomApp;

    @Size(max = 300)
    @Column(name = "var_desc_app", length = 300)
    private String varDescApp;

    @Size(max = 100)
    @Column(name = "var_url_app", length = 100)
    private String varUrlApp;

    @Column(name = "num_est_app")
    private Integer numEstApp;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @OneToMany(mappedBy = "aplicacion")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Modulo> modulos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomApp() {
        return varNomApp;
    }

    public Aplicacion varNomApp(String varNomApp) {
        this.varNomApp = varNomApp;
        return this;
    }

    public void setVarNomApp(String varNomApp) {
        this.varNomApp = varNomApp;
    }

    public String getVarDescApp() {
        return varDescApp;
    }

    public Aplicacion varDescApp(String varDescApp) {
        this.varDescApp = varDescApp;
        return this;
    }

    public void setVarDescApp(String varDescApp) {
        this.varDescApp = varDescApp;
    }

    public String getVarUrlApp() {
        return varUrlApp;
    }

    public Aplicacion varUrlApp(String varUrlApp) {
        this.varUrlApp = varUrlApp;
        return this;
    }

    public void setVarUrlApp(String varUrlApp) {
        this.varUrlApp = varUrlApp;
    }

    public Integer getNumEstApp() {
        return numEstApp;
    }

    public Aplicacion numEstApp(Integer numEstApp) {
        this.numEstApp = numEstApp;
        return this;
    }

    public void setNumEstApp(Integer numEstApp) {
        this.numEstApp = numEstApp;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Aplicacion varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Aplicacion datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Aplicacion numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Set<Modulo> getModulos() {
        return modulos;
    }

    public Aplicacion modulos(Set<Modulo> modulos) {
        this.modulos = modulos;
        return this;
    }

    public Aplicacion addModulo(Modulo modulo) {
        this.modulos.add(modulo);
        modulo.setAplicacion(this);
        return this;
    }

    public Aplicacion removeModulo(Modulo modulo) {
        this.modulos.remove(modulo);
        modulo.setAplicacion(null);
        return this;
    }

    public void setModulos(Set<Modulo> modulos) {
        this.modulos = modulos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Aplicacion aplicacion = (Aplicacion) o;
        if (aplicacion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), aplicacion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Aplicacion{" +
            "id=" + getId() +
            ", varNomApp='" + getVarNomApp() + "'" +
            ", varDescApp='" + getVarDescApp() + "'" +
            ", varUrlApp='" + getVarUrlApp() + "'" +
            ", numEstApp='" + getNumEstApp() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
