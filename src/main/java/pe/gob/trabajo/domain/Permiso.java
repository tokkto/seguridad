package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Permiso.
 */
@Entity
@Table(name = "sgl_tm_permiso")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "permiso")
public class Permiso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_permiso", length = 100)
    private String varNomPermiso;

    @NotNull
    @Column(name = "var_usuario_log", nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @OneToMany(mappedBy = "permiso")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UsuPer> usuPers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomPermiso() {
        return varNomPermiso;
    }

    public Permiso varNomPermiso(String varNomPermiso) {
        this.varNomPermiso = varNomPermiso;
        return this;
    }

    public void setVarNomPermiso(String varNomPermiso) {
        this.varNomPermiso = varNomPermiso;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Permiso varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Permiso datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Permiso numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Set<UsuPer> getUsuPers() {
        return usuPers;
    }

    public Permiso usuPers(Set<UsuPer> usuPers) {
        this.usuPers = usuPers;
        return this;
    }

    public Permiso addUsuPer(UsuPer usuPer) {
        this.usuPers.add(usuPer);
        usuPer.setPermiso(this);
        return this;
    }

    public Permiso removeUsuPer(UsuPer usuPer) {
        this.usuPers.remove(usuPer);
        usuPer.setPermiso(null);
        return this;
    }

    public void setUsuPers(Set<UsuPer> usuPers) {
        this.usuPers = usuPers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Permiso permiso = (Permiso) o;
        if (permiso.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), permiso.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Permiso{" +
            "id=" + getId() +
            ", varNomPermiso='" + getVarNomPermiso() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
