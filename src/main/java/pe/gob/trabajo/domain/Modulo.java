package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Modulo.
 */
@Entity
@Table(name = "sgl_tm_modulo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "modulo")
public class Modulo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 100)
    @Column(name = "var_nom_modulo", length = 100)
    private String varNomModulo;

    @Size(max = 300)
    @Column(name = "var_desc_modulo", length = 300)
    private String varDescModulo;

    @Column(name = "num_est_modulo")
    private Integer numEstModulo;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @ManyToOne
    private Aplicacion aplicacion;

    @OneToMany(mappedBy = "modulo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ModuloEntidad> moduloEntidads = new HashSet<>();

    @OneToMany(mappedBy = "modulo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Menu> menus = new HashSet<>();

    @OneToMany(mappedBy = "modulo")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Perfil> perfils = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVarNomModulo() {
        return varNomModulo;
    }

    public Modulo varNomModulo(String varNomModulo) {
        this.varNomModulo = varNomModulo;
        return this;
    }

    public void setVarNomModulo(String varNomModulo) {
        this.varNomModulo = varNomModulo;
    }

    public String getVarDescModulo() {
        return varDescModulo;
    }

    public Modulo varDescModulo(String varDescModulo) {
        this.varDescModulo = varDescModulo;
        return this;
    }

    public void setVarDescModulo(String varDescModulo) {
        this.varDescModulo = varDescModulo;
    }

    public Integer getNumEstModulo() {
        return numEstModulo;
    }

    public Modulo numEstModulo(Integer numEstModulo) {
        this.numEstModulo = numEstModulo;
        return this;
    }

    public void setNumEstModulo(Integer numEstModulo) {
        this.numEstModulo = numEstModulo;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public Modulo varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public Modulo datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public Modulo numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Aplicacion getAplicacion() {
        return aplicacion;
    }

    public Modulo aplicacion(Aplicacion aplicacion) {
        this.aplicacion = aplicacion;
        return this;
    }

    public void setAplicacion(Aplicacion aplicacion) {
        this.aplicacion = aplicacion;
    }

    public Set<ModuloEntidad> getModuloEntidads() {
        return moduloEntidads;
    }

    public Modulo moduloEntidads(Set<ModuloEntidad> moduloEntidads) {
        this.moduloEntidads = moduloEntidads;
        return this;
    }

    public Modulo addModuloEntidad(ModuloEntidad moduloEntidad) {
        this.moduloEntidads.add(moduloEntidad);
        moduloEntidad.setModulo(this);
        return this;
    }

    public Modulo removeModuloEntidad(ModuloEntidad moduloEntidad) {
        this.moduloEntidads.remove(moduloEntidad);
        moduloEntidad.setModulo(null);
        return this;
    }

    public void setModuloEntidads(Set<ModuloEntidad> moduloEntidads) {
        this.moduloEntidads = moduloEntidads;
    }

    public Set<Menu> getMenus() {
        return menus;
    }

    public Modulo menus(Set<Menu> menus) {
        this.menus = menus;
        return this;
    }

    public Modulo addMenu(Menu menu) {
        this.menus.add(menu);
        menu.setModulo(this);
        return this;
    }

    public Modulo removeMenu(Menu menu) {
        this.menus.remove(menu);
        menu.setModulo(null);
        return this;
    }

    public void setMenus(Set<Menu> menus) {
        this.menus = menus;
    }

    public Set<Perfil> getPerfils() {
        return perfils;
    }

    public Modulo perfils(Set<Perfil> perfils) {
        this.perfils = perfils;
        return this;
    }

    public Modulo addPerfil(Perfil perfil) {
        this.perfils.add(perfil);
        perfil.setModulo(this);
        return this;
    }

    public Modulo removePerfil(Perfil perfil) {
        this.perfils.remove(perfil);
        perfil.setModulo(null);
        return this;
    }

    public void setPerfils(Set<Perfil> perfils) {
        this.perfils = perfils;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Modulo modulo = (Modulo) o;
        if (modulo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modulo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Modulo{" +
            "id=" + getId() +
            ", varNomModulo='" + getVarNomModulo() + "'" +
            ", varDescModulo='" + getVarDescModulo() + "'" +
            ", numEstModulo='" + getNumEstModulo() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
