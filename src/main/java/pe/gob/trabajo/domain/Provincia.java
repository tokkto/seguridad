package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Provincia.
 */
@Entity
@Table(name = "sitb_provincia", schema="SIMINTRA1")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Provincia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "v_coddep")
    private String vCoddep;

    @Id
    @Column(name = "v_codpro")
    private String vCodpro;

    @Column(name = "v_despro", length = 100)
    private String vDespro;

    @Size(max = 2)
    @Column(name = "v_coddepren", length = 2)
    private String vCoddepren;

    @Size(max = 2)
    @Column(name = "v_codproren", length = 2)
    private String vCodproren;

    @Column(name = "v_flgact", length = 1)
    private String vFlgact;

    @OneToMany(mappedBy = "provincia")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Distrito> distritos = new HashSet<>();

    @ManyToOne
    private Departamento departamento;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public String getVCoddep() {
        return vCoddep;
    }
    public void setVCoddep(String vCoddep) {
        this.vCoddep = vCoddep;
    }
    public String getVCodpro() {
        return vCodpro;
    }
    public void setVCodpro(String vCodpro) {
        this.vCodpro = vCodpro;
    }

    public String getVDespro() {
        return vDespro;
    }
    public void setVDespro(String vDespro) {
        this.vDespro = vDespro;
    }

    public String getVCoddepren() {
        return vCoddepren;
    }
    public void setVCoddepren(String vCoddepren) {
        this.vCoddepren = vCoddepren;
    }

    public String getVCodproren() {
        return vCodproren;
    }
    public void setVCodproren(String vCodproren) {
        this.vCodproren = vCodproren;
    }

    public String getVFlgact() {
        return vFlgact;
    }
    public void setVFlgact(String vFlgact) {
        this.vFlgact = vFlgact;
    }

    public Departamento getDepartamento() {
        return departamento;
    }
    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Set<Distrito> getDistrito() {
        return distritos;
    }
    public void setDistrito(Set<Distrito> distritos) {
        this.distritos = distritos;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Provincia provincia = (Provincia) o;
        if (provincia.getVCodpro() == null || getVCodpro() == null) {
            return false;
        }
        return Objects.equals(getVCodpro(), provincia.getVCodpro());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getVCodpro());
    }

    @Override
    public String toString() {
        return "Provincia{" +
            "id=" + getVCodpro() +
            ", descripcion='" + getVDespro() + "'" +
            ", codigopren='" + getVCodproren() + "'" +
            ", flag='" + getVFlgact() + "'" +
            "}";
    }
}
