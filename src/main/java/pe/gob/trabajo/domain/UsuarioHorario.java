package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UsuarioHorario.
 */
@Entity
@Table(name = "sgl_tm_usuario_hora")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usuariohorario")
public class UsuarioHorario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "num_dia_semana")
    private Integer numDiaSemana;

    @Size(max = 4)
    @Column(name = "dat_hora_inicio", length = 4)
    private String datHoraInicio;

    @Size(max = 4)
    @Column(name = "dat_hora_fin", length = 4)
    private String datHoraFin;

    @NotNull
    @Size(max = 20)
    @Column(name = "var_usuario_log", length = 20, nullable = false)
    private String varUsuarioLog;

    @Column(name = "dat_fecha_log", nullable = false)
    private Instant datFechaLog;

    @NotNull
    @Min(value = 0)
    @Max(value = 1)
    @Column(name = "num_eliminar", nullable = false)
    private Integer numEliminar;

    @ManyToOne
    private Usuario usuario;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumDiaSemana() {
        return numDiaSemana;
    }

    public UsuarioHorario numDiaSemana(Integer numDiaSemana) {
        this.numDiaSemana = numDiaSemana;
        return this;
    }

    public void setNumDiaSemana(Integer numDiaSemana) {
        this.numDiaSemana = numDiaSemana;
    }

    public String getDatHoraInicio() {
        return datHoraInicio;
    }

    public UsuarioHorario datHoraInicio(String datHoraInicio) {
        this.datHoraInicio = datHoraInicio;
        return this;
    }

    public void setDatHoraInicio(String datHoraInicio) {
        this.datHoraInicio = datHoraInicio;
    }

    public String getDatHoraFin() {
        return datHoraFin;
    }

    public UsuarioHorario datHoraFin(String datHoraFin) {
        this.datHoraFin = datHoraFin;
        return this;
    }

    public void setDatHoraFin(String datHoraFin) {
        this.datHoraFin = datHoraFin;
    }

    public String getVarUsuarioLog() {
        return varUsuarioLog;
    }

    public UsuarioHorario varUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
        return this;
    }

    public void setVarUsuarioLog(String varUsuarioLog) {
        this.varUsuarioLog = varUsuarioLog;
    }

    public Instant getDatFechaLog() {
        return datFechaLog;
    }

    public UsuarioHorario datFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
        return this;
    }

    public void setDatFechaLog(Instant datFechaLog) {
        this.datFechaLog = datFechaLog;
    }

    public Integer getNumEliminar() {
        return numEliminar;
    }

    public UsuarioHorario numEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
        return this;
    }

    public void setNumEliminar(Integer numEliminar) {
        this.numEliminar = numEliminar;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public UsuarioHorario usuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UsuarioHorario usuarioHorario = (UsuarioHorario) o;
        if (usuarioHorario.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usuarioHorario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UsuarioHorario{" +
            "id=" + getId() +
            ", numDiaSemana='" + getNumDiaSemana() + "'" +
            ", datHoraInicio='" + getDatHoraInicio() + "'" +
            ", datHoraFin='" + getDatHoraFin() + "'" +
            ", varUsuarioLog='" + getVarUsuarioLog() + "'" +
            ", datFechaLog='" + getDatFechaLog() + "'" +
            ", numEliminar='" + getNumEliminar() + "'" +
            "}";
    }
}
