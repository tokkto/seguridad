package pe.gob.trabajo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.trabajo.domain.UsuPer;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UsuPer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsuPerRepository extends JpaRepository<UsuPer, Long> {

    @Query("select M.id, M.datFechaLog from UsuPer M where M.usuario.id = ?1 order by M.id")
    List<UsuPer> findUsuPer(Long id);

}
