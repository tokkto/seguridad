package pe.gob.trabajo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.trabajo.domain.Aplicacion;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Aplicacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AplicacionRepository extends JpaRepository<Aplicacion, Long> {

    @Query("select apl from Aplicacion apl where numEliminar!=0")
    Page<Aplicacion> findAllAplicationLogic(Pageable pageable);

}
