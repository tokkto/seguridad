package pe.gob.trabajo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.trabajo.domain.Modulo;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;


/**
 * Spring Data JPA repository for the Modulo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModuloRepository extends JpaRepository<Modulo, Long> {

    @Query("select M.id, M.varNomModulo, MD.id from Modulo M left join ModuloEntidad MD on M.id = MD.modulo.id and MD.entidad.id = ?1 where M.numEstModulo=1 and M.numEliminar!=0 order by M.id")
    List<Object[]> findModuloEntidad(Long id);

    @Query("select apl from Modulo apl where numEliminar!=0")
    Page<Modulo> findAllModuleLogic(Pageable pageable);

}
