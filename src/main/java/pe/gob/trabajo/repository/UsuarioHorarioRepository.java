package pe.gob.trabajo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.trabajo.domain.UsuarioHorario;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import java.util.List;


/**
 * Spring Data JPA repository for the UsuarioHorario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsuarioHorarioRepository extends JpaRepository<UsuarioHorario, Long> {

    @Query("select usuarioHorario from UsuarioHorario usuarioHorario where usuarioHorario.usuario.id = ?1 and usuarioHorario.numEliminar!=0")
    List<UsuarioHorario> findByUser(Long id);

    @Query("select apl from UsuarioHorario apl where numEliminar!=0")
    Page<UsuarioHorario> findAllUsuarioHorariosLogic(Pageable pageable);
  


}

