package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.ModuloEntidad;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ModuloEntidad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModuloEntidadRepository extends JpaRepository<ModuloEntidad, Long> {

}
