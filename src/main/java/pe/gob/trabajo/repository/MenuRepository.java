package pe.gob.trabajo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.trabajo.domain.Menu;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Menu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {

    @Query("select apl from Menu apl where numEliminar!=0")
    Page<Menu> findAllMenuLogic(Pageable pageable);

}
