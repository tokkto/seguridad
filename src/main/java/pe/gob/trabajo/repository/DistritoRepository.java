package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Distrito;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Distrito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistritoRepository extends JpaRepository<Distrito, String> {

    @Query("select dist.vCoddep, dist.vCodpro, dist.vCoddis, dist.vDesdis from Distrito dist where dist.vCoddep=?1 and dist.vCodpro=?2 and dist.vCoddep != 0 order by dist.vCoddis")
    List<Distrito> findFilterDistritos(String id, String idprov);
}
