package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.TipoEntidad;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TipoEntidad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoEntidadRepository extends JpaRepository<TipoEntidad, Long> {

}
