package pe.gob.trabajo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.trabajo.domain.Perfil;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Perfil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long> {

    @Query("select apl from Perfil apl where numEliminar!=0")
    Page<Perfil> findAllPerfilLogic(Pageable pageable);

}
