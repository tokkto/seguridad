package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Provincia;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Provincia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, String> {

    @Query("select prov.vCoddep, prov.vCodpro, prov.vDespro from Provincia prov where prov.vCoddep=?1 and prov.vCoddep != 0 order by prov.vCodpro")
    List<Provincia> findFilterProvincias(String id);
}
