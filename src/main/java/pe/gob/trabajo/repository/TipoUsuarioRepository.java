package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.TipoUsuario;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TipoUsuario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoUsuarioRepository extends JpaRepository<TipoUsuario, Long> {

}
