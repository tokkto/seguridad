package pe.gob.trabajo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.trabajo.domain.UsuarioGrupo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UsuarioGrupo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsuarioGrupoRepository extends JpaRepository<UsuarioGrupo, Long> {

    @Query("select usuarioGrupo from UsuarioGrupo usuarioGrupo where usuarioGrupo.usuario.id = ?1 and usuarioGrupo.numEliminar!=0")
    List<UsuarioGrupo> findByUser(Long id);

    @Query("select apl from UsuarioGrupo apl where numEliminar!=0")
    Page<UsuarioGrupo> findAllUsuarioGruposLogic(Pageable pageable);

}
