package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Perfil;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Perfil entity.
 */
public interface PerfilSearchRepository extends ElasticsearchRepository<Perfil, Long> {
}
