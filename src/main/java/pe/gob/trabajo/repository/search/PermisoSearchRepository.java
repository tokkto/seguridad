package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Permiso;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Permiso entity.
 */
public interface PermisoSearchRepository extends ElasticsearchRepository<Permiso, Long> {
}
