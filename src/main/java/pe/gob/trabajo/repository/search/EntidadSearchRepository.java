package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Entidad;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Entidad entity.
 */
public interface EntidadSearchRepository extends ElasticsearchRepository<Entidad, Long> {
}
