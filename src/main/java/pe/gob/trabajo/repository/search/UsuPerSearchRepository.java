package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.UsuPer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UsuPer entity.
 */
public interface UsuPerSearchRepository extends ElasticsearchRepository<UsuPer, Long> {
}
