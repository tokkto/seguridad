package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.UsuarioGrupo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UsuarioGrupo entity.
 */
public interface UsuarioGrupoSearchRepository extends ElasticsearchRepository<UsuarioGrupo, Long> {
}
