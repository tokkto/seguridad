package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Modulo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Modulo entity.
 */
public interface ModuloSearchRepository extends ElasticsearchRepository<Modulo, Long> {
}
