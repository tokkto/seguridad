package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.MenuPerfil;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MenuPerfil entity.
 */
public interface MenuPerfilSearchRepository extends ElasticsearchRepository<MenuPerfil, Long> {
}
