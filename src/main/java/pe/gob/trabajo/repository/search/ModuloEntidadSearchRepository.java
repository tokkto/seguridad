package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.ModuloEntidad;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ModuloEntidad entity.
 */
public interface ModuloEntidadSearchRepository extends ElasticsearchRepository<ModuloEntidad, Long> {
}
