package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Aplicacion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Aplicacion entity.
 */
public interface AplicacionSearchRepository extends ElasticsearchRepository<Aplicacion, Long> {
}
