package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Grupo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Grupo entity.
 */
public interface GrupoSearchRepository extends ElasticsearchRepository<Grupo, Long> {
}
