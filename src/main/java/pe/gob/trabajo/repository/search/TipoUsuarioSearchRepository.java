package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.TipoUsuario;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TipoUsuario entity.
 */
public interface TipoUsuarioSearchRepository extends ElasticsearchRepository<TipoUsuario, Long> {
}
