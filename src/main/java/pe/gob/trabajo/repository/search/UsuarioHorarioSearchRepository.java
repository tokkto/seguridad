package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.UsuarioHorario;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UsuarioHorario entity.
 */
public interface UsuarioHorarioSearchRepository extends ElasticsearchRepository<UsuarioHorario, Long> {
}
