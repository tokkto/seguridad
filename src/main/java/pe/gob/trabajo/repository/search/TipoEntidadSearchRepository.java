package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.TipoEntidad;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TipoEntidad entity.
 */
public interface TipoEntidadSearchRepository extends ElasticsearchRepository<TipoEntidad, Long> {
}
