package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Perfil;

import pe.gob.trabajo.repository.PerfilRepository;
import pe.gob.trabajo.repository.search.PerfilSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Perfil.
 */
@RestController
@RequestMapping("/api")
public class PerfilResource {

    private final Logger log = LoggerFactory.getLogger(PerfilResource.class);

    private static final String ENTITY_NAME = "perfil";

    private final PerfilRepository perfilRepository;

    private final PerfilSearchRepository perfilSearchRepository;

    public PerfilResource(PerfilRepository perfilRepository, PerfilSearchRepository perfilSearchRepository) {
        this.perfilRepository = perfilRepository;
        this.perfilSearchRepository = perfilSearchRepository;
    }

    /**
     * POST  /perfiles : Create a new perfil.
     *
     * @param perfil the perfil to create
     * @return the ResponseEntity with status 201 (Created) and with body the new perfil, or with status 400 (Bad Request) if the perfil has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/perfiles")
    @Timed
    public ResponseEntity<Perfil> createPerfil(@Valid @RequestBody Perfil perfil) throws URISyntaxException {
        log.debug("REST request to save Perfil : {}", perfil);
        if (perfil.getId() != null) {
            throw new BadRequestAlertException("A new perfil cannot already have an ID", ENTITY_NAME, "idexists");
        }
        perfil.datFechaLog(Instant.now());
        Perfil result = perfilRepository.save(perfil);
        perfilSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/perfiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /perfiles : Updates an existing perfil.
     *
     * @param perfil the perfil to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated perfil,
     * or with status 400 (Bad Request) if the perfil is not valid,
     * or with status 500 (Internal Server Error) if the perfil couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/perfiles")
    @Timed
    public ResponseEntity<Perfil> updatePerfil(@Valid @RequestBody Perfil perfil) throws URISyntaxException {
        log.debug("REST request to update Perfil : {}", perfil);
        if (perfil.getId() == null) {
            return createPerfil(perfil);
        }
        perfil.datFechaLog(Instant.now());
        Perfil result = perfilRepository.save(perfil);
        perfilSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, perfil.getId().toString()))
            .body(result);
    }

    /**
     * GET  /perfiles : get all the perfiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of perfiles in body
     */
    @GetMapping("/perfiles")
    @Timed
    public ResponseEntity<List<Perfil>> getAllPerfils(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Perfils");
        Page<Perfil> page = perfilRepository.findAllPerfilLogic(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/perfiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /perfiles/:id : get the "id" perfil.
     *
     * @param id the id of the perfil to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the perfil, or with status 404 (Not Found)
     */
    @GetMapping("/perfiles/{id}")
    @Timed
    public ResponseEntity<Perfil> getPerfil(@PathVariable Long id) {
        log.debug("REST request to get Perfil : {}", id);
        Perfil perfil = perfilRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(perfil));
    }

    /**
     * DELETE  /perfiles/:id : delete the "id" perfil.
     *
     * @param id the id of the perfil to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/perfiles/{id}")
    @Timed
    public ResponseEntity<Void> deletePerfil(@PathVariable Long id) {
        log.debug("REST request to delete Perfil : {}", id);
        perfilRepository.delete(id);
        perfilSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/perfiles?query=:query : search for the perfil corresponding
     * to the query.
     *
     * @param query the query of the perfil search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/perfiles")
    @Timed
    public ResponseEntity<List<Perfil>> searchPerfils(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Perfils for query {}", query);
        Page<Perfil> page = perfilSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/perfiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
