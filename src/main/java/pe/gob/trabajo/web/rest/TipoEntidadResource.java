package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.TipoEntidad;

import pe.gob.trabajo.repository.TipoEntidadRepository;
import pe.gob.trabajo.repository.search.TipoEntidadSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TipoEntidad.
 */
@RestController
@RequestMapping("/api")
public class TipoEntidadResource {

    private final Logger log = LoggerFactory.getLogger(TipoEntidadResource.class);

    private static final String ENTITY_NAME = "tipoEntidad";

    private final TipoEntidadRepository tipoEntidadRepository;

    private final TipoEntidadSearchRepository tipoEntidadSearchRepository;

    public TipoEntidadResource(TipoEntidadRepository tipoEntidadRepository, TipoEntidadSearchRepository tipoEntidadSearchRepository) {
        this.tipoEntidadRepository = tipoEntidadRepository;
        this.tipoEntidadSearchRepository = tipoEntidadSearchRepository;
    }

    /**
     * POST  /tipo-entidades : Create a new tipoEntidad.
     *
     * @param tipoEntidad the tipoEntidad to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoEntidad, or with status 400 (Bad Request) if the tipoEntidad has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-entidades")
    @Timed
    public ResponseEntity<TipoEntidad> createTipoEntidad(@Valid @RequestBody TipoEntidad tipoEntidad) throws URISyntaxException {
        log.debug("REST request to save TipoEntidad : {}", tipoEntidad);
        if (tipoEntidad.getId() != null) {
            throw new BadRequestAlertException("A new tipoEntidad cannot already have an ID", ENTITY_NAME, "idexists");
        }
        tipoEntidad.datFechaLog(Instant.now());
        TipoEntidad result = tipoEntidadRepository.save(tipoEntidad);
        tipoEntidadSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipo-entidades/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-entidades : Updates an existing tipoEntidad.
     *
     * @param tipoEntidad the tipoEntidad to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoEntidad,
     * or with status 400 (Bad Request) if the tipoEntidad is not valid,
     * or with status 500 (Internal Server Error) if the tipoEntidad couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-entidades")
    @Timed
    public ResponseEntity<TipoEntidad> updateTipoEntidad(@Valid @RequestBody TipoEntidad tipoEntidad) throws URISyntaxException {
        log.debug("REST request to update TipoEntidad : {}", tipoEntidad);
        if (tipoEntidad.getId() == null) {
            return createTipoEntidad(tipoEntidad);
        }
        tipoEntidad.datFechaLog(Instant.now());
        TipoEntidad result = tipoEntidadRepository.save(tipoEntidad);
        tipoEntidadSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoEntidad.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipo-entidades : get all the tipoEntidads.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tipoEntidads in body
     */
    @GetMapping("/tipo-entidades")
    @Timed
    public ResponseEntity<List<TipoEntidad>> getAllTipoEntidads(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TipoEntidads");
        Page<TipoEntidad> page = tipoEntidadRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tipo-entidades");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tipo-entidades/:id : get the "id" tipoEntidad.
     *
     * @param id the id of the tipoEntidad to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipoEntidad, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-entidades/{id}")
    @Timed
    public ResponseEntity<TipoEntidad> getTipoEntidad(@PathVariable Long id) {
        log.debug("REST request to get TipoEntidad : {}", id);
        TipoEntidad tipoEntidad = tipoEntidadRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoEntidad));
    }

    /**
     * DELETE  /tipo-entidades/:id : delete the "id" tipoEntidad.
     *
     * @param id the id of the tipoEntidad to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-entidades/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipoEntidad(@PathVariable Long id) {
        log.debug("REST request to delete TipoEntidad : {}", id);
        tipoEntidadRepository.delete(id);
        tipoEntidadSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipo-entidades?query=:query : search for the tipoEntidad corresponding
     * to the query.
     *
     * @param query the query of the tipoEntidad search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/tipo-entidades")
    @Timed
    public ResponseEntity<List<TipoEntidad>> searchTipoEntidads(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of TipoEntidads for query {}", query);
        Page<TipoEntidad> page = tipoEntidadSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tipo-entidades");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
