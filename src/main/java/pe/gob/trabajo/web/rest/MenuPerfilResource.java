package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.MenuPerfil;

import pe.gob.trabajo.repository.MenuPerfilRepository;
import pe.gob.trabajo.repository.search.MenuPerfilSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MenuPerfil.
 */
@RestController
@RequestMapping("/api")
public class MenuPerfilResource {

    private final Logger log = LoggerFactory.getLogger(MenuPerfilResource.class);

    private static final String ENTITY_NAME = "menuPerfil";

    private final MenuPerfilRepository menuPerfilRepository;

    private final MenuPerfilSearchRepository menuPerfilSearchRepository;

    public MenuPerfilResource(MenuPerfilRepository menuPerfilRepository, MenuPerfilSearchRepository menuPerfilSearchRepository) {
        this.menuPerfilRepository = menuPerfilRepository;
        this.menuPerfilSearchRepository = menuPerfilSearchRepository;
    }

    /**
     * POST  /menu-perfiles : Create a new menuPerfil.
     *
     * @param menuPerfil the menuPerfil to create
     * @return the ResponseEntity with status 201 (Created) and with body the new menuPerfil, or with status 400 (Bad Request) if the menuPerfil has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/menu-perfiles")
    @Timed
    public ResponseEntity<MenuPerfil> createMenuPerfil(@Valid @RequestBody MenuPerfil menuPerfil) throws URISyntaxException {
        log.debug("REST request to save MenuPerfil : {}", menuPerfil);
        if (menuPerfil.getId() != null) {
            throw new BadRequestAlertException("A new menuPerfil cannot already have an ID", ENTITY_NAME, "idexists");
        }
        menuPerfil.datFechaLog(Instant.now());
        MenuPerfil result = menuPerfilRepository.save(menuPerfil);
        menuPerfilSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/menu-perfiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /menu-perfiles : Updates an existing menuPerfil.
     *
     * @param menuPerfil the menuPerfil to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated menuPerfil,
     * or with status 400 (Bad Request) if the menuPerfil is not valid,
     * or with status 500 (Internal Server Error) if the menuPerfil couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/menu-perfiles")
    @Timed
    public ResponseEntity<MenuPerfil> updateMenuPerfil(@Valid @RequestBody MenuPerfil menuPerfil) throws URISyntaxException {
        log.debug("REST request to update MenuPerfil : {}", menuPerfil);
        if (menuPerfil.getId() == null) {
            return createMenuPerfil(menuPerfil);
        }
        menuPerfil.datFechaLog(Instant.now());
        MenuPerfil result = menuPerfilRepository.save(menuPerfil);
        menuPerfilSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, menuPerfil.getId().toString()))
            .body(result);
    }

    /**
     * GET  /menu-perfiles : get all the menuPerfils.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of menuPerfils in body
     */
    @GetMapping("/menu-perfiles")
    @Timed
    public ResponseEntity<List<MenuPerfil>> getAllMenuPerfils(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MenuPerfils");
        Page<MenuPerfil> page = menuPerfilRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/menu-perfiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /menu-perfiles/:id : get the "id" menuPerfil.
     *
     * @param id the id of the menuPerfil to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuPerfil, or with status 404 (Not Found)
     */
    @GetMapping("/menu-perfiles/{id}")
    @Timed
    public ResponseEntity<MenuPerfil> getMenuPerfil(@PathVariable Long id) {
        log.debug("REST request to get MenuPerfil : {}", id);
        MenuPerfil menuPerfil = menuPerfilRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(menuPerfil));
    }

    /**
     * DELETE  /menu-perfiles/:id : delete the "id" menuPerfil.
     *
     * @param id the id of the menuPerfil to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/menu-perfiles/{id}")
    @Timed
    public ResponseEntity<Void> deleteMenuPerfil(@PathVariable Long id) {
        log.debug("REST request to delete MenuPerfil : {}", id);
        menuPerfilRepository.delete(id);
        menuPerfilSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/menu-perfiles?query=:query : search for the menuPerfil corresponding
     * to the query.
     *
     * @param query the query of the menuPerfil search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/menu-perfiles")
    @Timed
    public ResponseEntity<List<MenuPerfil>> searchMenuPerfils(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of MenuPerfils for query {}", query);
        Page<MenuPerfil> page = menuPerfilSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/menu-perfiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
