package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Permiso;

import pe.gob.trabajo.repository.PermisoRepository;
import pe.gob.trabajo.repository.search.PermisoSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Permiso.
 */
@RestController
@RequestMapping("/api")
public class PermisoResource {

    private final Logger log = LoggerFactory.getLogger(PermisoResource.class);

    private static final String ENTITY_NAME = "permiso";

    private final PermisoRepository permisoRepository;

    private final PermisoSearchRepository permisoSearchRepository;

    public PermisoResource(PermisoRepository permisoRepository, PermisoSearchRepository permisoSearchRepository) {
        this.permisoRepository = permisoRepository;
        this.permisoSearchRepository = permisoSearchRepository;
    }

    /**
     * POST  /permisos : Create a new permiso.
     *
     * @param permiso the permiso to create
     * @return the ResponseEntity with status 201 (Created) and with body the new permiso, or with status 400 (Bad Request) if the permiso has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/permisos")
    @Timed
    public ResponseEntity<Permiso> createPermiso(@Valid @RequestBody Permiso permiso) throws URISyntaxException {
        log.debug("REST request to save Permiso : {}", permiso);
        if (permiso.getId() != null) {
            throw new BadRequestAlertException("A new permiso cannot already have an ID", ENTITY_NAME, "idexists");
        }
        permiso.datFechaLog(Instant.now());
        Permiso result = permisoRepository.save(permiso);
        permisoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/permisos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /permisos : Updates an existing permiso.
     *
     * @param permiso the permiso to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated permiso,
     * or with status 400 (Bad Request) if the permiso is not valid,
     * or with status 500 (Internal Server Error) if the permiso couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/permisos")
    @Timed
    public ResponseEntity<Permiso> updatePermiso(@Valid @RequestBody Permiso permiso) throws URISyntaxException {
        log.debug("REST request to update Permiso : {}", permiso);
        if (permiso.getId() == null) {
            return createPermiso(permiso);
        }
        permiso.datFechaLog(Instant.now());
        Permiso result = permisoRepository.save(permiso);
        permisoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, permiso.getId().toString()))
            .body(result);
    }

    /**
     * GET  /permisos : get all the permisos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of permisos in body
     */
    @GetMapping("/permisos")
    @Timed
    public ResponseEntity<List<Permiso>> getAllPermisos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Permisos");
        Page<Permiso> page = permisoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/permisos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /permisos/:id : get the "id" permiso.
     *
     * @param id the id of the permiso to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the permiso, or with status 404 (Not Found)
     */
    @GetMapping("/permisos/{id}")
    @Timed
    public ResponseEntity<Permiso> getPermiso(@PathVariable Long id) {
        log.debug("REST request to get Permiso : {}", id);
        Permiso permiso = permisoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(permiso));
    }

    /**
     * DELETE  /permisos/:id : delete the "id" permiso.
     *
     * @param id the id of the permiso to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/permisos/{id}")
    @Timed
    public ResponseEntity<Void> deletePermiso(@PathVariable Long id) {
        log.debug("REST request to delete Permiso : {}", id);
        permisoRepository.delete(id);
        permisoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/permisos?query=:query : search for the permiso corresponding
     * to the query.
     *
     * @param query the query of the permiso search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/permisos")
    @Timed
    public ResponseEntity<List<Permiso>> searchPermisos(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Permisos for query {}", query);
        Page<Permiso> page = permisoSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/permisos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
