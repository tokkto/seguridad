package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.TipoUsuario;

import pe.gob.trabajo.repository.TipoUsuarioRepository;
import pe.gob.trabajo.repository.search.TipoUsuarioSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TipoUsuario.
 */
@RestController
@RequestMapping("/api")
public class TipoUsuarioResource {

    private final Logger log = LoggerFactory.getLogger(TipoUsuarioResource.class);

    private static final String ENTITY_NAME = "tipoUsuario";

    private final TipoUsuarioRepository tipoUsuarioRepository;

    private final TipoUsuarioSearchRepository tipoUsuarioSearchRepository;

    public TipoUsuarioResource(TipoUsuarioRepository tipoUsuarioRepository, TipoUsuarioSearchRepository tipoUsuarioSearchRepository) {
        this.tipoUsuarioRepository = tipoUsuarioRepository;
        this.tipoUsuarioSearchRepository = tipoUsuarioSearchRepository;
    }

    /**
     * POST  /tipo-usuarios : Create a new tipoUsuario.
     *
     * @param tipoUsuario the tipoUsuario to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoUsuario, or with status 400 (Bad Request) if the tipoUsuario has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-usuarios")
    @Timed
    public ResponseEntity<TipoUsuario> createTipoUsuario(@Valid @RequestBody TipoUsuario tipoUsuario) throws URISyntaxException {
        log.debug("REST request to save TipoUsuario : {}", tipoUsuario);
        if (tipoUsuario.getId() != null) {
            throw new BadRequestAlertException("A new tipoUsuario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        tipoUsuario.datFechaLog(Instant.now());
        TipoUsuario result = tipoUsuarioRepository.save(tipoUsuario);
        tipoUsuarioSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipo-usuarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-usuarios : Updates an existing tipoUsuario.
     *
     * @param tipoUsuario the tipoUsuario to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoUsuario,
     * or with status 400 (Bad Request) if the tipoUsuario is not valid,
     * or with status 500 (Internal Server Error) if the tipoUsuario couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-usuarios")
    @Timed
    public ResponseEntity<TipoUsuario> updateTipoUsuario(@Valid @RequestBody TipoUsuario tipoUsuario) throws URISyntaxException {
        log.debug("REST request to update TipoUsuario : {}", tipoUsuario);
        if (tipoUsuario.getId() == null) {
            return createTipoUsuario(tipoUsuario);
        }
        tipoUsuario.datFechaLog(Instant.now());
        TipoUsuario result = tipoUsuarioRepository.save(tipoUsuario);
        tipoUsuarioSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoUsuario.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipo-usuarios : get all the tipoUsuarios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tipoUsuarios in body
     */
    @GetMapping("/tipo-usuarios")
    @Timed
    public ResponseEntity<List<TipoUsuario>> getAllTipoUsuarios(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TipoUsuarios");
        Page<TipoUsuario> page = tipoUsuarioRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tipo-usuarios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tipo-usuarios/:id : get the "id" tipoUsuario.
     *
     * @param id the id of the tipoUsuario to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipoUsuario, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-usuarios/{id}")
    @Timed
    public ResponseEntity<TipoUsuario> getTipoUsuario(@PathVariable Long id) {
        log.debug("REST request to get TipoUsuario : {}", id);
        TipoUsuario tipoUsuario = tipoUsuarioRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoUsuario));
    }

    /**
     * DELETE  /tipo-usuarios/:id : delete the "id" tipoUsuario.
     *
     * @param id the id of the tipoUsuario to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-usuarios/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipoUsuario(@PathVariable Long id) {
        log.debug("REST request to delete TipoUsuario : {}", id);
        tipoUsuarioRepository.delete(id);
        tipoUsuarioSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipo-usuarios?query=:query : search for the tipoUsuario corresponding
     * to the query.
     *
     * @param query the query of the tipoUsuario search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/tipo-usuarios")
    @Timed
    public ResponseEntity<List<TipoUsuario>> searchTipoUsuarios(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of TipoUsuarios for query {}", query);
        Page<TipoUsuario> page = tipoUsuarioSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tipo-usuarios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
