package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Departamento;

import pe.gob.trabajo.repository.DepartamentoRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Departamento.
 */
@RestController
@RequestMapping("/api")
public class DepartamentoResource {

    private final Logger log = LoggerFactory.getLogger(DepartamentoResource.class);

    private static final String ENTITY_NAME = "departamento";

    private final DepartamentoRepository departamentoRepository;
    public DepartamentoResource(DepartamentoRepository departamentoRepository) {
        this.departamentoRepository = departamentoRepository;
    }

    /**
     * GET  /departamentos : get all the departamentos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of departamentos in body
     */
    @GetMapping("/departamentos")
    @Timed
    public ResponseEntity<List<Departamento>> getAllDepartamentos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of departamentos");
        List<Departamento> page = departamentoRepository.findAllDepartamentos();
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /departamentos/:id : get the "id" aplicacion.
     *
     * @param id the id of the aplicacion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aplicacion, or with status 404 (Not Found)
     */
    @GetMapping("/departamentos/{id}")
    @Timed
    public ResponseEntity<Departamento> getDepartamento(@PathVariable String id) {
        log.debug("REST request to get Aplicacion : {}", id);
        Departamento departamento = departamentoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(departamento));
    }
}
