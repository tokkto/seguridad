package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Entidad;

import pe.gob.trabajo.repository.EntidadRepository;
import pe.gob.trabajo.repository.search.EntidadSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Entidad.
 */
@RestController
@RequestMapping("/api")
public class EntidadResource {

    private final Logger log = LoggerFactory.getLogger(EntidadResource.class);

    private static final String ENTITY_NAME = "entidad";

    private final EntidadRepository entidadRepository;

    private final EntidadSearchRepository entidadSearchRepository;

    public EntidadResource(EntidadRepository entidadRepository, EntidadSearchRepository entidadSearchRepository) {
        this.entidadRepository = entidadRepository;
        this.entidadSearchRepository = entidadSearchRepository;
    }

    /**
     * POST  /entidades : Create a new entidad.
     *
     * @param entidad the entidad to create
     * @return the ResponseEntity with status 201 (Created) and with body the new entidad, or with status 400 (Bad Request) if the entidad has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/entidades")
    @Timed
    public ResponseEntity<Entidad> createEntidad(@Valid @RequestBody Entidad entidad) throws URISyntaxException {
        log.debug("REST request to save Entidad : {}", entidad);
        if (entidad.getId() != null) {
            throw new BadRequestAlertException("A new entidad cannot already have an ID", ENTITY_NAME, "idexists");
        }
        entidad.datFechaLog(Instant.now());
        Entidad result = entidadRepository.save(entidad);
        entidadSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/entidades/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /entidades : Updates an existing entidad.
     *
     * @param entidad the entidad to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated entidad,
     * or with status 400 (Bad Request) if the entidad is not valid,
     * or with status 500 (Internal Server Error) if the entidad couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/entidades")
    @Timed
    public ResponseEntity<Entidad> updateEntidad(@Valid @RequestBody Entidad entidad) throws URISyntaxException {
        log.debug("REST request to update Entidad : {}", entidad);
        if (entidad.getId() == null) {
            return createEntidad(entidad);
        }
        entidad.datFechaLog(Instant.now());
        Entidad result = entidadRepository.save(entidad);
        entidadSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, entidad.getId().toString()))
            .body(result);
    }

    /**
     * GET  /entidades : get all the entidades.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of entidades in body
     */
    @GetMapping("/entidades")
    @Timed
    public ResponseEntity<List<Entidad>> getAllEntidads(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Entidads");
        Page<Entidad> page = entidadRepository.findAllEntitiesLogic(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/entidades");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /entidades/:id : get the "id" entidad.
     *
     * @param id the id of the entidad to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the entidad, or with status 404 (Not Found)
     */
    @GetMapping("/entidades/{id}")
    @Timed
    public ResponseEntity<Entidad> getEntidad(@PathVariable Long id) {
        log.debug("REST request to get Entidad : {}", id);
        Entidad entidad = entidadRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(entidad));
    }

    /**
     * DELETE  /entidades/:id : delete the "id" entidad.
     *
     * @param id the id of the entidad to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/entidades/{id}")
    @Timed
    public ResponseEntity<Void> deleteEntidad(@PathVariable Long id) {
        log.debug("REST request to delete Entidad : {}", id);
        entidadRepository.delete(id);
        entidadSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/entidades?query=:query : search for the entidad corresponding
     * to the query.
     *
     * @param query the query of the entidad search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/entidades")
    @Timed
    public ResponseEntity<List<Entidad>> searchEntidads(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Entidads for query {}", query);
        Page<Entidad> page = entidadSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/entidades");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
