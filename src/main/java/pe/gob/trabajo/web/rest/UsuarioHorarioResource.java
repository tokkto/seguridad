package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.UsuarioHorario;

import pe.gob.trabajo.repository.UsuarioHorarioRepository;
import pe.gob.trabajo.repository.search.UsuarioHorarioSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UsuarioHorario.
 */
@RestController
@RequestMapping("/api")
public class UsuarioHorarioResource {

    private final Logger log = LoggerFactory.getLogger(UsuarioHorarioResource.class);

    private static final String ENTITY_NAME = "usuarioHorario";

    private final UsuarioHorarioRepository usuarioHorarioRepository;

    private final UsuarioHorarioSearchRepository usuarioHorarioSearchRepository;

    public UsuarioHorarioResource(UsuarioHorarioRepository usuarioHorarioRepository, UsuarioHorarioSearchRepository usuarioHorarioSearchRepository) {
        this.usuarioHorarioRepository = usuarioHorarioRepository;
        this.usuarioHorarioSearchRepository = usuarioHorarioSearchRepository;
    }

    /**
     * POST  /usuario-horarios : Create a new usuarioHorario.
     *
     * @param usuarioHorario the usuarioHorario to create
     * @return the ResponseEntity with status 201 (Created) and with body the new usuarioHorario, or with status 400 (Bad Request) if the usuarioHorario has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/usuario-horarios")
    @Timed
    public ResponseEntity<UsuarioHorario> createUsuarioHorario(@Valid @RequestBody UsuarioHorario usuarioHorario) throws URISyntaxException {
        log.debug("REST request to save UsuarioHorario : {}", usuarioHorario);
        if (usuarioHorario.getId() != null) {
            throw new BadRequestAlertException("A new usuarioHorario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        usuarioHorario.datFechaLog(Instant.now());
        UsuarioHorario result = usuarioHorarioRepository.save(usuarioHorario);
        usuarioHorarioSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/usuario-horarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /usuario-horarios : Updates an existing usuarioHorario.
     *
     * @param usuarioHorario the usuarioHorario to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated usuarioHorario,
     * or with status 400 (Bad Request) if the usuarioHorario is not valid,
     * or with status 500 (Internal Server Error) if the usuarioHorario couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/usuario-horarios")
    @Timed
    public ResponseEntity<UsuarioHorario> updateUsuarioHorario(@Valid @RequestBody UsuarioHorario usuarioHorario) throws URISyntaxException {
        log.debug("REST request to update UsuarioHorario : {}", usuarioHorario);
        if (usuarioHorario.getId() == null) {
            return createUsuarioHorario(usuarioHorario);
        }
        usuarioHorario.datFechaLog(Instant.now());
        UsuarioHorario result = usuarioHorarioRepository.save(usuarioHorario);
        usuarioHorarioSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, usuarioHorario.getId().toString()))
            .body(result);
    }


    /**
     * GET  /usuario-horarios : get all the usuarioHorarios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of usuarioHorarios in body
     */
    @GetMapping("/usuario-horarios")
    @Timed
    public ResponseEntity<List<UsuarioHorario>> getAllUsuarioHorarios(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UsuarioHorarios");
        Page<UsuarioHorario> page = usuarioHorarioRepository.findAllUsuarioHorariosLogic(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/usuario-horarios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /usuario-horarios/:id : get the "id" usuarioHorario.
     *
     * @param id the id of the usuarioHorario to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the usuarioHorario, or with status 404 (Not Found)
     */
    @GetMapping("/usuario-horarios/{id}")
    @Timed
    public ResponseEntity<UsuarioHorario> getUsuarioHorario(@PathVariable Long id) {
        log.debug("REST request to get UsuarioHorario : {}", id);
        UsuarioHorario usuarioHorario = usuarioHorarioRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(usuarioHorario));
    }


    /**
     * GET  /usuario-horarios/tipo/:id : get the "id" usuarioHorario.
     *
     * @param id the id of the usuarioHorario to retrieve
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of usuarioHorarios, or with status 404 (Not Found)
     */
    @GetMapping("/usuario-horarios/tipo/{id}")
    @Timed
    public ResponseEntity<List<UsuarioHorario>> getUsuarioHorarioById(@PathVariable Long id, @ApiParam Pageable pageable) {
        log.debug("REST request to get UsuarioHorarioById : {}", id);
        //UsuarioHorario usuarioHorario = usuarioHorarioRepository.findOne(id);
        List<UsuarioHorario> page = usuarioHorarioRepository.findByUser(id);
        log.debug("REST request to get data UsuarioHorarioById : {}", page);
        return new ResponseEntity<>(page, HttpStatus.OK);
        //return ResponseUtil.wrapOrNotFound(Optional.ofNullable(usuarioHorario));        
    }

    /**
     * DELETE  /usuario-horarios/:id : delete the "id" usuarioHorario.
     *
     * @param id the id of the usuarioHorario to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/usuario-horarios/{id}")
    @Timed
    public ResponseEntity<Void> deleteUsuarioHorario(@PathVariable Long id) {
        log.debug("REST request to delete UsuarioHorario : {}", id);
        usuarioHorarioRepository.delete(id);
        usuarioHorarioSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/usuario-horarios?query=:query : search for the usuarioHorario corresponding
     * to the query.
     *
     * @param query the query of the usuarioHorario search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/usuario-horarios")
    @Timed
    public ResponseEntity<List<UsuarioHorario>> searchUsuarioHorarios(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UsuarioHorarios for query {}", query);
        Page<UsuarioHorario> page = usuarioHorarioSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/usuario-horarios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
