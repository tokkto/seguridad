package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Distrito;

import pe.gob.trabajo.repository.DistritoRepository;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Distrito.
 */
@RestController
@RequestMapping("/api")
public class DistritoResource {

    private final Logger log = LoggerFactory.getLogger(DistritoResource.class);

    private static final String ENTITY_NAME = "distrito";

    private final DistritoRepository distritoRepository;
    public DistritoResource(DistritoRepository distritoRepository) {
        this.distritoRepository = distritoRepository;
    }

    /**
     * GET  /distritos : get all the distritos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of distritos in body
     */
    @GetMapping("/distritos")
    @Timed
    public ResponseEntity<List<Distrito>> getAllDistritos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Distritos");
        List<Distrito> page = distritoRepository.findAll();
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    /**
     * GET  /departamentos/:id : get the "id" aplicacion.
     *
     * @param id the id of the aplicacion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aplicacion, or with status 404 (Not Found)
     */
    @GetMapping("/distritos/{id}/{idprov}")
    @Timed
    public ResponseEntity<List<Distrito>> getDistrito(@PathVariable String id, @PathVariable String idprov) {
        log.debug("REST request to get Aplicacion : {}", id);
        List<Distrito> page = distritoRepository.findFilterDistritos(id,idprov);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }
}
