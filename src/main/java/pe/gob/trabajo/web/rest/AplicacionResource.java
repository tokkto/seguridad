package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Aplicacion;

import pe.gob.trabajo.repository.AplicacionRepository;
import pe.gob.trabajo.repository.search.AplicacionSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Aplicacion.
 */
@RestController
@RequestMapping("/api")
public class AplicacionResource {

    private final Logger log = LoggerFactory.getLogger(AplicacionResource.class);

    private static final String ENTITY_NAME = "aplicacion";

    private final AplicacionRepository aplicacionRepository;

    private final AplicacionSearchRepository aplicacionSearchRepository;

    public AplicacionResource(AplicacionRepository aplicacionRepository, AplicacionSearchRepository aplicacionSearchRepository) {
        this.aplicacionRepository = aplicacionRepository;
        this.aplicacionSearchRepository = aplicacionSearchRepository;
    }

    /**
     * POST  /aplicaciones : Create a new aplicacion.
     *
     * @param aplicacion the aplicacion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new aplicacion, or with status 400 (Bad Request) if the aplicacion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/aplicaciones")
    @Timed
    public ResponseEntity<Aplicacion> createAplicacion(@Valid @RequestBody Aplicacion aplicacion) throws URISyntaxException {
        log.debug("REST request to save Aplicacion : {}", aplicacion);
        if (aplicacion.getId() != null) {
            throw new BadRequestAlertException("A new aplicacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        aplicacion.datFechaLog(Instant.now());
        Aplicacion result = aplicacionRepository.save(aplicacion);
        aplicacionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/aplicaciones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /aplicaciones : Updates an existing aplicacion.
     *
     * @param aplicacion the aplicacion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated aplicacion,
     * or with status 400 (Bad Request) if the aplicacion is not valid,
     * or with status 500 (Internal Server Error) if the aplicacion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/aplicaciones")
    @Timed
    public ResponseEntity<Aplicacion> updateAplicacion(@Valid @RequestBody Aplicacion aplicacion) throws URISyntaxException {
        log.debug("REST request to update Aplicacion : {}", aplicacion);
        if (aplicacion.getId() == null) {
            return createAplicacion(aplicacion);
        }
        aplicacion.datFechaLog(Instant.now());
        Aplicacion result = aplicacionRepository.save(aplicacion);
        aplicacionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, aplicacion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /aplicaciones : get all the aplicaciones.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of aplicaciones in body
     */
    @GetMapping("/aplicaciones")
    @Timed
    public ResponseEntity<List<Aplicacion>> getAllAplicacionsLo(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Aplicacions");
        Page<Aplicacion> page = aplicacionRepository.findAllAplicationLogic(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/aplicaciones");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /aplicaciones/:id : get the "id" aplicacion.
     *
     * @param id the id of the aplicacion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aplicacion, or with status 404 (Not Found)
     */
    @GetMapping("/aplicaciones/{id}")
    @Timed
    public ResponseEntity<Aplicacion> getAplicacion(@PathVariable Long id) {
        log.debug("REST request to get Aplicacion : {}", id);
        Aplicacion aplicacion = aplicacionRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(aplicacion));
    }

    /**
     * DELETE  /aplicaciones/:id : delete the "id" aplicacion.
     *
     * @param id the id of the aplicacion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/aplicaciones/{id}")
    @Timed
    public ResponseEntity<Void> deleteAplicacion(@PathVariable Long id) {
        log.debug("REST request to delete Aplicacion : {}", id);
        aplicacionRepository.delete(id);
        aplicacionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/aplicaciones?query=:query : search for the aplicacion corresponding
     * to the query.
     *
     * @param query the query of the aplicacion search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/aplicaciones")
    @Timed
    public ResponseEntity<List<Aplicacion>> searchAplicacions(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Aplicacions for query {}", query);
        Page<Aplicacion> page = aplicacionSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/aplicaciones");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
