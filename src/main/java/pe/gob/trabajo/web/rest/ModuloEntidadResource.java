package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.ModuloEntidad;

import pe.gob.trabajo.repository.ModuloEntidadRepository;
import pe.gob.trabajo.repository.search.ModuloEntidadSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ModuloEntidad.
 */
@RestController
@RequestMapping("/api")
public class ModuloEntidadResource {

    private final Logger log = LoggerFactory.getLogger(ModuloEntidadResource.class);

    private static final String ENTITY_NAME = "moduloEntidad";

    private final ModuloEntidadRepository moduloEntidadRepository;

    private final ModuloEntidadSearchRepository moduloEntidadSearchRepository;

    public ModuloEntidadResource(ModuloEntidadRepository moduloEntidadRepository, ModuloEntidadSearchRepository moduloEntidadSearchRepository) {
        this.moduloEntidadRepository = moduloEntidadRepository;
        this.moduloEntidadSearchRepository = moduloEntidadSearchRepository;
    }

    /**
     * POST  /modulo-entidades : Create a new moduloEntidad.
     *
     * @param moduloEntidad the moduloEntidad to create
     * @return the ResponseEntity with status 201 (Created) and with body the new moduloEntidad, or with status 400 (Bad Request) if the moduloEntidad has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/modulo-entidades")
    @Timed
    public ResponseEntity<ModuloEntidad> createModuloEntidad(@Valid @RequestBody ModuloEntidad moduloEntidad) throws URISyntaxException {
        log.debug("REST request to save ModuloEntidad : {}", moduloEntidad);
        if (moduloEntidad.getId() != null) {
            throw new BadRequestAlertException("A new moduloEntidad cannot already have an ID", ENTITY_NAME, "idexists");
        }
        moduloEntidad.datFechaLog(Instant.now());
        ModuloEntidad result = moduloEntidadRepository.save(moduloEntidad);
        moduloEntidadSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/modulo-entidades/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /modulo-entidades : Updates an existing moduloEntidad.
     *
     * @param moduloEntidad the moduloEntidad to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated moduloEntidad,
     * or with status 400 (Bad Request) if the moduloEntidad is not valid,
     * or with status 500 (Internal Server Error) if the moduloEntidad couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/modulo-entidades")
    @Timed
    public ResponseEntity<ModuloEntidad> updateModuloEntidad(@Valid @RequestBody ModuloEntidad moduloEntidad) throws URISyntaxException {
        log.debug("REST request to update ModuloEntidad : {}", moduloEntidad);
        if (moduloEntidad.getId() == null) {
            return createModuloEntidad(moduloEntidad);
        }
        moduloEntidad.datFechaLog(Instant.now());
        ModuloEntidad result = moduloEntidadRepository.save(moduloEntidad);
        moduloEntidadSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, moduloEntidad.getId().toString()))
            .body(result);
    }

    /**
     * GET  /modulo-entidades : get all the moduloEntidads.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of moduloEntidads in body
     */
    @GetMapping("/modulo-entidades")
    @Timed
    public ResponseEntity<List<ModuloEntidad>> getAllModuloEntidads(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ModuloEntidads");
        Page<ModuloEntidad> page = moduloEntidadRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/modulo-entidades");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /modulo-entidades/:id : get the "id" moduloEntidad.
     *
     * @param id the id of the moduloEntidad to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the moduloEntidad, or with status 404 (Not Found)
     */
    @GetMapping("/modulo-entidades/{id}")
    @Timed
    public ResponseEntity<ModuloEntidad> getModuloEntidad(@PathVariable Long id) {
        log.debug("REST request to get ModuloEntidad : {}", id);
        ModuloEntidad moduloEntidad = moduloEntidadRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(moduloEntidad));
    }

    /**
     * DELETE  /modulo-entidades/:id : delete the "id" moduloEntidad.
     *
     * @param id the id of the moduloEntidad to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/modulo-entidades/{id}")
    @Timed
    public ResponseEntity<Void> deleteModuloEntidad(@PathVariable Long id) {
        log.debug("REST request to delete ModuloEntidad : {}", id);
        moduloEntidadRepository.delete(id);
        moduloEntidadSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/modulo-entidades?query=:query : search for the moduloEntidad corresponding
     * to the query.
     *
     * @param query the query of the moduloEntidad search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/modulo-entidades")
    @Timed
    public ResponseEntity<List<ModuloEntidad>> searchModuloEntidads(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ModuloEntidads for query {}", query);
        Page<ModuloEntidad> page = moduloEntidadSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/modulo-entidades");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
