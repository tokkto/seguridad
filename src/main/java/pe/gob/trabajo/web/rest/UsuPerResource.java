package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.UsuPer;

import pe.gob.trabajo.repository.UsuPerRepository;
import pe.gob.trabajo.repository.search.UsuPerSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UsuPer.
 */
@RestController
@RequestMapping("/api")
public class UsuPerResource {

    private final Logger log = LoggerFactory.getLogger(UsuPerResource.class);

    private static final String ENTITY_NAME = "usuPer";

    private final UsuPerRepository usuPerRepository;

    private final UsuPerSearchRepository usuPerSearchRepository;

    public UsuPerResource(UsuPerRepository usuPerRepository, UsuPerSearchRepository usuPerSearchRepository) {
        this.usuPerRepository = usuPerRepository;
        this.usuPerSearchRepository = usuPerSearchRepository;
    }

    /**
     * POST  /usu-pers : Create a new usuPer.
     *
     * @param usuPer the usuPer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new usuPer, or with status 400 (Bad Request) if the usuPer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/usu-pers")
    @Timed
    public ResponseEntity<UsuPer> createUsuPer(@Valid @RequestBody UsuPer usuPer) throws URISyntaxException {
        log.debug("REST request to save UsuPer : {}", usuPer);
        if (usuPer.getId() != null) {
            throw new BadRequestAlertException("A new usuPer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        usuPer.datFechaLog(Instant.now());
        UsuPer result = usuPerRepository.save(usuPer);
        usuPerSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/usu-pers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /usu-pers : Updates an existing usuPer.
     *
     * @param usuPer the usuPer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated usuPer,
     * or with status 400 (Bad Request) if the usuPer is not valid,
     * or with status 500 (Internal Server Error) if the usuPer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/usu-pers")
    @Timed
    public ResponseEntity<UsuPer> updateUsuPer(@Valid @RequestBody UsuPer usuPer) throws URISyntaxException {
        log.debug("REST request to update UsuPer : {}", usuPer);
        if (usuPer.getId() == null) {
            return createUsuPer(usuPer);
        }
        usuPer.datFechaLog(Instant.now());
        UsuPer result = usuPerRepository.save(usuPer);
        usuPerSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, usuPer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /usu-pers : get all the usuPers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of usuPers in body
     */
    @GetMapping("/usu-pers")
    @Timed
    public ResponseEntity<List<UsuPer>> getAllUsuPers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UsuPers");
        Page<UsuPer> page = usuPerRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/usu-pers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/usu-pers/usuario/{id}")
    @Timed
    public ResponseEntity<List<UsuPer>> getUsuPerUsuario(@PathVariable Long id) {
        log.debug("REST request to get UsuPer : {}", id);
        List<UsuPer> page = usuPerRepository.findUsuPer(id);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }


    /**
     * GET  /usu-pers/:id : get the "id" usuPer.
     *
     * @param id the id of the usuPer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the usuPer, or with status 404 (Not Found)
     */
    @GetMapping("/usu-pers/{id}")
    @Timed
    public ResponseEntity<UsuPer> getUsuPer(@PathVariable Long id) {
        log.debug("REST request to get UsuPer : {}", id);
        UsuPer usuPer = usuPerRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(usuPer));
    }

    /**
     * DELETE  /usu-pers/:id : delete the "id" usuPer.
     *
     * @param id the id of the usuPer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/usu-pers/{id}")
    @Timed
    public ResponseEntity<Void> deleteUsuPer(@PathVariable Long id) {
        log.debug("REST request to delete UsuPer : {}", id);
        usuPerRepository.delete(id);
        usuPerSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/usu-pers?query=:query : search for the usuPer corresponding
     * to the query.
     *
     * @param query the query of the usuPer search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/usu-pers")
    @Timed
    public ResponseEntity<List<UsuPer>> searchUsuPers(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UsuPers for query {}", query);
        Page<UsuPer> page = usuPerSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/usu-pers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
