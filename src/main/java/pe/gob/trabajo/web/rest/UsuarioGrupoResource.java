package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.UsuarioGrupo;

import pe.gob.trabajo.repository.UsuarioGrupoRepository;
import pe.gob.trabajo.repository.search.UsuarioGrupoSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import pe.gob.trabajo.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UsuarioGrupo.
 */
@RestController
@RequestMapping("/api")
public class UsuarioGrupoResource {

    private final Logger log = LoggerFactory.getLogger(UsuarioGrupoResource.class);

    private static final String ENTITY_NAME = "usuarioGrupo";

    private final UsuarioGrupoRepository usuarioGrupoRepository;

    private final UsuarioGrupoSearchRepository usuarioGrupoSearchRepository;

    public UsuarioGrupoResource(UsuarioGrupoRepository usuarioGrupoRepository, UsuarioGrupoSearchRepository usuarioGrupoSearchRepository) {
        this.usuarioGrupoRepository = usuarioGrupoRepository;
        this.usuarioGrupoSearchRepository = usuarioGrupoSearchRepository;
    }

    /**
     * POST  /usuario-grupos : Create a new usuarioGrupo.
     *
     * @param usuarioGrupo the usuarioGrupo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new usuarioGrupo, or with status 400 (Bad Request) if the usuarioGrupo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/usuario-grupos")
    @Timed
    public ResponseEntity<UsuarioGrupo> createUsuarioGrupo(@Valid @RequestBody UsuarioGrupo usuarioGrupo) throws URISyntaxException {
        log.debug("REST request to save UsuarioGrupo : {}", usuarioGrupo);
        if (usuarioGrupo.getId() != null) {
            throw new BadRequestAlertException("A new usuarioGrupo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        usuarioGrupo.datFechaLog(Instant.now());
        UsuarioGrupo result = usuarioGrupoRepository.save(usuarioGrupo);
        usuarioGrupoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/usuario-grupos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /usuario-grupos : Updates an existing usuarioGrupo.
     *
     * @param usuarioGrupo the usuarioGrupo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated usuarioGrupo,
     * or with status 400 (Bad Request) if the usuarioGrupo is not valid,
     * or with status 500 (Internal Server Error) if the usuarioGrupo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/usuario-grupos")
    @Timed
    public ResponseEntity<UsuarioGrupo> updateUsuarioGrupo(@Valid @RequestBody UsuarioGrupo usuarioGrupo) throws URISyntaxException {
        log.debug("REST request to update UsuarioGrupo : {}", usuarioGrupo);
        if (usuarioGrupo.getId() == null) {
            return createUsuarioGrupo(usuarioGrupo);
        }
        usuarioGrupo.datFechaLog(Instant.now());
        UsuarioGrupo result = usuarioGrupoRepository.save(usuarioGrupo);
        usuarioGrupoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, usuarioGrupo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /usuario-grupos : get all the usuarioGrupos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of usuarioGrupos in body
     */
    @GetMapping("/usuario-grupos")
    @Timed
    public ResponseEntity<List<UsuarioGrupo>> getAllUsuarioGrupos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UsuarioGrupos");
        Page<UsuarioGrupo> page = usuarioGrupoRepository.findAllUsuarioGruposLogic(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/usuario-grupos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /usuario-grupos/:id : get the "id" usuarioGrupo.
     *
     * @param id the id of the usuarioGrupo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the usuarioGrupo, or with status 404 (Not Found)
     */
    @GetMapping("/usuario-grupos/{id}")
    @Timed
    public ResponseEntity<UsuarioGrupo> getUsuarioGrupo(@PathVariable Long id) {
        log.debug("REST request to get UsuarioGrupo : {}", id);
        UsuarioGrupo usuarioGrupo = usuarioGrupoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(usuarioGrupo));
    }

    /**
     * DELETE  /usuario-grupos/:id : delete the "id" usuarioGrupo.
     *
     * @param id the id of the usuarioGrupo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/usuario-grupos/{id}")
    @Timed
    public ResponseEntity<Void> deleteUsuarioGrupo(@PathVariable Long id) {
        log.debug("REST request to delete UsuarioGrupo : {}", id);
        usuarioGrupoRepository.delete(id);
        usuarioGrupoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/usuario-grupos?query=:query : search for the usuarioGrupo corresponding
     * to the query.
     *
     * @param query the query of the usuarioGrupo search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/usuario-grupos")
    @Timed
    public ResponseEntity<List<UsuarioGrupo>> searchUsuarioGrupos(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UsuarioGrupos for query {}", query);
        Page<UsuarioGrupo> page = usuarioGrupoSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/usuario-grupos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
