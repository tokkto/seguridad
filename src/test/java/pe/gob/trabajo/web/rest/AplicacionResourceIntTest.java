package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.Aplicacion;
import pe.gob.trabajo.repository.AplicacionRepository;
import pe.gob.trabajo.repository.search.AplicacionSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AplicacionResource REST controller.
 *
 * @see AplicacionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class AplicacionResourceIntTest {

    private static final String DEFAULT_VAR_NOM_APP = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_APP = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_DESC_APP = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DESC_APP = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_URL_APP = "AAAAAAAAAA";
    private static final String UPDATED_VAR_URL_APP = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_EST_APP = 1;
    private static final Integer UPDATED_NUM_EST_APP = 2;

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private AplicacionRepository aplicacionRepository;

    @Autowired
    private AplicacionSearchRepository aplicacionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAplicacionMockMvc;

    private Aplicacion aplicacion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AplicacionResource aplicacionResource = new AplicacionResource(aplicacionRepository, aplicacionSearchRepository);
        this.restAplicacionMockMvc = MockMvcBuilders.standaloneSetup(aplicacionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Aplicacion createEntity(EntityManager em) {
        Aplicacion aplicacion = new Aplicacion()
            .varNomApp(DEFAULT_VAR_NOM_APP)
            .varDescApp(DEFAULT_VAR_DESC_APP)
            .varUrlApp(DEFAULT_VAR_URL_APP)
            .numEstApp(DEFAULT_NUM_EST_APP)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return aplicacion;
    }

    @Before
    public void initTest() {
        aplicacionSearchRepository.deleteAll();
        aplicacion = createEntity(em);
    }

    @Test
    @Transactional
    public void createAplicacion() throws Exception {
        int databaseSizeBeforeCreate = aplicacionRepository.findAll().size();

        // Create the Aplicacion
        restAplicacionMockMvc.perform(post("/api/aplicacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aplicacion)))
            .andExpect(status().isCreated());

        // Validate the Aplicacion in the database
        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeCreate + 1);
        Aplicacion testAplicacion = aplicacionList.get(aplicacionList.size() - 1);
        assertThat(testAplicacion.getVarNomApp()).isEqualTo(DEFAULT_VAR_NOM_APP);
        assertThat(testAplicacion.getVarDescApp()).isEqualTo(DEFAULT_VAR_DESC_APP);
        assertThat(testAplicacion.getVarUrlApp()).isEqualTo(DEFAULT_VAR_URL_APP);
        assertThat(testAplicacion.getNumEstApp()).isEqualTo(DEFAULT_NUM_EST_APP);
        assertThat(testAplicacion.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testAplicacion.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testAplicacion.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the Aplicacion in Elasticsearch
        Aplicacion aplicacionEs = aplicacionSearchRepository.findOne(testAplicacion.getId());
        assertThat(aplicacionEs).isEqualToComparingFieldByField(testAplicacion);
    }

    @Test
    @Transactional
    public void createAplicacionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aplicacionRepository.findAll().size();

        // Create the Aplicacion with an existing ID
        aplicacion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAplicacionMockMvc.perform(post("/api/aplicacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aplicacion)))
            .andExpect(status().isBadRequest());

        // Validate the Aplicacion in the database
        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = aplicacionRepository.findAll().size();
        // set the field null
        aplicacion.setVarUsuarioLog(null);

        // Create the Aplicacion, which fails.

        restAplicacionMockMvc.perform(post("/api/aplicacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aplicacion)))
            .andExpect(status().isBadRequest());

        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = aplicacionRepository.findAll().size();
        // set the field null
        aplicacion.setDatFechaLog(null);

        // Create the Aplicacion, which fails.

        restAplicacionMockMvc.perform(post("/api/aplicacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aplicacion)))
            .andExpect(status().isBadRequest());

        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = aplicacionRepository.findAll().size();
        // set the field null
        aplicacion.setNumEliminar(null);

        // Create the Aplicacion, which fails.

        restAplicacionMockMvc.perform(post("/api/aplicacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aplicacion)))
            .andExpect(status().isBadRequest());

        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAplicacions() throws Exception {
        // Initialize the database
        aplicacionRepository.saveAndFlush(aplicacion);

        // Get all the aplicacionList
        restAplicacionMockMvc.perform(get("/api/aplicacions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aplicacion.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomApp").value(hasItem(DEFAULT_VAR_NOM_APP.toString())))
            .andExpect(jsonPath("$.[*].varDescApp").value(hasItem(DEFAULT_VAR_DESC_APP.toString())))
            .andExpect(jsonPath("$.[*].varUrlApp").value(hasItem(DEFAULT_VAR_URL_APP.toString())))
            .andExpect(jsonPath("$.[*].numEstApp").value(hasItem(DEFAULT_NUM_EST_APP)))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getAplicacion() throws Exception {
        // Initialize the database
        aplicacionRepository.saveAndFlush(aplicacion);

        // Get the aplicacion
        restAplicacionMockMvc.perform(get("/api/aplicacions/{id}", aplicacion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aplicacion.getId().intValue()))
            .andExpect(jsonPath("$.varNomApp").value(DEFAULT_VAR_NOM_APP.toString()))
            .andExpect(jsonPath("$.varDescApp").value(DEFAULT_VAR_DESC_APP.toString()))
            .andExpect(jsonPath("$.varUrlApp").value(DEFAULT_VAR_URL_APP.toString()))
            .andExpect(jsonPath("$.numEstApp").value(DEFAULT_NUM_EST_APP))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingAplicacion() throws Exception {
        // Get the aplicacion
        restAplicacionMockMvc.perform(get("/api/aplicacions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAplicacion() throws Exception {
        // Initialize the database
        aplicacionRepository.saveAndFlush(aplicacion);
        aplicacionSearchRepository.save(aplicacion);
        int databaseSizeBeforeUpdate = aplicacionRepository.findAll().size();

        // Update the aplicacion
        Aplicacion updatedAplicacion = aplicacionRepository.findOne(aplicacion.getId());
        updatedAplicacion
            .varNomApp(UPDATED_VAR_NOM_APP)
            .varDescApp(UPDATED_VAR_DESC_APP)
            .varUrlApp(UPDATED_VAR_URL_APP)
            .numEstApp(UPDATED_NUM_EST_APP)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restAplicacionMockMvc.perform(put("/api/aplicacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAplicacion)))
            .andExpect(status().isOk());

        // Validate the Aplicacion in the database
        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeUpdate);
        Aplicacion testAplicacion = aplicacionList.get(aplicacionList.size() - 1);
        assertThat(testAplicacion.getVarNomApp()).isEqualTo(UPDATED_VAR_NOM_APP);
        assertThat(testAplicacion.getVarDescApp()).isEqualTo(UPDATED_VAR_DESC_APP);
        assertThat(testAplicacion.getVarUrlApp()).isEqualTo(UPDATED_VAR_URL_APP);
        assertThat(testAplicacion.getNumEstApp()).isEqualTo(UPDATED_NUM_EST_APP);
        assertThat(testAplicacion.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testAplicacion.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testAplicacion.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the Aplicacion in Elasticsearch
        Aplicacion aplicacionEs = aplicacionSearchRepository.findOne(testAplicacion.getId());
        assertThat(aplicacionEs).isEqualToComparingFieldByField(testAplicacion);
    }

    @Test
    @Transactional
    public void updateNonExistingAplicacion() throws Exception {
        int databaseSizeBeforeUpdate = aplicacionRepository.findAll().size();

        // Create the Aplicacion

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAplicacionMockMvc.perform(put("/api/aplicacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aplicacion)))
            .andExpect(status().isCreated());

        // Validate the Aplicacion in the database
        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAplicacion() throws Exception {
        // Initialize the database
        aplicacionRepository.saveAndFlush(aplicacion);
        aplicacionSearchRepository.save(aplicacion);
        int databaseSizeBeforeDelete = aplicacionRepository.findAll().size();

        // Get the aplicacion
        restAplicacionMockMvc.perform(delete("/api/aplicacions/{id}", aplicacion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean aplicacionExistsInEs = aplicacionSearchRepository.exists(aplicacion.getId());
        assertThat(aplicacionExistsInEs).isFalse();

        // Validate the database is empty
        List<Aplicacion> aplicacionList = aplicacionRepository.findAll();
        assertThat(aplicacionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAplicacion() throws Exception {
        // Initialize the database
        aplicacionRepository.saveAndFlush(aplicacion);
        aplicacionSearchRepository.save(aplicacion);

        // Search the aplicacion
        restAplicacionMockMvc.perform(get("/api/_search/aplicacions?query=id:" + aplicacion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aplicacion.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomApp").value(hasItem(DEFAULT_VAR_NOM_APP.toString())))
            .andExpect(jsonPath("$.[*].varDescApp").value(hasItem(DEFAULT_VAR_DESC_APP.toString())))
            .andExpect(jsonPath("$.[*].varUrlApp").value(hasItem(DEFAULT_VAR_URL_APP.toString())))
            .andExpect(jsonPath("$.[*].numEstApp").value(hasItem(DEFAULT_NUM_EST_APP)))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Aplicacion.class);
        Aplicacion aplicacion1 = new Aplicacion();
        aplicacion1.setId(1L);
        Aplicacion aplicacion2 = new Aplicacion();
        aplicacion2.setId(aplicacion1.getId());
        assertThat(aplicacion1).isEqualTo(aplicacion2);
        aplicacion2.setId(2L);
        assertThat(aplicacion1).isNotEqualTo(aplicacion2);
        aplicacion1.setId(null);
        assertThat(aplicacion1).isNotEqualTo(aplicacion2);
    }
}
