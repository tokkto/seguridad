package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.UsuPer;
import pe.gob.trabajo.repository.UsuPerRepository;
import pe.gob.trabajo.repository.search.UsuPerSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UsuPerResource REST controller.
 *
 * @see UsuPerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class UsuPerResourceIntTest {

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private UsuPerRepository usuPerRepository;

    @Autowired
    private UsuPerSearchRepository usuPerSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUsuPerMockMvc;

    private UsuPer usuPer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UsuPerResource usuPerResource = new UsuPerResource(usuPerRepository, usuPerSearchRepository);
        this.restUsuPerMockMvc = MockMvcBuilders.standaloneSetup(usuPerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UsuPer createEntity(EntityManager em) {
        UsuPer usuPer = new UsuPer()
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return usuPer;
    }

    @Before
    public void initTest() {
        usuPerSearchRepository.deleteAll();
        usuPer = createEntity(em);
    }

    @Test
    @Transactional
    public void createUsuPer() throws Exception {
        int databaseSizeBeforeCreate = usuPerRepository.findAll().size();

        // Create the UsuPer
        restUsuPerMockMvc.perform(post("/api/usu-pers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuPer)))
            .andExpect(status().isCreated());

        // Validate the UsuPer in the database
        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeCreate + 1);
        UsuPer testUsuPer = usuPerList.get(usuPerList.size() - 1);
        assertThat(testUsuPer.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testUsuPer.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testUsuPer.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the UsuPer in Elasticsearch
        UsuPer usuPerEs = usuPerSearchRepository.findOne(testUsuPer.getId());
        assertThat(usuPerEs).isEqualToComparingFieldByField(testUsuPer);
    }

    @Test
    @Transactional
    public void createUsuPerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = usuPerRepository.findAll().size();

        // Create the UsuPer with an existing ID
        usuPer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUsuPerMockMvc.perform(post("/api/usu-pers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuPer)))
            .andExpect(status().isBadRequest());

        // Validate the UsuPer in the database
        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuPerRepository.findAll().size();
        // set the field null
        usuPer.setVarUsuarioLog(null);

        // Create the UsuPer, which fails.

        restUsuPerMockMvc.perform(post("/api/usu-pers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuPer)))
            .andExpect(status().isBadRequest());

        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuPerRepository.findAll().size();
        // set the field null
        usuPer.setDatFechaLog(null);

        // Create the UsuPer, which fails.

        restUsuPerMockMvc.perform(post("/api/usu-pers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuPer)))
            .andExpect(status().isBadRequest());

        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuPerRepository.findAll().size();
        // set the field null
        usuPer.setNumEliminar(null);

        // Create the UsuPer, which fails.

        restUsuPerMockMvc.perform(post("/api/usu-pers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuPer)))
            .andExpect(status().isBadRequest());

        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUsuPers() throws Exception {
        // Initialize the database
        usuPerRepository.saveAndFlush(usuPer);

        // Get all the usuPerList
        restUsuPerMockMvc.perform(get("/api/usu-pers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuPer.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getUsuPer() throws Exception {
        // Initialize the database
        usuPerRepository.saveAndFlush(usuPer);

        // Get the usuPer
        restUsuPerMockMvc.perform(get("/api/usu-pers/{id}", usuPer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(usuPer.getId().intValue()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingUsuPer() throws Exception {
        // Get the usuPer
        restUsuPerMockMvc.perform(get("/api/usu-pers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUsuPer() throws Exception {
        // Initialize the database
        usuPerRepository.saveAndFlush(usuPer);
        usuPerSearchRepository.save(usuPer);
        int databaseSizeBeforeUpdate = usuPerRepository.findAll().size();

        // Update the usuPer
        UsuPer updatedUsuPer = usuPerRepository.findOne(usuPer.getId());
        updatedUsuPer
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restUsuPerMockMvc.perform(put("/api/usu-pers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUsuPer)))
            .andExpect(status().isOk());

        // Validate the UsuPer in the database
        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeUpdate);
        UsuPer testUsuPer = usuPerList.get(usuPerList.size() - 1);
        assertThat(testUsuPer.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testUsuPer.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testUsuPer.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the UsuPer in Elasticsearch
        UsuPer usuPerEs = usuPerSearchRepository.findOne(testUsuPer.getId());
        assertThat(usuPerEs).isEqualToComparingFieldByField(testUsuPer);
    }

    @Test
    @Transactional
    public void updateNonExistingUsuPer() throws Exception {
        int databaseSizeBeforeUpdate = usuPerRepository.findAll().size();

        // Create the UsuPer

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUsuPerMockMvc.perform(put("/api/usu-pers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuPer)))
            .andExpect(status().isCreated());

        // Validate the UsuPer in the database
        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUsuPer() throws Exception {
        // Initialize the database
        usuPerRepository.saveAndFlush(usuPer);
        usuPerSearchRepository.save(usuPer);
        int databaseSizeBeforeDelete = usuPerRepository.findAll().size();

        // Get the usuPer
        restUsuPerMockMvc.perform(delete("/api/usu-pers/{id}", usuPer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean usuPerExistsInEs = usuPerSearchRepository.exists(usuPer.getId());
        assertThat(usuPerExistsInEs).isFalse();

        // Validate the database is empty
        List<UsuPer> usuPerList = usuPerRepository.findAll();
        assertThat(usuPerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUsuPer() throws Exception {
        // Initialize the database
        usuPerRepository.saveAndFlush(usuPer);
        usuPerSearchRepository.save(usuPer);

        // Search the usuPer
        restUsuPerMockMvc.perform(get("/api/_search/usu-pers?query=id:" + usuPer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuPer.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UsuPer.class);
        UsuPer usuPer1 = new UsuPer();
        usuPer1.setId(1L);
        UsuPer usuPer2 = new UsuPer();
        usuPer2.setId(usuPer1.getId());
        assertThat(usuPer1).isEqualTo(usuPer2);
        usuPer2.setId(2L);
        assertThat(usuPer1).isNotEqualTo(usuPer2);
        usuPer1.setId(null);
        assertThat(usuPer1).isNotEqualTo(usuPer2);
    }
}
