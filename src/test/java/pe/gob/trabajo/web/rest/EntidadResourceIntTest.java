package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.Entidad;
import pe.gob.trabajo.repository.EntidadRepository;
import pe.gob.trabajo.repository.search.EntidadSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EntidadResource REST controller.
 *
 * @see EntidadResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class EntidadResourceIntTest {

    private static final String DEFAULT_VAR_RSOCIAL_ENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_RSOCIAL_ENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_RUC_ENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_RUC_ENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_DIREC_ENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DIREC_ENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_COD_DEPARTAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_NUM_COD_DEPARTAMENTO = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_COD_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_NUM_COD_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_COD_DISTRITO = "AAAAAAAAAA";
    private static final String UPDATED_NUM_COD_DISTRITO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_TELEF_ENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_TELEF_ENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_FAX_ENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_FAX_ENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_EMAIL_ENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_EMAIL_ENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_PAGWEB_ENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_PAGWEB_ENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    private static final String DEFAULT_S_CODIGOENTCER = "AAAAAAAA";
    private static final String UPDATED_S_CODIGOENTCER = "BBBBBBBB";

    private static final String DEFAULT_LATITUD = "AAAAAAAAAA";
    private static final String UPDATED_LATITUD = "BBBBBBBBBB";

    private static final String DEFAULT_LONGITUD = "AAAAAAAAAA";
    private static final String UPDATED_LONGITUD = "BBBBBBBBBB";

    @Autowired
    private EntidadRepository entidadRepository;

    @Autowired
    private EntidadSearchRepository entidadSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEntidadMockMvc;

    private Entidad entidad;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EntidadResource entidadResource = new EntidadResource(entidadRepository, entidadSearchRepository);
        this.restEntidadMockMvc = MockMvcBuilders.standaloneSetup(entidadResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Entidad createEntity(EntityManager em) {
        Entidad entidad = new Entidad()
            .varRsocialEntidad(DEFAULT_VAR_RSOCIAL_ENTIDAD)
            .varRucEntidad(DEFAULT_VAR_RUC_ENTIDAD)
            .varDirecEntidad(DEFAULT_VAR_DIREC_ENTIDAD)
            .numCodDepartamento(DEFAULT_NUM_COD_DEPARTAMENTO)
            .numCodProvincia(DEFAULT_NUM_COD_PROVINCIA)
            .numCodDistrito(DEFAULT_NUM_COD_DISTRITO)
            .varTelefEntidad(DEFAULT_VAR_TELEF_ENTIDAD)
            .varFaxEntidad(DEFAULT_VAR_FAX_ENTIDAD)
            .varEmailEntidad(DEFAULT_VAR_EMAIL_ENTIDAD)
            .varPagwebEntidad(DEFAULT_VAR_PAGWEB_ENTIDAD)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR)
            .sCodigoentcer(DEFAULT_S_CODIGOENTCER)
            .latitud(DEFAULT_LATITUD)
            .longitud(DEFAULT_LONGITUD);
        return entidad;
    }

    @Before
    public void initTest() {
        entidadSearchRepository.deleteAll();
        entidad = createEntity(em);
    }

    @Test
    @Transactional
    public void createEntidad() throws Exception {
        int databaseSizeBeforeCreate = entidadRepository.findAll().size();

        // Create the Entidad
        restEntidadMockMvc.perform(post("/api/entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entidad)))
            .andExpect(status().isCreated());

        // Validate the Entidad in the database
        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeCreate + 1);
        Entidad testEntidad = entidadList.get(entidadList.size() - 1);
        assertThat(testEntidad.getVarRsocialEntidad()).isEqualTo(DEFAULT_VAR_RSOCIAL_ENTIDAD);
        assertThat(testEntidad.getVarRucEntidad()).isEqualTo(DEFAULT_VAR_RUC_ENTIDAD);
        assertThat(testEntidad.getVarDirecEntidad()).isEqualTo(DEFAULT_VAR_DIREC_ENTIDAD);
        assertThat(testEntidad.getNumCodDepartamento()).isEqualTo(DEFAULT_NUM_COD_DEPARTAMENTO);
        assertThat(testEntidad.getNumCodProvincia()).isEqualTo(DEFAULT_NUM_COD_PROVINCIA);
        assertThat(testEntidad.getNumCodDistrito()).isEqualTo(DEFAULT_NUM_COD_DISTRITO);
        assertThat(testEntidad.getVarTelefEntidad()).isEqualTo(DEFAULT_VAR_TELEF_ENTIDAD);
        assertThat(testEntidad.getVarFaxEntidad()).isEqualTo(DEFAULT_VAR_FAX_ENTIDAD);
        assertThat(testEntidad.getVarEmailEntidad()).isEqualTo(DEFAULT_VAR_EMAIL_ENTIDAD);
        assertThat(testEntidad.getVarPagwebEntidad()).isEqualTo(DEFAULT_VAR_PAGWEB_ENTIDAD);
        assertThat(testEntidad.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testEntidad.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testEntidad.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);
        assertThat(testEntidad.getsCodigoentcer()).isEqualTo(DEFAULT_S_CODIGOENTCER);
        assertThat(testEntidad.getLatitud()).isEqualTo(DEFAULT_LATITUD);
        assertThat(testEntidad.getLongitud()).isEqualTo(DEFAULT_LONGITUD);

        // Validate the Entidad in Elasticsearch
        Entidad entidadEs = entidadSearchRepository.findOne(testEntidad.getId());
        assertThat(entidadEs).isEqualToComparingFieldByField(testEntidad);
    }

    @Test
    @Transactional
    public void createEntidadWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = entidadRepository.findAll().size();

        // Create the Entidad with an existing ID
        entidad.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEntidadMockMvc.perform(post("/api/entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entidad)))
            .andExpect(status().isBadRequest());

        // Validate the Entidad in the database
        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = entidadRepository.findAll().size();
        // set the field null
        entidad.setVarUsuarioLog(null);

        // Create the Entidad, which fails.

        restEntidadMockMvc.perform(post("/api/entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entidad)))
            .andExpect(status().isBadRequest());

        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = entidadRepository.findAll().size();
        // set the field null
        entidad.setDatFechaLog(null);

        // Create the Entidad, which fails.

        restEntidadMockMvc.perform(post("/api/entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entidad)))
            .andExpect(status().isBadRequest());

        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = entidadRepository.findAll().size();
        // set the field null
        entidad.setNumEliminar(null);

        // Create the Entidad, which fails.

        restEntidadMockMvc.perform(post("/api/entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entidad)))
            .andExpect(status().isBadRequest());

        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEntidads() throws Exception {
        // Initialize the database
        entidadRepository.saveAndFlush(entidad);

        // Get all the entidadList
        restEntidadMockMvc.perform(get("/api/entidads?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(entidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].varRsocialEntidad").value(hasItem(DEFAULT_VAR_RSOCIAL_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varRucEntidad").value(hasItem(DEFAULT_VAR_RUC_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varDirecEntidad").value(hasItem(DEFAULT_VAR_DIREC_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].numCodDepartamento").value(hasItem(DEFAULT_NUM_COD_DEPARTAMENTO.toString())))
            .andExpect(jsonPath("$.[*].numCodProvincia").value(hasItem(DEFAULT_NUM_COD_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].numCodDistrito").value(hasItem(DEFAULT_NUM_COD_DISTRITO.toString())))
            .andExpect(jsonPath("$.[*].varTelefEntidad").value(hasItem(DEFAULT_VAR_TELEF_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varFaxEntidad").value(hasItem(DEFAULT_VAR_FAX_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varEmailEntidad").value(hasItem(DEFAULT_VAR_EMAIL_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varPagwebEntidad").value(hasItem(DEFAULT_VAR_PAGWEB_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)))
            .andExpect(jsonPath("$.[*].sCodigoentcer").value(hasItem(DEFAULT_S_CODIGOENTCER.toString())))
            .andExpect(jsonPath("$.[*].latitud").value(hasItem(DEFAULT_LATITUD.toString())))
            .andExpect(jsonPath("$.[*].longitud").value(hasItem(DEFAULT_LONGITUD.toString())));
    }

    @Test
    @Transactional
    public void getEntidad() throws Exception {
        // Initialize the database
        entidadRepository.saveAndFlush(entidad);

        // Get the entidad
        restEntidadMockMvc.perform(get("/api/entidads/{id}", entidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(entidad.getId().intValue()))
            .andExpect(jsonPath("$.varRsocialEntidad").value(DEFAULT_VAR_RSOCIAL_ENTIDAD.toString()))
            .andExpect(jsonPath("$.varRucEntidad").value(DEFAULT_VAR_RUC_ENTIDAD.toString()))
            .andExpect(jsonPath("$.varDirecEntidad").value(DEFAULT_VAR_DIREC_ENTIDAD.toString()))
            .andExpect(jsonPath("$.numCodDepartamento").value(DEFAULT_NUM_COD_DEPARTAMENTO.toString()))
            .andExpect(jsonPath("$.numCodProvincia").value(DEFAULT_NUM_COD_PROVINCIA.toString()))
            .andExpect(jsonPath("$.numCodDistrito").value(DEFAULT_NUM_COD_DISTRITO.toString()))
            .andExpect(jsonPath("$.varTelefEntidad").value(DEFAULT_VAR_TELEF_ENTIDAD.toString()))
            .andExpect(jsonPath("$.varFaxEntidad").value(DEFAULT_VAR_FAX_ENTIDAD.toString()))
            .andExpect(jsonPath("$.varEmailEntidad").value(DEFAULT_VAR_EMAIL_ENTIDAD.toString()))
            .andExpect(jsonPath("$.varPagwebEntidad").value(DEFAULT_VAR_PAGWEB_ENTIDAD.toString()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR))
            .andExpect(jsonPath("$.sCodigoentcer").value(DEFAULT_S_CODIGOENTCER.toString()))
            .andExpect(jsonPath("$.latitud").value(DEFAULT_LATITUD.toString()))
            .andExpect(jsonPath("$.longitud").value(DEFAULT_LONGITUD.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEntidad() throws Exception {
        // Get the entidad
        restEntidadMockMvc.perform(get("/api/entidads/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEntidad() throws Exception {
        // Initialize the database
        entidadRepository.saveAndFlush(entidad);
        entidadSearchRepository.save(entidad);
        int databaseSizeBeforeUpdate = entidadRepository.findAll().size();

        // Update the entidad
        Entidad updatedEntidad = entidadRepository.findOne(entidad.getId());
        updatedEntidad
            .varRsocialEntidad(UPDATED_VAR_RSOCIAL_ENTIDAD)
            .varRucEntidad(UPDATED_VAR_RUC_ENTIDAD)
            .varDirecEntidad(UPDATED_VAR_DIREC_ENTIDAD)
            .numCodDepartamento(UPDATED_NUM_COD_DEPARTAMENTO)
            .numCodProvincia(UPDATED_NUM_COD_PROVINCIA)
            .numCodDistrito(UPDATED_NUM_COD_DISTRITO)
            .varTelefEntidad(UPDATED_VAR_TELEF_ENTIDAD)
            .varFaxEntidad(UPDATED_VAR_FAX_ENTIDAD)
            .varEmailEntidad(UPDATED_VAR_EMAIL_ENTIDAD)
            .varPagwebEntidad(UPDATED_VAR_PAGWEB_ENTIDAD)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR)
            .sCodigoentcer(UPDATED_S_CODIGOENTCER)
            .latitud(UPDATED_LATITUD)
            .longitud(UPDATED_LONGITUD);

        restEntidadMockMvc.perform(put("/api/entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEntidad)))
            .andExpect(status().isOk());

        // Validate the Entidad in the database
        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeUpdate);
        Entidad testEntidad = entidadList.get(entidadList.size() - 1);
        assertThat(testEntidad.getVarRsocialEntidad()).isEqualTo(UPDATED_VAR_RSOCIAL_ENTIDAD);
        assertThat(testEntidad.getVarRucEntidad()).isEqualTo(UPDATED_VAR_RUC_ENTIDAD);
        assertThat(testEntidad.getVarDirecEntidad()).isEqualTo(UPDATED_VAR_DIREC_ENTIDAD);
        assertThat(testEntidad.getNumCodDepartamento()).isEqualTo(UPDATED_NUM_COD_DEPARTAMENTO);
        assertThat(testEntidad.getNumCodProvincia()).isEqualTo(UPDATED_NUM_COD_PROVINCIA);
        assertThat(testEntidad.getNumCodDistrito()).isEqualTo(UPDATED_NUM_COD_DISTRITO);
        assertThat(testEntidad.getVarTelefEntidad()).isEqualTo(UPDATED_VAR_TELEF_ENTIDAD);
        assertThat(testEntidad.getVarFaxEntidad()).isEqualTo(UPDATED_VAR_FAX_ENTIDAD);
        assertThat(testEntidad.getVarEmailEntidad()).isEqualTo(UPDATED_VAR_EMAIL_ENTIDAD);
        assertThat(testEntidad.getVarPagwebEntidad()).isEqualTo(UPDATED_VAR_PAGWEB_ENTIDAD);
        assertThat(testEntidad.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testEntidad.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testEntidad.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);
        assertThat(testEntidad.getsCodigoentcer()).isEqualTo(UPDATED_S_CODIGOENTCER);
        assertThat(testEntidad.getLatitud()).isEqualTo(UPDATED_LATITUD);
        assertThat(testEntidad.getLongitud()).isEqualTo(UPDATED_LONGITUD);

        // Validate the Entidad in Elasticsearch
        Entidad entidadEs = entidadSearchRepository.findOne(testEntidad.getId());
        assertThat(entidadEs).isEqualToComparingFieldByField(testEntidad);
    }

    @Test
    @Transactional
    public void updateNonExistingEntidad() throws Exception {
        int databaseSizeBeforeUpdate = entidadRepository.findAll().size();

        // Create the Entidad

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEntidadMockMvc.perform(put("/api/entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entidad)))
            .andExpect(status().isCreated());

        // Validate the Entidad in the database
        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEntidad() throws Exception {
        // Initialize the database
        entidadRepository.saveAndFlush(entidad);
        entidadSearchRepository.save(entidad);
        int databaseSizeBeforeDelete = entidadRepository.findAll().size();

        // Get the entidad
        restEntidadMockMvc.perform(delete("/api/entidads/{id}", entidad.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean entidadExistsInEs = entidadSearchRepository.exists(entidad.getId());
        assertThat(entidadExistsInEs).isFalse();

        // Validate the database is empty
        List<Entidad> entidadList = entidadRepository.findAll();
        assertThat(entidadList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchEntidad() throws Exception {
        // Initialize the database
        entidadRepository.saveAndFlush(entidad);
        entidadSearchRepository.save(entidad);

        // Search the entidad
        restEntidadMockMvc.perform(get("/api/_search/entidads?query=id:" + entidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(entidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].varRsocialEntidad").value(hasItem(DEFAULT_VAR_RSOCIAL_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varRucEntidad").value(hasItem(DEFAULT_VAR_RUC_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varDirecEntidad").value(hasItem(DEFAULT_VAR_DIREC_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].numCodDepartamento").value(hasItem(DEFAULT_NUM_COD_DEPARTAMENTO.toString())))
            .andExpect(jsonPath("$.[*].numCodProvincia").value(hasItem(DEFAULT_NUM_COD_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].numCodDistrito").value(hasItem(DEFAULT_NUM_COD_DISTRITO.toString())))
            .andExpect(jsonPath("$.[*].varTelefEntidad").value(hasItem(DEFAULT_VAR_TELEF_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varFaxEntidad").value(hasItem(DEFAULT_VAR_FAX_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varEmailEntidad").value(hasItem(DEFAULT_VAR_EMAIL_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varPagwebEntidad").value(hasItem(DEFAULT_VAR_PAGWEB_ENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)))
            .andExpect(jsonPath("$.[*].sCodigoentcer").value(hasItem(DEFAULT_S_CODIGOENTCER.toString())))
            .andExpect(jsonPath("$.[*].latitud").value(hasItem(DEFAULT_LATITUD.toString())))
            .andExpect(jsonPath("$.[*].longitud").value(hasItem(DEFAULT_LONGITUD.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Entidad.class);
        Entidad entidad1 = new Entidad();
        entidad1.setId(1L);
        Entidad entidad2 = new Entidad();
        entidad2.setId(entidad1.getId());
        assertThat(entidad1).isEqualTo(entidad2);
        entidad2.setId(2L);
        assertThat(entidad1).isNotEqualTo(entidad2);
        entidad1.setId(null);
        assertThat(entidad1).isNotEqualTo(entidad2);
    }
}
