package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.Permiso;
import pe.gob.trabajo.repository.PermisoRepository;
import pe.gob.trabajo.repository.search.PermisoSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PermisoResource REST controller.
 *
 * @see PermisoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class PermisoResourceIntTest {

    private static final String DEFAULT_VAR_NOM_PERMISO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_PERMISO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private PermisoRepository permisoRepository;

    @Autowired
    private PermisoSearchRepository permisoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPermisoMockMvc;

    private Permiso permiso;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PermisoResource permisoResource = new PermisoResource(permisoRepository, permisoSearchRepository);
        this.restPermisoMockMvc = MockMvcBuilders.standaloneSetup(permisoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Permiso createEntity(EntityManager em) {
        Permiso permiso = new Permiso()
            .varNomPermiso(DEFAULT_VAR_NOM_PERMISO)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return permiso;
    }

    @Before
    public void initTest() {
        permisoSearchRepository.deleteAll();
        permiso = createEntity(em);
    }

    @Test
    @Transactional
    public void createPermiso() throws Exception {
        int databaseSizeBeforeCreate = permisoRepository.findAll().size();

        // Create the Permiso
        restPermisoMockMvc.perform(post("/api/permisos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permiso)))
            .andExpect(status().isCreated());

        // Validate the Permiso in the database
        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeCreate + 1);
        Permiso testPermiso = permisoList.get(permisoList.size() - 1);
        assertThat(testPermiso.getVarNomPermiso()).isEqualTo(DEFAULT_VAR_NOM_PERMISO);
        assertThat(testPermiso.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testPermiso.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testPermiso.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the Permiso in Elasticsearch
        Permiso permisoEs = permisoSearchRepository.findOne(testPermiso.getId());
        assertThat(permisoEs).isEqualToComparingFieldByField(testPermiso);
    }

    @Test
    @Transactional
    public void createPermisoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = permisoRepository.findAll().size();

        // Create the Permiso with an existing ID
        permiso.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPermisoMockMvc.perform(post("/api/permisos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permiso)))
            .andExpect(status().isBadRequest());

        // Validate the Permiso in the database
        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = permisoRepository.findAll().size();
        // set the field null
        permiso.setVarUsuarioLog(null);

        // Create the Permiso, which fails.

        restPermisoMockMvc.perform(post("/api/permisos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permiso)))
            .andExpect(status().isBadRequest());

        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = permisoRepository.findAll().size();
        // set the field null
        permiso.setDatFechaLog(null);

        // Create the Permiso, which fails.

        restPermisoMockMvc.perform(post("/api/permisos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permiso)))
            .andExpect(status().isBadRequest());

        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = permisoRepository.findAll().size();
        // set the field null
        permiso.setNumEliminar(null);

        // Create the Permiso, which fails.

        restPermisoMockMvc.perform(post("/api/permisos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permiso)))
            .andExpect(status().isBadRequest());

        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPermisos() throws Exception {
        // Initialize the database
        permisoRepository.saveAndFlush(permiso);

        // Get all the permisoList
        restPermisoMockMvc.perform(get("/api/permisos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permiso.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomPermiso").value(hasItem(DEFAULT_VAR_NOM_PERMISO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getPermiso() throws Exception {
        // Initialize the database
        permisoRepository.saveAndFlush(permiso);

        // Get the permiso
        restPermisoMockMvc.perform(get("/api/permisos/{id}", permiso.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(permiso.getId().intValue()))
            .andExpect(jsonPath("$.varNomPermiso").value(DEFAULT_VAR_NOM_PERMISO.toString()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingPermiso() throws Exception {
        // Get the permiso
        restPermisoMockMvc.perform(get("/api/permisos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePermiso() throws Exception {
        // Initialize the database
        permisoRepository.saveAndFlush(permiso);
        permisoSearchRepository.save(permiso);
        int databaseSizeBeforeUpdate = permisoRepository.findAll().size();

        // Update the permiso
        Permiso updatedPermiso = permisoRepository.findOne(permiso.getId());
        updatedPermiso
            .varNomPermiso(UPDATED_VAR_NOM_PERMISO)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restPermisoMockMvc.perform(put("/api/permisos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPermiso)))
            .andExpect(status().isOk());

        // Validate the Permiso in the database
        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeUpdate);
        Permiso testPermiso = permisoList.get(permisoList.size() - 1);
        assertThat(testPermiso.getVarNomPermiso()).isEqualTo(UPDATED_VAR_NOM_PERMISO);
        assertThat(testPermiso.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testPermiso.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testPermiso.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the Permiso in Elasticsearch
        Permiso permisoEs = permisoSearchRepository.findOne(testPermiso.getId());
        assertThat(permisoEs).isEqualToComparingFieldByField(testPermiso);
    }

    @Test
    @Transactional
    public void updateNonExistingPermiso() throws Exception {
        int databaseSizeBeforeUpdate = permisoRepository.findAll().size();

        // Create the Permiso

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPermisoMockMvc.perform(put("/api/permisos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(permiso)))
            .andExpect(status().isCreated());

        // Validate the Permiso in the database
        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePermiso() throws Exception {
        // Initialize the database
        permisoRepository.saveAndFlush(permiso);
        permisoSearchRepository.save(permiso);
        int databaseSizeBeforeDelete = permisoRepository.findAll().size();

        // Get the permiso
        restPermisoMockMvc.perform(delete("/api/permisos/{id}", permiso.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean permisoExistsInEs = permisoSearchRepository.exists(permiso.getId());
        assertThat(permisoExistsInEs).isFalse();

        // Validate the database is empty
        List<Permiso> permisoList = permisoRepository.findAll();
        assertThat(permisoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPermiso() throws Exception {
        // Initialize the database
        permisoRepository.saveAndFlush(permiso);
        permisoSearchRepository.save(permiso);

        // Search the permiso
        restPermisoMockMvc.perform(get("/api/_search/permisos?query=id:" + permiso.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(permiso.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomPermiso").value(hasItem(DEFAULT_VAR_NOM_PERMISO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Permiso.class);
        Permiso permiso1 = new Permiso();
        permiso1.setId(1L);
        Permiso permiso2 = new Permiso();
        permiso2.setId(permiso1.getId());
        assertThat(permiso1).isEqualTo(permiso2);
        permiso2.setId(2L);
        assertThat(permiso1).isNotEqualTo(permiso2);
        permiso1.setId(null);
        assertThat(permiso1).isNotEqualTo(permiso2);
    }
}
