package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.Grupo;
import pe.gob.trabajo.repository.GrupoRepository;
import pe.gob.trabajo.repository.search.GrupoSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GrupoResource REST controller.
 *
 * @see GrupoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class GrupoResourceIntTest {

    private static final Integer DEFAULT_NUM_COD_LOCAL = 1;
    private static final Integer UPDATED_NUM_COD_LOCAL = 2;

    private static final Integer DEFAULT_NUM_EST_GRUPO = 1;
    private static final Integer UPDATED_NUM_EST_GRUPO = 2;

    private static final String DEFAULT_VAR_NOM_GRUPO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_GRUPO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_DESC_GRUPO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DESC_GRUPO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private GrupoRepository grupoRepository;

    @Autowired
    private GrupoSearchRepository grupoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restGrupoMockMvc;

    private Grupo grupo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final GrupoResource grupoResource = new GrupoResource(grupoRepository, grupoSearchRepository);
        this.restGrupoMockMvc = MockMvcBuilders.standaloneSetup(grupoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Grupo createEntity(EntityManager em) {
        Grupo grupo = new Grupo()
            .numCodLocal(DEFAULT_NUM_COD_LOCAL)
            .numEstGrupo(DEFAULT_NUM_EST_GRUPO)
            .varNomGrupo(DEFAULT_VAR_NOM_GRUPO)
            .varDescGrupo(DEFAULT_VAR_DESC_GRUPO)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return grupo;
    }

    @Before
    public void initTest() {
        grupoSearchRepository.deleteAll();
        grupo = createEntity(em);
    }

    @Test
    @Transactional
    public void createGrupo() throws Exception {
        int databaseSizeBeforeCreate = grupoRepository.findAll().size();

        // Create the Grupo
        restGrupoMockMvc.perform(post("/api/grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(grupo)))
            .andExpect(status().isCreated());

        // Validate the Grupo in the database
        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeCreate + 1);
        Grupo testGrupo = grupoList.get(grupoList.size() - 1);
        assertThat(testGrupo.getNumCodLocal()).isEqualTo(DEFAULT_NUM_COD_LOCAL);
        assertThat(testGrupo.getNumEstGrupo()).isEqualTo(DEFAULT_NUM_EST_GRUPO);
        assertThat(testGrupo.getVarNomGrupo()).isEqualTo(DEFAULT_VAR_NOM_GRUPO);
        assertThat(testGrupo.getVarDescGrupo()).isEqualTo(DEFAULT_VAR_DESC_GRUPO);
        assertThat(testGrupo.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testGrupo.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testGrupo.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the Grupo in Elasticsearch
        Grupo grupoEs = grupoSearchRepository.findOne(testGrupo.getId());
        assertThat(grupoEs).isEqualToComparingFieldByField(testGrupo);
    }

    @Test
    @Transactional
    public void createGrupoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = grupoRepository.findAll().size();

        // Create the Grupo with an existing ID
        grupo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGrupoMockMvc.perform(post("/api/grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(grupo)))
            .andExpect(status().isBadRequest());

        // Validate the Grupo in the database
        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupoRepository.findAll().size();
        // set the field null
        grupo.setVarUsuarioLog(null);

        // Create the Grupo, which fails.

        restGrupoMockMvc.perform(post("/api/grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(grupo)))
            .andExpect(status().isBadRequest());

        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupoRepository.findAll().size();
        // set the field null
        grupo.setDatFechaLog(null);

        // Create the Grupo, which fails.

        restGrupoMockMvc.perform(post("/api/grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(grupo)))
            .andExpect(status().isBadRequest());

        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupoRepository.findAll().size();
        // set the field null
        grupo.setNumEliminar(null);

        // Create the Grupo, which fails.

        restGrupoMockMvc.perform(post("/api/grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(grupo)))
            .andExpect(status().isBadRequest());

        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGrupos() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);

        // Get all the grupoList
        restGrupoMockMvc.perform(get("/api/grupos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(grupo.getId().intValue())))
            .andExpect(jsonPath("$.[*].numCodLocal").value(hasItem(DEFAULT_NUM_COD_LOCAL)))
            .andExpect(jsonPath("$.[*].numEstGrupo").value(hasItem(DEFAULT_NUM_EST_GRUPO)))
            .andExpect(jsonPath("$.[*].varNomGrupo").value(hasItem(DEFAULT_VAR_NOM_GRUPO.toString())))
            .andExpect(jsonPath("$.[*].varDescGrupo").value(hasItem(DEFAULT_VAR_DESC_GRUPO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getGrupo() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);

        // Get the grupo
        restGrupoMockMvc.perform(get("/api/grupos/{id}", grupo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(grupo.getId().intValue()))
            .andExpect(jsonPath("$.numCodLocal").value(DEFAULT_NUM_COD_LOCAL))
            .andExpect(jsonPath("$.numEstGrupo").value(DEFAULT_NUM_EST_GRUPO))
            .andExpect(jsonPath("$.varNomGrupo").value(DEFAULT_VAR_NOM_GRUPO.toString()))
            .andExpect(jsonPath("$.varDescGrupo").value(DEFAULT_VAR_DESC_GRUPO.toString()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingGrupo() throws Exception {
        // Get the grupo
        restGrupoMockMvc.perform(get("/api/grupos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGrupo() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);
        grupoSearchRepository.save(grupo);
        int databaseSizeBeforeUpdate = grupoRepository.findAll().size();

        // Update the grupo
        Grupo updatedGrupo = grupoRepository.findOne(grupo.getId());
        updatedGrupo
            .numCodLocal(UPDATED_NUM_COD_LOCAL)
            .numEstGrupo(UPDATED_NUM_EST_GRUPO)
            .varNomGrupo(UPDATED_VAR_NOM_GRUPO)
            .varDescGrupo(UPDATED_VAR_DESC_GRUPO)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restGrupoMockMvc.perform(put("/api/grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedGrupo)))
            .andExpect(status().isOk());

        // Validate the Grupo in the database
        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeUpdate);
        Grupo testGrupo = grupoList.get(grupoList.size() - 1);
        assertThat(testGrupo.getNumCodLocal()).isEqualTo(UPDATED_NUM_COD_LOCAL);
        assertThat(testGrupo.getNumEstGrupo()).isEqualTo(UPDATED_NUM_EST_GRUPO);
        assertThat(testGrupo.getVarNomGrupo()).isEqualTo(UPDATED_VAR_NOM_GRUPO);
        assertThat(testGrupo.getVarDescGrupo()).isEqualTo(UPDATED_VAR_DESC_GRUPO);
        assertThat(testGrupo.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testGrupo.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testGrupo.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the Grupo in Elasticsearch
        Grupo grupoEs = grupoSearchRepository.findOne(testGrupo.getId());
        assertThat(grupoEs).isEqualToComparingFieldByField(testGrupo);
    }

    @Test
    @Transactional
    public void updateNonExistingGrupo() throws Exception {
        int databaseSizeBeforeUpdate = grupoRepository.findAll().size();

        // Create the Grupo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restGrupoMockMvc.perform(put("/api/grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(grupo)))
            .andExpect(status().isCreated());

        // Validate the Grupo in the database
        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteGrupo() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);
        grupoSearchRepository.save(grupo);
        int databaseSizeBeforeDelete = grupoRepository.findAll().size();

        // Get the grupo
        restGrupoMockMvc.perform(delete("/api/grupos/{id}", grupo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean grupoExistsInEs = grupoSearchRepository.exists(grupo.getId());
        assertThat(grupoExistsInEs).isFalse();

        // Validate the database is empty
        List<Grupo> grupoList = grupoRepository.findAll();
        assertThat(grupoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGrupo() throws Exception {
        // Initialize the database
        grupoRepository.saveAndFlush(grupo);
        grupoSearchRepository.save(grupo);

        // Search the grupo
        restGrupoMockMvc.perform(get("/api/_search/grupos?query=id:" + grupo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(grupo.getId().intValue())))
            .andExpect(jsonPath("$.[*].numCodLocal").value(hasItem(DEFAULT_NUM_COD_LOCAL)))
            .andExpect(jsonPath("$.[*].numEstGrupo").value(hasItem(DEFAULT_NUM_EST_GRUPO)))
            .andExpect(jsonPath("$.[*].varNomGrupo").value(hasItem(DEFAULT_VAR_NOM_GRUPO.toString())))
            .andExpect(jsonPath("$.[*].varDescGrupo").value(hasItem(DEFAULT_VAR_DESC_GRUPO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Grupo.class);
        Grupo grupo1 = new Grupo();
        grupo1.setId(1L);
        Grupo grupo2 = new Grupo();
        grupo2.setId(grupo1.getId());
        assertThat(grupo1).isEqualTo(grupo2);
        grupo2.setId(2L);
        assertThat(grupo1).isNotEqualTo(grupo2);
        grupo1.setId(null);
        assertThat(grupo1).isNotEqualTo(grupo2);
    }
}
