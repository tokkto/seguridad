package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.TipoEntidad;
import pe.gob.trabajo.repository.TipoEntidadRepository;
import pe.gob.trabajo.repository.search.TipoEntidadSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipoEntidadResource REST controller.
 *
 * @see TipoEntidadResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class TipoEntidadResourceIntTest {

    private static final String DEFAULT_VAR_NOM_TPENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_TPENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_DESC_TPENTIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DESC_TPENTIDAD = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private TipoEntidadRepository tipoEntidadRepository;

    @Autowired
    private TipoEntidadSearchRepository tipoEntidadSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipoEntidadMockMvc;

    private TipoEntidad tipoEntidad;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoEntidadResource tipoEntidadResource = new TipoEntidadResource(tipoEntidadRepository, tipoEntidadSearchRepository);
        this.restTipoEntidadMockMvc = MockMvcBuilders.standaloneSetup(tipoEntidadResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoEntidad createEntity(EntityManager em) {
        TipoEntidad tipoEntidad = new TipoEntidad()
            .varNomTpentidad(DEFAULT_VAR_NOM_TPENTIDAD)
            .varDescTpentidad(DEFAULT_VAR_DESC_TPENTIDAD)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return tipoEntidad;
    }

    @Before
    public void initTest() {
        tipoEntidadSearchRepository.deleteAll();
        tipoEntidad = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoEntidad() throws Exception {
        int databaseSizeBeforeCreate = tipoEntidadRepository.findAll().size();

        // Create the TipoEntidad
        restTipoEntidadMockMvc.perform(post("/api/tipo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoEntidad)))
            .andExpect(status().isCreated());

        // Validate the TipoEntidad in the database
        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeCreate + 1);
        TipoEntidad testTipoEntidad = tipoEntidadList.get(tipoEntidadList.size() - 1);
        assertThat(testTipoEntidad.getVarNomTpentidad()).isEqualTo(DEFAULT_VAR_NOM_TPENTIDAD);
        assertThat(testTipoEntidad.getVarDescTpentidad()).isEqualTo(DEFAULT_VAR_DESC_TPENTIDAD);
        assertThat(testTipoEntidad.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testTipoEntidad.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testTipoEntidad.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the TipoEntidad in Elasticsearch
        TipoEntidad tipoEntidadEs = tipoEntidadSearchRepository.findOne(testTipoEntidad.getId());
        assertThat(tipoEntidadEs).isEqualToComparingFieldByField(testTipoEntidad);
    }

    @Test
    @Transactional
    public void createTipoEntidadWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoEntidadRepository.findAll().size();

        // Create the TipoEntidad with an existing ID
        tipoEntidad.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoEntidadMockMvc.perform(post("/api/tipo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoEntidad)))
            .andExpect(status().isBadRequest());

        // Validate the TipoEntidad in the database
        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoEntidadRepository.findAll().size();
        // set the field null
        tipoEntidad.setVarUsuarioLog(null);

        // Create the TipoEntidad, which fails.

        restTipoEntidadMockMvc.perform(post("/api/tipo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoEntidad)))
            .andExpect(status().isBadRequest());

        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoEntidadRepository.findAll().size();
        // set the field null
        tipoEntidad.setDatFechaLog(null);

        // Create the TipoEntidad, which fails.

        restTipoEntidadMockMvc.perform(post("/api/tipo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoEntidad)))
            .andExpect(status().isBadRequest());

        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoEntidadRepository.findAll().size();
        // set the field null
        tipoEntidad.setNumEliminar(null);

        // Create the TipoEntidad, which fails.

        restTipoEntidadMockMvc.perform(post("/api/tipo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoEntidad)))
            .andExpect(status().isBadRequest());

        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipoEntidads() throws Exception {
        // Initialize the database
        tipoEntidadRepository.saveAndFlush(tipoEntidad);

        // Get all the tipoEntidadList
        restTipoEntidadMockMvc.perform(get("/api/tipo-entidads?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoEntidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomTpentidad").value(hasItem(DEFAULT_VAR_NOM_TPENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varDescTpentidad").value(hasItem(DEFAULT_VAR_DESC_TPENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getTipoEntidad() throws Exception {
        // Initialize the database
        tipoEntidadRepository.saveAndFlush(tipoEntidad);

        // Get the tipoEntidad
        restTipoEntidadMockMvc.perform(get("/api/tipo-entidads/{id}", tipoEntidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoEntidad.getId().intValue()))
            .andExpect(jsonPath("$.varNomTpentidad").value(DEFAULT_VAR_NOM_TPENTIDAD.toString()))
            .andExpect(jsonPath("$.varDescTpentidad").value(DEFAULT_VAR_DESC_TPENTIDAD.toString()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingTipoEntidad() throws Exception {
        // Get the tipoEntidad
        restTipoEntidadMockMvc.perform(get("/api/tipo-entidads/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoEntidad() throws Exception {
        // Initialize the database
        tipoEntidadRepository.saveAndFlush(tipoEntidad);
        tipoEntidadSearchRepository.save(tipoEntidad);
        int databaseSizeBeforeUpdate = tipoEntidadRepository.findAll().size();

        // Update the tipoEntidad
        TipoEntidad updatedTipoEntidad = tipoEntidadRepository.findOne(tipoEntidad.getId());
        updatedTipoEntidad
            .varNomTpentidad(UPDATED_VAR_NOM_TPENTIDAD)
            .varDescTpentidad(UPDATED_VAR_DESC_TPENTIDAD)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restTipoEntidadMockMvc.perform(put("/api/tipo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoEntidad)))
            .andExpect(status().isOk());

        // Validate the TipoEntidad in the database
        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeUpdate);
        TipoEntidad testTipoEntidad = tipoEntidadList.get(tipoEntidadList.size() - 1);
        assertThat(testTipoEntidad.getVarNomTpentidad()).isEqualTo(UPDATED_VAR_NOM_TPENTIDAD);
        assertThat(testTipoEntidad.getVarDescTpentidad()).isEqualTo(UPDATED_VAR_DESC_TPENTIDAD);
        assertThat(testTipoEntidad.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testTipoEntidad.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testTipoEntidad.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the TipoEntidad in Elasticsearch
        TipoEntidad tipoEntidadEs = tipoEntidadSearchRepository.findOne(testTipoEntidad.getId());
        assertThat(tipoEntidadEs).isEqualToComparingFieldByField(testTipoEntidad);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoEntidad() throws Exception {
        int databaseSizeBeforeUpdate = tipoEntidadRepository.findAll().size();

        // Create the TipoEntidad

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipoEntidadMockMvc.perform(put("/api/tipo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoEntidad)))
            .andExpect(status().isCreated());

        // Validate the TipoEntidad in the database
        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipoEntidad() throws Exception {
        // Initialize the database
        tipoEntidadRepository.saveAndFlush(tipoEntidad);
        tipoEntidadSearchRepository.save(tipoEntidad);
        int databaseSizeBeforeDelete = tipoEntidadRepository.findAll().size();

        // Get the tipoEntidad
        restTipoEntidadMockMvc.perform(delete("/api/tipo-entidads/{id}", tipoEntidad.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipoEntidadExistsInEs = tipoEntidadSearchRepository.exists(tipoEntidad.getId());
        assertThat(tipoEntidadExistsInEs).isFalse();

        // Validate the database is empty
        List<TipoEntidad> tipoEntidadList = tipoEntidadRepository.findAll();
        assertThat(tipoEntidadList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipoEntidad() throws Exception {
        // Initialize the database
        tipoEntidadRepository.saveAndFlush(tipoEntidad);
        tipoEntidadSearchRepository.save(tipoEntidad);

        // Search the tipoEntidad
        restTipoEntidadMockMvc.perform(get("/api/_search/tipo-entidads?query=id:" + tipoEntidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoEntidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomTpentidad").value(hasItem(DEFAULT_VAR_NOM_TPENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varDescTpentidad").value(hasItem(DEFAULT_VAR_DESC_TPENTIDAD.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoEntidad.class);
        TipoEntidad tipoEntidad1 = new TipoEntidad();
        tipoEntidad1.setId(1L);
        TipoEntidad tipoEntidad2 = new TipoEntidad();
        tipoEntidad2.setId(tipoEntidad1.getId());
        assertThat(tipoEntidad1).isEqualTo(tipoEntidad2);
        tipoEntidad2.setId(2L);
        assertThat(tipoEntidad1).isNotEqualTo(tipoEntidad2);
        tipoEntidad1.setId(null);
        assertThat(tipoEntidad1).isNotEqualTo(tipoEntidad2);
    }
}
