package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.Usuario;
import pe.gob.trabajo.repository.UsuarioRepository;
import pe.gob.trabajo.repository.search.UsuarioSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UsuarioResource REST controller.
 *
 * @see UsuarioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class UsuarioResourceIntTest {

    private static final String DEFAULT_VAR_NOM_USUARIO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_USUARIO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_LOGIN_USUARIO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_LOGIN_USUARIO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_INICIAL_USUARIO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_INICIAL_USUARIO = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_EST_USUARIO = 1;
    private static final Integer UPDATED_NUM_EST_USUARIO = 2;

    private static final String DEFAULT_VAR_MOTIV_USUARIO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_MOTIV_USUARIO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_DOC_CONTACTO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DOC_CONTACTO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_NOM_CONTACTO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_CONTACTO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_REF_CONTACTO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_REF_CONTACTO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    private static final String DEFAULT_VAR_DIRECCION_IP = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DIRECCION_IP = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_MAC_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_VAR_MAC_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_NOMBRE_PC = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOMBRE_PC = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_COD_SEGURIDAD = "AAAAAAAAAA";
    private static final String UPDATED_VAR_COD_SEGURIDAD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DAT_FEC_TERMINO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DAT_FEC_TERMINO = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_VAR_PTO_CONTROL = 1;
    private static final Integer UPDATED_VAR_PTO_CONTROL = 2;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioSearchRepository usuarioSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUsuarioMockMvc;

    private Usuario usuario;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UsuarioResource usuarioResource = new UsuarioResource(usuarioRepository, usuarioSearchRepository);
        this.restUsuarioMockMvc = MockMvcBuilders.standaloneSetup(usuarioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Usuario createEntity(EntityManager em) {
        Usuario usuario = new Usuario()
            .varNomUsuario(DEFAULT_VAR_NOM_USUARIO)
            .varLoginUsuario(DEFAULT_VAR_LOGIN_USUARIO)
            .varInicialUsuario(DEFAULT_VAR_INICIAL_USUARIO)
            .numEstUsuario(DEFAULT_NUM_EST_USUARIO)
            .varMotivUsuario(DEFAULT_VAR_MOTIV_USUARIO)
            .varDocContacto(DEFAULT_VAR_DOC_CONTACTO)
            .varNomContacto(DEFAULT_VAR_NOM_CONTACTO)
            .varRefContacto(DEFAULT_VAR_REF_CONTACTO)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR)
            .varDireccionIp(DEFAULT_VAR_DIRECCION_IP)
            .varMacAddress(DEFAULT_VAR_MAC_ADDRESS)
            .varNombrePc(DEFAULT_VAR_NOMBRE_PC)
            .varCodSeguridad(DEFAULT_VAR_COD_SEGURIDAD)
            .datFecTermino(DEFAULT_DAT_FEC_TERMINO)
            .varPtoControl(DEFAULT_VAR_PTO_CONTROL);
        return usuario;
    }

    @Before
    public void initTest() {
        usuarioSearchRepository.deleteAll();
        usuario = createEntity(em);
    }

    @Test
    @Transactional
    public void createUsuario() throws Exception {
        int databaseSizeBeforeCreate = usuarioRepository.findAll().size();

        // Create the Usuario
        restUsuarioMockMvc.perform(post("/api/usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isCreated());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeCreate + 1);
        Usuario testUsuario = usuarioList.get(usuarioList.size() - 1);
        assertThat(testUsuario.getVarNomUsuario()).isEqualTo(DEFAULT_VAR_NOM_USUARIO);
        assertThat(testUsuario.getVarLoginUsuario()).isEqualTo(DEFAULT_VAR_LOGIN_USUARIO);
        assertThat(testUsuario.getVarInicialUsuario()).isEqualTo(DEFAULT_VAR_INICIAL_USUARIO);
        assertThat(testUsuario.getNumEstUsuario()).isEqualTo(DEFAULT_NUM_EST_USUARIO);
        assertThat(testUsuario.getVarMotivUsuario()).isEqualTo(DEFAULT_VAR_MOTIV_USUARIO);
        assertThat(testUsuario.getVarDocContacto()).isEqualTo(DEFAULT_VAR_DOC_CONTACTO);
        assertThat(testUsuario.getVarNomContacto()).isEqualTo(DEFAULT_VAR_NOM_CONTACTO);
        assertThat(testUsuario.getVarRefContacto()).isEqualTo(DEFAULT_VAR_REF_CONTACTO);
        assertThat(testUsuario.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testUsuario.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testUsuario.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);
        assertThat(testUsuario.getVarDireccionIp()).isEqualTo(DEFAULT_VAR_DIRECCION_IP);
        assertThat(testUsuario.getVarMacAddress()).isEqualTo(DEFAULT_VAR_MAC_ADDRESS);
        assertThat(testUsuario.getVarNombrePc()).isEqualTo(DEFAULT_VAR_NOMBRE_PC);
        assertThat(testUsuario.getVarCodSeguridad()).isEqualTo(DEFAULT_VAR_COD_SEGURIDAD);
        assertThat(testUsuario.getDatFecTermino()).isEqualTo(DEFAULT_DAT_FEC_TERMINO);
        assertThat(testUsuario.getVarPtoControl()).isEqualTo(DEFAULT_VAR_PTO_CONTROL);

        // Validate the Usuario in Elasticsearch
        Usuario usuarioEs = usuarioSearchRepository.findOne(testUsuario.getId());
        assertThat(usuarioEs).isEqualToComparingFieldByField(testUsuario);
    }

    @Test
    @Transactional
    public void createUsuarioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = usuarioRepository.findAll().size();

        // Create the Usuario with an existing ID
        usuario.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUsuarioMockMvc.perform(post("/api/usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isBadRequest());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioRepository.findAll().size();
        // set the field null
        usuario.setVarUsuarioLog(null);

        // Create the Usuario, which fails.

        restUsuarioMockMvc.perform(post("/api/usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isBadRequest());

        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioRepository.findAll().size();
        // set the field null
        usuario.setDatFechaLog(null);

        // Create the Usuario, which fails.

        restUsuarioMockMvc.perform(post("/api/usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isBadRequest());

        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioRepository.findAll().size();
        // set the field null
        usuario.setNumEliminar(null);

        // Create the Usuario, which fails.

        restUsuarioMockMvc.perform(post("/api/usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isBadRequest());

        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUsuarios() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get all the usuarioList
        restUsuarioMockMvc.perform(get("/api/usuarios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuario.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomUsuario").value(hasItem(DEFAULT_VAR_NOM_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].varLoginUsuario").value(hasItem(DEFAULT_VAR_LOGIN_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].varInicialUsuario").value(hasItem(DEFAULT_VAR_INICIAL_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].numEstUsuario").value(hasItem(DEFAULT_NUM_EST_USUARIO)))
            .andExpect(jsonPath("$.[*].varMotivUsuario").value(hasItem(DEFAULT_VAR_MOTIV_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].varDocContacto").value(hasItem(DEFAULT_VAR_DOC_CONTACTO.toString())))
            .andExpect(jsonPath("$.[*].varNomContacto").value(hasItem(DEFAULT_VAR_NOM_CONTACTO.toString())))
            .andExpect(jsonPath("$.[*].varRefContacto").value(hasItem(DEFAULT_VAR_REF_CONTACTO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)))
            .andExpect(jsonPath("$.[*].varDireccionIp").value(hasItem(DEFAULT_VAR_DIRECCION_IP.toString())))
            .andExpect(jsonPath("$.[*].varMacAddress").value(hasItem(DEFAULT_VAR_MAC_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].varNombrePc").value(hasItem(DEFAULT_VAR_NOMBRE_PC.toString())))
            .andExpect(jsonPath("$.[*].varCodSeguridad").value(hasItem(DEFAULT_VAR_COD_SEGURIDAD.toString())))
            .andExpect(jsonPath("$.[*].datFecTermino").value(hasItem(DEFAULT_DAT_FEC_TERMINO.toString())))
            .andExpect(jsonPath("$.[*].varPtoControl").value(hasItem(DEFAULT_VAR_PTO_CONTROL)));
    }

    @Test
    @Transactional
    public void getUsuario() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);

        // Get the usuario
        restUsuarioMockMvc.perform(get("/api/usuarios/{id}", usuario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(usuario.getId().intValue()))
            .andExpect(jsonPath("$.varNomUsuario").value(DEFAULT_VAR_NOM_USUARIO.toString()))
            .andExpect(jsonPath("$.varLoginUsuario").value(DEFAULT_VAR_LOGIN_USUARIO.toString()))
            .andExpect(jsonPath("$.varInicialUsuario").value(DEFAULT_VAR_INICIAL_USUARIO.toString()))
            .andExpect(jsonPath("$.numEstUsuario").value(DEFAULT_NUM_EST_USUARIO))
            .andExpect(jsonPath("$.varMotivUsuario").value(DEFAULT_VAR_MOTIV_USUARIO.toString()))
            .andExpect(jsonPath("$.varDocContacto").value(DEFAULT_VAR_DOC_CONTACTO.toString()))
            .andExpect(jsonPath("$.varNomContacto").value(DEFAULT_VAR_NOM_CONTACTO.toString()))
            .andExpect(jsonPath("$.varRefContacto").value(DEFAULT_VAR_REF_CONTACTO.toString()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR))
            .andExpect(jsonPath("$.varDireccionIp").value(DEFAULT_VAR_DIRECCION_IP.toString()))
            .andExpect(jsonPath("$.varMacAddress").value(DEFAULT_VAR_MAC_ADDRESS.toString()))
            .andExpect(jsonPath("$.varNombrePc").value(DEFAULT_VAR_NOMBRE_PC.toString()))
            .andExpect(jsonPath("$.varCodSeguridad").value(DEFAULT_VAR_COD_SEGURIDAD.toString()))
            .andExpect(jsonPath("$.datFecTermino").value(DEFAULT_DAT_FEC_TERMINO.toString()))
            .andExpect(jsonPath("$.varPtoControl").value(DEFAULT_VAR_PTO_CONTROL));
    }

    @Test
    @Transactional
    public void getNonExistingUsuario() throws Exception {
        // Get the usuario
        restUsuarioMockMvc.perform(get("/api/usuarios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUsuario() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);
        usuarioSearchRepository.save(usuario);
        int databaseSizeBeforeUpdate = usuarioRepository.findAll().size();

        // Update the usuario
        Usuario updatedUsuario = usuarioRepository.findOne(usuario.getId());
        updatedUsuario
            .varNomUsuario(UPDATED_VAR_NOM_USUARIO)
            .varLoginUsuario(UPDATED_VAR_LOGIN_USUARIO)
            .varInicialUsuario(UPDATED_VAR_INICIAL_USUARIO)
            .numEstUsuario(UPDATED_NUM_EST_USUARIO)
            .varMotivUsuario(UPDATED_VAR_MOTIV_USUARIO)
            .varDocContacto(UPDATED_VAR_DOC_CONTACTO)
            .varNomContacto(UPDATED_VAR_NOM_CONTACTO)
            .varRefContacto(UPDATED_VAR_REF_CONTACTO)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR)
            .varDireccionIp(UPDATED_VAR_DIRECCION_IP)
            .varMacAddress(UPDATED_VAR_MAC_ADDRESS)
            .varNombrePc(UPDATED_VAR_NOMBRE_PC)
            .varCodSeguridad(UPDATED_VAR_COD_SEGURIDAD)
            .datFecTermino(UPDATED_DAT_FEC_TERMINO)
            .varPtoControl(UPDATED_VAR_PTO_CONTROL);

        restUsuarioMockMvc.perform(put("/api/usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUsuario)))
            .andExpect(status().isOk());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeUpdate);
        Usuario testUsuario = usuarioList.get(usuarioList.size() - 1);
        assertThat(testUsuario.getVarNomUsuario()).isEqualTo(UPDATED_VAR_NOM_USUARIO);
        assertThat(testUsuario.getVarLoginUsuario()).isEqualTo(UPDATED_VAR_LOGIN_USUARIO);
        assertThat(testUsuario.getVarInicialUsuario()).isEqualTo(UPDATED_VAR_INICIAL_USUARIO);
        assertThat(testUsuario.getNumEstUsuario()).isEqualTo(UPDATED_NUM_EST_USUARIO);
        assertThat(testUsuario.getVarMotivUsuario()).isEqualTo(UPDATED_VAR_MOTIV_USUARIO);
        assertThat(testUsuario.getVarDocContacto()).isEqualTo(UPDATED_VAR_DOC_CONTACTO);
        assertThat(testUsuario.getVarNomContacto()).isEqualTo(UPDATED_VAR_NOM_CONTACTO);
        assertThat(testUsuario.getVarRefContacto()).isEqualTo(UPDATED_VAR_REF_CONTACTO);
        assertThat(testUsuario.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testUsuario.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testUsuario.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);
        assertThat(testUsuario.getVarDireccionIp()).isEqualTo(UPDATED_VAR_DIRECCION_IP);
        assertThat(testUsuario.getVarMacAddress()).isEqualTo(UPDATED_VAR_MAC_ADDRESS);
        assertThat(testUsuario.getVarNombrePc()).isEqualTo(UPDATED_VAR_NOMBRE_PC);
        assertThat(testUsuario.getVarCodSeguridad()).isEqualTo(UPDATED_VAR_COD_SEGURIDAD);
        assertThat(testUsuario.getDatFecTermino()).isEqualTo(UPDATED_DAT_FEC_TERMINO);
        assertThat(testUsuario.getVarPtoControl()).isEqualTo(UPDATED_VAR_PTO_CONTROL);

        // Validate the Usuario in Elasticsearch
        Usuario usuarioEs = usuarioSearchRepository.findOne(testUsuario.getId());
        assertThat(usuarioEs).isEqualToComparingFieldByField(testUsuario);
    }

    @Test
    @Transactional
    public void updateNonExistingUsuario() throws Exception {
        int databaseSizeBeforeUpdate = usuarioRepository.findAll().size();

        // Create the Usuario

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUsuarioMockMvc.perform(put("/api/usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuario)))
            .andExpect(status().isCreated());

        // Validate the Usuario in the database
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUsuario() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);
        usuarioSearchRepository.save(usuario);
        int databaseSizeBeforeDelete = usuarioRepository.findAll().size();

        // Get the usuario
        restUsuarioMockMvc.perform(delete("/api/usuarios/{id}", usuario.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean usuarioExistsInEs = usuarioSearchRepository.exists(usuario.getId());
        assertThat(usuarioExistsInEs).isFalse();

        // Validate the database is empty
        List<Usuario> usuarioList = usuarioRepository.findAll();
        assertThat(usuarioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUsuario() throws Exception {
        // Initialize the database
        usuarioRepository.saveAndFlush(usuario);
        usuarioSearchRepository.save(usuario);

        // Search the usuario
        restUsuarioMockMvc.perform(get("/api/_search/usuarios?query=id:" + usuario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuario.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomUsuario").value(hasItem(DEFAULT_VAR_NOM_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].varLoginUsuario").value(hasItem(DEFAULT_VAR_LOGIN_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].varInicialUsuario").value(hasItem(DEFAULT_VAR_INICIAL_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].numEstUsuario").value(hasItem(DEFAULT_NUM_EST_USUARIO)))
            .andExpect(jsonPath("$.[*].varMotivUsuario").value(hasItem(DEFAULT_VAR_MOTIV_USUARIO.toString())))
            .andExpect(jsonPath("$.[*].varDocContacto").value(hasItem(DEFAULT_VAR_DOC_CONTACTO.toString())))
            .andExpect(jsonPath("$.[*].varNomContacto").value(hasItem(DEFAULT_VAR_NOM_CONTACTO.toString())))
            .andExpect(jsonPath("$.[*].varRefContacto").value(hasItem(DEFAULT_VAR_REF_CONTACTO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)))
            .andExpect(jsonPath("$.[*].varDireccionIp").value(hasItem(DEFAULT_VAR_DIRECCION_IP.toString())))
            .andExpect(jsonPath("$.[*].varMacAddress").value(hasItem(DEFAULT_VAR_MAC_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].varNombrePc").value(hasItem(DEFAULT_VAR_NOMBRE_PC.toString())))
            .andExpect(jsonPath("$.[*].varCodSeguridad").value(hasItem(DEFAULT_VAR_COD_SEGURIDAD.toString())))
            .andExpect(jsonPath("$.[*].datFecTermino").value(hasItem(DEFAULT_DAT_FEC_TERMINO.toString())))
            .andExpect(jsonPath("$.[*].varPtoControl").value(hasItem(DEFAULT_VAR_PTO_CONTROL)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Usuario.class);
        Usuario usuario1 = new Usuario();
        usuario1.setId(1L);
        Usuario usuario2 = new Usuario();
        usuario2.setId(usuario1.getId());
        assertThat(usuario1).isEqualTo(usuario2);
        usuario2.setId(2L);
        assertThat(usuario1).isNotEqualTo(usuario2);
        usuario1.setId(null);
        assertThat(usuario1).isNotEqualTo(usuario2);
    }
}
