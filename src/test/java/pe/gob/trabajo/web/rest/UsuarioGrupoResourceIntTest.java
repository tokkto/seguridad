package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.UsuarioGrupo;
import pe.gob.trabajo.repository.UsuarioGrupoRepository;
import pe.gob.trabajo.repository.search.UsuarioGrupoSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UsuarioGrupoResource REST controller.
 *
 * @see UsuarioGrupoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class UsuarioGrupoResourceIntTest {

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private UsuarioGrupoRepository usuarioGrupoRepository;

    @Autowired
    private UsuarioGrupoSearchRepository usuarioGrupoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUsuarioGrupoMockMvc;

    private UsuarioGrupo usuarioGrupo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UsuarioGrupoResource usuarioGrupoResource = new UsuarioGrupoResource(usuarioGrupoRepository, usuarioGrupoSearchRepository);
        this.restUsuarioGrupoMockMvc = MockMvcBuilders.standaloneSetup(usuarioGrupoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UsuarioGrupo createEntity(EntityManager em) {
        UsuarioGrupo usuarioGrupo = new UsuarioGrupo()
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return usuarioGrupo;
    }

    @Before
    public void initTest() {
        usuarioGrupoSearchRepository.deleteAll();
        usuarioGrupo = createEntity(em);
    }

    @Test
    @Transactional
    public void createUsuarioGrupo() throws Exception {
        int databaseSizeBeforeCreate = usuarioGrupoRepository.findAll().size();

        // Create the UsuarioGrupo
        restUsuarioGrupoMockMvc.perform(post("/api/usuario-grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioGrupo)))
            .andExpect(status().isCreated());

        // Validate the UsuarioGrupo in the database
        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeCreate + 1);
        UsuarioGrupo testUsuarioGrupo = usuarioGrupoList.get(usuarioGrupoList.size() - 1);
        assertThat(testUsuarioGrupo.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testUsuarioGrupo.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testUsuarioGrupo.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the UsuarioGrupo in Elasticsearch
        UsuarioGrupo usuarioGrupoEs = usuarioGrupoSearchRepository.findOne(testUsuarioGrupo.getId());
        assertThat(usuarioGrupoEs).isEqualToComparingFieldByField(testUsuarioGrupo);
    }

    @Test
    @Transactional
    public void createUsuarioGrupoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = usuarioGrupoRepository.findAll().size();

        // Create the UsuarioGrupo with an existing ID
        usuarioGrupo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUsuarioGrupoMockMvc.perform(post("/api/usuario-grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioGrupo)))
            .andExpect(status().isBadRequest());

        // Validate the UsuarioGrupo in the database
        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioGrupoRepository.findAll().size();
        // set the field null
        usuarioGrupo.setVarUsuarioLog(null);

        // Create the UsuarioGrupo, which fails.

        restUsuarioGrupoMockMvc.perform(post("/api/usuario-grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioGrupo)))
            .andExpect(status().isBadRequest());

        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioGrupoRepository.findAll().size();
        // set the field null
        usuarioGrupo.setDatFechaLog(null);

        // Create the UsuarioGrupo, which fails.

        restUsuarioGrupoMockMvc.perform(post("/api/usuario-grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioGrupo)))
            .andExpect(status().isBadRequest());

        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioGrupoRepository.findAll().size();
        // set the field null
        usuarioGrupo.setNumEliminar(null);

        // Create the UsuarioGrupo, which fails.

        restUsuarioGrupoMockMvc.perform(post("/api/usuario-grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioGrupo)))
            .andExpect(status().isBadRequest());

        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUsuarioGrupos() throws Exception {
        // Initialize the database
        usuarioGrupoRepository.saveAndFlush(usuarioGrupo);

        // Get all the usuarioGrupoList
        restUsuarioGrupoMockMvc.perform(get("/api/usuario-grupos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuarioGrupo.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getUsuarioGrupo() throws Exception {
        // Initialize the database
        usuarioGrupoRepository.saveAndFlush(usuarioGrupo);

        // Get the usuarioGrupo
        restUsuarioGrupoMockMvc.perform(get("/api/usuario-grupos/{id}", usuarioGrupo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(usuarioGrupo.getId().intValue()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingUsuarioGrupo() throws Exception {
        // Get the usuarioGrupo
        restUsuarioGrupoMockMvc.perform(get("/api/usuario-grupos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUsuarioGrupo() throws Exception {
        // Initialize the database
        usuarioGrupoRepository.saveAndFlush(usuarioGrupo);
        usuarioGrupoSearchRepository.save(usuarioGrupo);
        int databaseSizeBeforeUpdate = usuarioGrupoRepository.findAll().size();

        // Update the usuarioGrupo
        UsuarioGrupo updatedUsuarioGrupo = usuarioGrupoRepository.findOne(usuarioGrupo.getId());
        updatedUsuarioGrupo
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restUsuarioGrupoMockMvc.perform(put("/api/usuario-grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUsuarioGrupo)))
            .andExpect(status().isOk());

        // Validate the UsuarioGrupo in the database
        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeUpdate);
        UsuarioGrupo testUsuarioGrupo = usuarioGrupoList.get(usuarioGrupoList.size() - 1);
        assertThat(testUsuarioGrupo.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testUsuarioGrupo.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testUsuarioGrupo.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the UsuarioGrupo in Elasticsearch
        UsuarioGrupo usuarioGrupoEs = usuarioGrupoSearchRepository.findOne(testUsuarioGrupo.getId());
        assertThat(usuarioGrupoEs).isEqualToComparingFieldByField(testUsuarioGrupo);
    }

    @Test
    @Transactional
    public void updateNonExistingUsuarioGrupo() throws Exception {
        int databaseSizeBeforeUpdate = usuarioGrupoRepository.findAll().size();

        // Create the UsuarioGrupo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUsuarioGrupoMockMvc.perform(put("/api/usuario-grupos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioGrupo)))
            .andExpect(status().isCreated());

        // Validate the UsuarioGrupo in the database
        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUsuarioGrupo() throws Exception {
        // Initialize the database
        usuarioGrupoRepository.saveAndFlush(usuarioGrupo);
        usuarioGrupoSearchRepository.save(usuarioGrupo);
        int databaseSizeBeforeDelete = usuarioGrupoRepository.findAll().size();

        // Get the usuarioGrupo
        restUsuarioGrupoMockMvc.perform(delete("/api/usuario-grupos/{id}", usuarioGrupo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean usuarioGrupoExistsInEs = usuarioGrupoSearchRepository.exists(usuarioGrupo.getId());
        assertThat(usuarioGrupoExistsInEs).isFalse();

        // Validate the database is empty
        List<UsuarioGrupo> usuarioGrupoList = usuarioGrupoRepository.findAll();
        assertThat(usuarioGrupoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUsuarioGrupo() throws Exception {
        // Initialize the database
        usuarioGrupoRepository.saveAndFlush(usuarioGrupo);
        usuarioGrupoSearchRepository.save(usuarioGrupo);

        // Search the usuarioGrupo
        restUsuarioGrupoMockMvc.perform(get("/api/_search/usuario-grupos?query=id:" + usuarioGrupo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuarioGrupo.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UsuarioGrupo.class);
        UsuarioGrupo usuarioGrupo1 = new UsuarioGrupo();
        usuarioGrupo1.setId(1L);
        UsuarioGrupo usuarioGrupo2 = new UsuarioGrupo();
        usuarioGrupo2.setId(usuarioGrupo1.getId());
        assertThat(usuarioGrupo1).isEqualTo(usuarioGrupo2);
        usuarioGrupo2.setId(2L);
        assertThat(usuarioGrupo1).isNotEqualTo(usuarioGrupo2);
        usuarioGrupo1.setId(null);
        assertThat(usuarioGrupo1).isNotEqualTo(usuarioGrupo2);
    }
}
