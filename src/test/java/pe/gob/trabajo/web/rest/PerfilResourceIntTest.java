package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.Perfil;
import pe.gob.trabajo.repository.PerfilRepository;
import pe.gob.trabajo.repository.search.PerfilSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PerfilResource REST controller.
 *
 * @see PerfilResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class PerfilResourceIntTest {

    private static final String DEFAULT_VAR_NOM_PERFIL = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_PERFIL = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_DESC_PERFIL = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DESC_PERFIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_EST_PERFIL = 1;
    private static final Integer UPDATED_NUM_EST_PERFIL = 2;

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private PerfilRepository perfilRepository;

    @Autowired
    private PerfilSearchRepository perfilSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPerfilMockMvc;

    private Perfil perfil;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PerfilResource perfilResource = new PerfilResource(perfilRepository, perfilSearchRepository);
        this.restPerfilMockMvc = MockMvcBuilders.standaloneSetup(perfilResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Perfil createEntity(EntityManager em) {
        Perfil perfil = new Perfil()
            .varNomPerfil(DEFAULT_VAR_NOM_PERFIL)
            .varDescPerfil(DEFAULT_VAR_DESC_PERFIL)
            .numEstPerfil(DEFAULT_NUM_EST_PERFIL)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return perfil;
    }

    @Before
    public void initTest() {
        perfilSearchRepository.deleteAll();
        perfil = createEntity(em);
    }

    @Test
    @Transactional
    public void createPerfil() throws Exception {
        int databaseSizeBeforeCreate = perfilRepository.findAll().size();

        // Create the Perfil
        restPerfilMockMvc.perform(post("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isCreated());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeCreate + 1);
        Perfil testPerfil = perfilList.get(perfilList.size() - 1);
        assertThat(testPerfil.getVarNomPerfil()).isEqualTo(DEFAULT_VAR_NOM_PERFIL);
        assertThat(testPerfil.getVarDescPerfil()).isEqualTo(DEFAULT_VAR_DESC_PERFIL);
        assertThat(testPerfil.getNumEstPerfil()).isEqualTo(DEFAULT_NUM_EST_PERFIL);
        assertThat(testPerfil.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testPerfil.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testPerfil.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the Perfil in Elasticsearch
        Perfil perfilEs = perfilSearchRepository.findOne(testPerfil.getId());
        assertThat(perfilEs).isEqualToComparingFieldByField(testPerfil);
    }

    @Test
    @Transactional
    public void createPerfilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = perfilRepository.findAll().size();

        // Create the Perfil with an existing ID
        perfil.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPerfilMockMvc.perform(post("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isBadRequest());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = perfilRepository.findAll().size();
        // set the field null
        perfil.setVarUsuarioLog(null);

        // Create the Perfil, which fails.

        restPerfilMockMvc.perform(post("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isBadRequest());

        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = perfilRepository.findAll().size();
        // set the field null
        perfil.setDatFechaLog(null);

        // Create the Perfil, which fails.

        restPerfilMockMvc.perform(post("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isBadRequest());

        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = perfilRepository.findAll().size();
        // set the field null
        perfil.setNumEliminar(null);

        // Create the Perfil, which fails.

        restPerfilMockMvc.perform(post("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isBadRequest());

        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPerfils() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList
        restPerfilMockMvc.perform(get("/api/perfils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perfil.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomPerfil").value(hasItem(DEFAULT_VAR_NOM_PERFIL.toString())))
            .andExpect(jsonPath("$.[*].varDescPerfil").value(hasItem(DEFAULT_VAR_DESC_PERFIL.toString())))
            .andExpect(jsonPath("$.[*].numEstPerfil").value(hasItem(DEFAULT_NUM_EST_PERFIL)))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getPerfil() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get the perfil
        restPerfilMockMvc.perform(get("/api/perfils/{id}", perfil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(perfil.getId().intValue()))
            .andExpect(jsonPath("$.varNomPerfil").value(DEFAULT_VAR_NOM_PERFIL.toString()))
            .andExpect(jsonPath("$.varDescPerfil").value(DEFAULT_VAR_DESC_PERFIL.toString()))
            .andExpect(jsonPath("$.numEstPerfil").value(DEFAULT_NUM_EST_PERFIL))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingPerfil() throws Exception {
        // Get the perfil
        restPerfilMockMvc.perform(get("/api/perfils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerfil() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);
        perfilSearchRepository.save(perfil);
        int databaseSizeBeforeUpdate = perfilRepository.findAll().size();

        // Update the perfil
        Perfil updatedPerfil = perfilRepository.findOne(perfil.getId());
        updatedPerfil
            .varNomPerfil(UPDATED_VAR_NOM_PERFIL)
            .varDescPerfil(UPDATED_VAR_DESC_PERFIL)
            .numEstPerfil(UPDATED_NUM_EST_PERFIL)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restPerfilMockMvc.perform(put("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPerfil)))
            .andExpect(status().isOk());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeUpdate);
        Perfil testPerfil = perfilList.get(perfilList.size() - 1);
        assertThat(testPerfil.getVarNomPerfil()).isEqualTo(UPDATED_VAR_NOM_PERFIL);
        assertThat(testPerfil.getVarDescPerfil()).isEqualTo(UPDATED_VAR_DESC_PERFIL);
        assertThat(testPerfil.getNumEstPerfil()).isEqualTo(UPDATED_NUM_EST_PERFIL);
        assertThat(testPerfil.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testPerfil.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testPerfil.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the Perfil in Elasticsearch
        Perfil perfilEs = perfilSearchRepository.findOne(testPerfil.getId());
        assertThat(perfilEs).isEqualToComparingFieldByField(testPerfil);
    }

    @Test
    @Transactional
    public void updateNonExistingPerfil() throws Exception {
        int databaseSizeBeforeUpdate = perfilRepository.findAll().size();

        // Create the Perfil

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPerfilMockMvc.perform(put("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isCreated());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePerfil() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);
        perfilSearchRepository.save(perfil);
        int databaseSizeBeforeDelete = perfilRepository.findAll().size();

        // Get the perfil
        restPerfilMockMvc.perform(delete("/api/perfils/{id}", perfil.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean perfilExistsInEs = perfilSearchRepository.exists(perfil.getId());
        assertThat(perfilExistsInEs).isFalse();

        // Validate the database is empty
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPerfil() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);
        perfilSearchRepository.save(perfil);

        // Search the perfil
        restPerfilMockMvc.perform(get("/api/_search/perfils?query=id:" + perfil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perfil.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomPerfil").value(hasItem(DEFAULT_VAR_NOM_PERFIL.toString())))
            .andExpect(jsonPath("$.[*].varDescPerfil").value(hasItem(DEFAULT_VAR_DESC_PERFIL.toString())))
            .andExpect(jsonPath("$.[*].numEstPerfil").value(hasItem(DEFAULT_NUM_EST_PERFIL)))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Perfil.class);
        Perfil perfil1 = new Perfil();
        perfil1.setId(1L);
        Perfil perfil2 = new Perfil();
        perfil2.setId(perfil1.getId());
        assertThat(perfil1).isEqualTo(perfil2);
        perfil2.setId(2L);
        assertThat(perfil1).isNotEqualTo(perfil2);
        perfil1.setId(null);
        assertThat(perfil1).isNotEqualTo(perfil2);
    }
}
