package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.UsuarioHorario;
import pe.gob.trabajo.repository.UsuarioHorarioRepository;
import pe.gob.trabajo.repository.search.UsuarioHorarioSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UsuarioHorarioResource REST controller.
 *
 * @see UsuarioHorarioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class UsuarioHorarioResourceIntTest {

    private static final Integer DEFAULT_NUM_DIA_SEMANA = 1;
    private static final Integer UPDATED_NUM_DIA_SEMANA = 2;

    private static final LocalDate DEFAULT_DAT_HORA_INICIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DAT_HORA_INICIO = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DAT_HORA_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DAT_HORA_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private UsuarioHorarioRepository usuarioHorarioRepository;

    @Autowired
    private UsuarioHorarioSearchRepository usuarioHorarioSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUsuarioHorarioMockMvc;

    private UsuarioHorario usuarioHorario;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UsuarioHorarioResource usuarioHorarioResource = new UsuarioHorarioResource(usuarioHorarioRepository, usuarioHorarioSearchRepository);
        this.restUsuarioHorarioMockMvc = MockMvcBuilders.standaloneSetup(usuarioHorarioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UsuarioHorario createEntity(EntityManager em) {
        UsuarioHorario usuarioHorario = new UsuarioHorario()
            .numDiaSemana(DEFAULT_NUM_DIA_SEMANA)
            .datHoraInicio(DEFAULT_DAT_HORA_INICIO)
            .datHoraFin(DEFAULT_DAT_HORA_FIN)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return usuarioHorario;
    }

    @Before
    public void initTest() {
        usuarioHorarioSearchRepository.deleteAll();
        usuarioHorario = createEntity(em);
    }

    @Test
    @Transactional
    public void createUsuarioHorario() throws Exception {
        int databaseSizeBeforeCreate = usuarioHorarioRepository.findAll().size();

        // Create the UsuarioHorario
        restUsuarioHorarioMockMvc.perform(post("/api/usuario-horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioHorario)))
            .andExpect(status().isCreated());

        // Validate the UsuarioHorario in the database
        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeCreate + 1);
        UsuarioHorario testUsuarioHorario = usuarioHorarioList.get(usuarioHorarioList.size() - 1);
        assertThat(testUsuarioHorario.getNumDiaSemana()).isEqualTo(DEFAULT_NUM_DIA_SEMANA);
        assertThat(testUsuarioHorario.getDatHoraInicio()).isEqualTo(DEFAULT_DAT_HORA_INICIO);
        assertThat(testUsuarioHorario.getDatHoraFin()).isEqualTo(DEFAULT_DAT_HORA_FIN);
        assertThat(testUsuarioHorario.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testUsuarioHorario.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testUsuarioHorario.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the UsuarioHorario in Elasticsearch
        UsuarioHorario usuarioHorarioEs = usuarioHorarioSearchRepository.findOne(testUsuarioHorario.getId());
        assertThat(usuarioHorarioEs).isEqualToComparingFieldByField(testUsuarioHorario);
    }

    @Test
    @Transactional
    public void createUsuarioHorarioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = usuarioHorarioRepository.findAll().size();

        // Create the UsuarioHorario with an existing ID
        usuarioHorario.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUsuarioHorarioMockMvc.perform(post("/api/usuario-horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioHorario)))
            .andExpect(status().isBadRequest());

        // Validate the UsuarioHorario in the database
        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioHorarioRepository.findAll().size();
        // set the field null
        usuarioHorario.setVarUsuarioLog(null);

        // Create the UsuarioHorario, which fails.

        restUsuarioHorarioMockMvc.perform(post("/api/usuario-horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioHorario)))
            .andExpect(status().isBadRequest());

        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioHorarioRepository.findAll().size();
        // set the field null
        usuarioHorario.setDatFechaLog(null);

        // Create the UsuarioHorario, which fails.

        restUsuarioHorarioMockMvc.perform(post("/api/usuario-horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioHorario)))
            .andExpect(status().isBadRequest());

        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = usuarioHorarioRepository.findAll().size();
        // set the field null
        usuarioHorario.setNumEliminar(null);

        // Create the UsuarioHorario, which fails.

        restUsuarioHorarioMockMvc.perform(post("/api/usuario-horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioHorario)))
            .andExpect(status().isBadRequest());

        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUsuarioHorarios() throws Exception {
        // Initialize the database
        usuarioHorarioRepository.saveAndFlush(usuarioHorario);

        // Get all the usuarioHorarioList
        restUsuarioHorarioMockMvc.perform(get("/api/usuario-horarios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuarioHorario.getId().intValue())))
            .andExpect(jsonPath("$.[*].numDiaSemana").value(hasItem(DEFAULT_NUM_DIA_SEMANA)))
            .andExpect(jsonPath("$.[*].datHoraInicio").value(hasItem(DEFAULT_DAT_HORA_INICIO.toString())))
            .andExpect(jsonPath("$.[*].datHoraFin").value(hasItem(DEFAULT_DAT_HORA_FIN.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getUsuarioHorario() throws Exception {
        // Initialize the database
        usuarioHorarioRepository.saveAndFlush(usuarioHorario);

        // Get the usuarioHorario
        restUsuarioHorarioMockMvc.perform(get("/api/usuario-horarios/{id}", usuarioHorario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(usuarioHorario.getId().intValue()))
            .andExpect(jsonPath("$.numDiaSemana").value(DEFAULT_NUM_DIA_SEMANA))
            .andExpect(jsonPath("$.datHoraInicio").value(DEFAULT_DAT_HORA_INICIO.toString()))
            .andExpect(jsonPath("$.datHoraFin").value(DEFAULT_DAT_HORA_FIN.toString()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingUsuarioHorario() throws Exception {
        // Get the usuarioHorario
        restUsuarioHorarioMockMvc.perform(get("/api/usuario-horarios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUsuarioHorario() throws Exception {
        // Initialize the database
        usuarioHorarioRepository.saveAndFlush(usuarioHorario);
        usuarioHorarioSearchRepository.save(usuarioHorario);
        int databaseSizeBeforeUpdate = usuarioHorarioRepository.findAll().size();

        // Update the usuarioHorario
        UsuarioHorario updatedUsuarioHorario = usuarioHorarioRepository.findOne(usuarioHorario.getId());
        updatedUsuarioHorario
            .numDiaSemana(UPDATED_NUM_DIA_SEMANA)
            .datHoraInicio(UPDATED_DAT_HORA_INICIO)
            .datHoraFin(UPDATED_DAT_HORA_FIN)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restUsuarioHorarioMockMvc.perform(put("/api/usuario-horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUsuarioHorario)))
            .andExpect(status().isOk());

        // Validate the UsuarioHorario in the database
        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeUpdate);
        UsuarioHorario testUsuarioHorario = usuarioHorarioList.get(usuarioHorarioList.size() - 1);
        assertThat(testUsuarioHorario.getNumDiaSemana()).isEqualTo(UPDATED_NUM_DIA_SEMANA);
        assertThat(testUsuarioHorario.getDatHoraInicio()).isEqualTo(UPDATED_DAT_HORA_INICIO);
        assertThat(testUsuarioHorario.getDatHoraFin()).isEqualTo(UPDATED_DAT_HORA_FIN);
        assertThat(testUsuarioHorario.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testUsuarioHorario.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testUsuarioHorario.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the UsuarioHorario in Elasticsearch
        UsuarioHorario usuarioHorarioEs = usuarioHorarioSearchRepository.findOne(testUsuarioHorario.getId());
        assertThat(usuarioHorarioEs).isEqualToComparingFieldByField(testUsuarioHorario);
    }

    @Test
    @Transactional
    public void updateNonExistingUsuarioHorario() throws Exception {
        int databaseSizeBeforeUpdate = usuarioHorarioRepository.findAll().size();

        // Create the UsuarioHorario

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUsuarioHorarioMockMvc.perform(put("/api/usuario-horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usuarioHorario)))
            .andExpect(status().isCreated());

        // Validate the UsuarioHorario in the database
        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUsuarioHorario() throws Exception {
        // Initialize the database
        usuarioHorarioRepository.saveAndFlush(usuarioHorario);
        usuarioHorarioSearchRepository.save(usuarioHorario);
        int databaseSizeBeforeDelete = usuarioHorarioRepository.findAll().size();

        // Get the usuarioHorario
        restUsuarioHorarioMockMvc.perform(delete("/api/usuario-horarios/{id}", usuarioHorario.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean usuarioHorarioExistsInEs = usuarioHorarioSearchRepository.exists(usuarioHorario.getId());
        assertThat(usuarioHorarioExistsInEs).isFalse();

        // Validate the database is empty
        List<UsuarioHorario> usuarioHorarioList = usuarioHorarioRepository.findAll();
        assertThat(usuarioHorarioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUsuarioHorario() throws Exception {
        // Initialize the database
        usuarioHorarioRepository.saveAndFlush(usuarioHorario);
        usuarioHorarioSearchRepository.save(usuarioHorario);

        // Search the usuarioHorario
        restUsuarioHorarioMockMvc.perform(get("/api/_search/usuario-horarios?query=id:" + usuarioHorario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(usuarioHorario.getId().intValue())))
            .andExpect(jsonPath("$.[*].numDiaSemana").value(hasItem(DEFAULT_NUM_DIA_SEMANA)))
            .andExpect(jsonPath("$.[*].datHoraInicio").value(hasItem(DEFAULT_DAT_HORA_INICIO.toString())))
            .andExpect(jsonPath("$.[*].datHoraFin").value(hasItem(DEFAULT_DAT_HORA_FIN.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UsuarioHorario.class);
        UsuarioHorario usuarioHorario1 = new UsuarioHorario();
        usuarioHorario1.setId(1L);
        UsuarioHorario usuarioHorario2 = new UsuarioHorario();
        usuarioHorario2.setId(usuarioHorario1.getId());
        assertThat(usuarioHorario1).isEqualTo(usuarioHorario2);
        usuarioHorario2.setId(2L);
        assertThat(usuarioHorario1).isNotEqualTo(usuarioHorario2);
        usuarioHorario1.setId(null);
        assertThat(usuarioHorario1).isNotEqualTo(usuarioHorario2);
    }
}
