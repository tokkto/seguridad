package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.Menu;
import pe.gob.trabajo.repository.MenuRepository;
import pe.gob.trabajo.repository.search.MenuSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MenuResource REST controller.
 *
 * @see MenuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class MenuResourceIntTest {

    private static final String DEFAULT_VAR_NOM_MENU = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_MENU = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_ORDEN_ITEM = 1;
    private static final Integer UPDATED_NUM_ORDEN_ITEM = 2;

    private static final String DEFAULT_VAR_URL_MENU = "AAAAAAAAAA";
    private static final String UPDATED_VAR_URL_MENU = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_OPCION = 1;
    private static final Integer UPDATED_NUM_OPCION = 2;

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private MenuSearchRepository menuSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMenuMockMvc;

    private Menu menu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MenuResource menuResource = new MenuResource(menuRepository, menuSearchRepository);
        this.restMenuMockMvc = MockMvcBuilders.standaloneSetup(menuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Menu createEntity(EntityManager em) {
        Menu menu = new Menu()
            .varNomMenu(DEFAULT_VAR_NOM_MENU)
            .numOrdenItem(DEFAULT_NUM_ORDEN_ITEM)
            .varUrlMenu(DEFAULT_VAR_URL_MENU)
            .numOpcion(DEFAULT_NUM_OPCION)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return menu;
    }

    @Before
    public void initTest() {
        menuSearchRepository.deleteAll();
        menu = createEntity(em);
    }

    @Test
    @Transactional
    public void createMenu() throws Exception {
        int databaseSizeBeforeCreate = menuRepository.findAll().size();

        // Create the Menu
        restMenuMockMvc.perform(post("/api/menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menu)))
            .andExpect(status().isCreated());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeCreate + 1);
        Menu testMenu = menuList.get(menuList.size() - 1);
        assertThat(testMenu.getVarNomMenu()).isEqualTo(DEFAULT_VAR_NOM_MENU);
        assertThat(testMenu.getNumOrdenItem()).isEqualTo(DEFAULT_NUM_ORDEN_ITEM);
        assertThat(testMenu.getVarUrlMenu()).isEqualTo(DEFAULT_VAR_URL_MENU);
        assertThat(testMenu.getNumOpcion()).isEqualTo(DEFAULT_NUM_OPCION);
        assertThat(testMenu.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testMenu.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testMenu.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the Menu in Elasticsearch
        Menu menuEs = menuSearchRepository.findOne(testMenu.getId());
        assertThat(menuEs).isEqualToComparingFieldByField(testMenu);
    }

    @Test
    @Transactional
    public void createMenuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = menuRepository.findAll().size();

        // Create the Menu with an existing ID
        menu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMenuMockMvc.perform(post("/api/menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menu)))
            .andExpect(status().isBadRequest());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuRepository.findAll().size();
        // set the field null
        menu.setVarUsuarioLog(null);

        // Create the Menu, which fails.

        restMenuMockMvc.perform(post("/api/menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menu)))
            .andExpect(status().isBadRequest());

        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuRepository.findAll().size();
        // set the field null
        menu.setDatFechaLog(null);

        // Create the Menu, which fails.

        restMenuMockMvc.perform(post("/api/menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menu)))
            .andExpect(status().isBadRequest());

        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuRepository.findAll().size();
        // set the field null
        menu.setNumEliminar(null);

        // Create the Menu, which fails.

        restMenuMockMvc.perform(post("/api/menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menu)))
            .andExpect(status().isBadRequest());

        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMenus() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get all the menuList
        restMenuMockMvc.perform(get("/api/menus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menu.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomMenu").value(hasItem(DEFAULT_VAR_NOM_MENU.toString())))
            .andExpect(jsonPath("$.[*].numOrdenItem").value(hasItem(DEFAULT_NUM_ORDEN_ITEM)))
            .andExpect(jsonPath("$.[*].varUrlMenu").value(hasItem(DEFAULT_VAR_URL_MENU.toString())))
            .andExpect(jsonPath("$.[*].numOpcion").value(hasItem(DEFAULT_NUM_OPCION)))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getMenu() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);

        // Get the menu
        restMenuMockMvc.perform(get("/api/menus/{id}", menu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(menu.getId().intValue()))
            .andExpect(jsonPath("$.varNomMenu").value(DEFAULT_VAR_NOM_MENU.toString()))
            .andExpect(jsonPath("$.numOrdenItem").value(DEFAULT_NUM_ORDEN_ITEM))
            .andExpect(jsonPath("$.varUrlMenu").value(DEFAULT_VAR_URL_MENU.toString()))
            .andExpect(jsonPath("$.numOpcion").value(DEFAULT_NUM_OPCION))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingMenu() throws Exception {
        // Get the menu
        restMenuMockMvc.perform(get("/api/menus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMenu() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);
        menuSearchRepository.save(menu);
        int databaseSizeBeforeUpdate = menuRepository.findAll().size();

        // Update the menu
        Menu updatedMenu = menuRepository.findOne(menu.getId());
        updatedMenu
            .varNomMenu(UPDATED_VAR_NOM_MENU)
            .numOrdenItem(UPDATED_NUM_ORDEN_ITEM)
            .varUrlMenu(UPDATED_VAR_URL_MENU)
            .numOpcion(UPDATED_NUM_OPCION)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restMenuMockMvc.perform(put("/api/menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMenu)))
            .andExpect(status().isOk());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeUpdate);
        Menu testMenu = menuList.get(menuList.size() - 1);
        assertThat(testMenu.getVarNomMenu()).isEqualTo(UPDATED_VAR_NOM_MENU);
        assertThat(testMenu.getNumOrdenItem()).isEqualTo(UPDATED_NUM_ORDEN_ITEM);
        assertThat(testMenu.getVarUrlMenu()).isEqualTo(UPDATED_VAR_URL_MENU);
        assertThat(testMenu.getNumOpcion()).isEqualTo(UPDATED_NUM_OPCION);
        assertThat(testMenu.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testMenu.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testMenu.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the Menu in Elasticsearch
        Menu menuEs = menuSearchRepository.findOne(testMenu.getId());
        assertThat(menuEs).isEqualToComparingFieldByField(testMenu);
    }

    @Test
    @Transactional
    public void updateNonExistingMenu() throws Exception {
        int databaseSizeBeforeUpdate = menuRepository.findAll().size();

        // Create the Menu

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMenuMockMvc.perform(put("/api/menus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menu)))
            .andExpect(status().isCreated());

        // Validate the Menu in the database
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMenu() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);
        menuSearchRepository.save(menu);
        int databaseSizeBeforeDelete = menuRepository.findAll().size();

        // Get the menu
        restMenuMockMvc.perform(delete("/api/menus/{id}", menu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean menuExistsInEs = menuSearchRepository.exists(menu.getId());
        assertThat(menuExistsInEs).isFalse();

        // Validate the database is empty
        List<Menu> menuList = menuRepository.findAll();
        assertThat(menuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMenu() throws Exception {
        // Initialize the database
        menuRepository.saveAndFlush(menu);
        menuSearchRepository.save(menu);

        // Search the menu
        restMenuMockMvc.perform(get("/api/_search/menus?query=id:" + menu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menu.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomMenu").value(hasItem(DEFAULT_VAR_NOM_MENU.toString())))
            .andExpect(jsonPath("$.[*].numOrdenItem").value(hasItem(DEFAULT_NUM_ORDEN_ITEM)))
            .andExpect(jsonPath("$.[*].varUrlMenu").value(hasItem(DEFAULT_VAR_URL_MENU.toString())))
            .andExpect(jsonPath("$.[*].numOpcion").value(hasItem(DEFAULT_NUM_OPCION)))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Menu.class);
        Menu menu1 = new Menu();
        menu1.setId(1L);
        Menu menu2 = new Menu();
        menu2.setId(menu1.getId());
        assertThat(menu1).isEqualTo(menu2);
        menu2.setId(2L);
        assertThat(menu1).isNotEqualTo(menu2);
        menu1.setId(null);
        assertThat(menu1).isNotEqualTo(menu2);
    }
}
