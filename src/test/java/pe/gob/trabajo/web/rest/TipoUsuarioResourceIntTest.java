package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.TipoUsuario;
import pe.gob.trabajo.repository.TipoUsuarioRepository;
import pe.gob.trabajo.repository.search.TipoUsuarioSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipoUsuarioResource REST controller.
 *
 * @see TipoUsuarioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class TipoUsuarioResourceIntTest {

    private static final String DEFAULT_VAR_NOM_TPUSUARIO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_NOM_TPUSUARIO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_DESC_TPUSUARIO = "AAAAAAAAAA";
    private static final String UPDATED_VAR_DESC_TPUSUARIO = "BBBBBBBBBB";

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private TipoUsuarioRepository tipoUsuarioRepository;

    @Autowired
    private TipoUsuarioSearchRepository tipoUsuarioSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipoUsuarioMockMvc;

    private TipoUsuario tipoUsuario;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoUsuarioResource tipoUsuarioResource = new TipoUsuarioResource(tipoUsuarioRepository, tipoUsuarioSearchRepository);
        this.restTipoUsuarioMockMvc = MockMvcBuilders.standaloneSetup(tipoUsuarioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoUsuario createEntity(EntityManager em) {
        TipoUsuario tipoUsuario = new TipoUsuario()
            .varNomTpusuario(DEFAULT_VAR_NOM_TPUSUARIO)
            .varDescTpusuario(DEFAULT_VAR_DESC_TPUSUARIO)
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return tipoUsuario;
    }

    @Before
    public void initTest() {
        tipoUsuarioSearchRepository.deleteAll();
        tipoUsuario = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoUsuario() throws Exception {
        int databaseSizeBeforeCreate = tipoUsuarioRepository.findAll().size();

        // Create the TipoUsuario
        restTipoUsuarioMockMvc.perform(post("/api/tipo-usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoUsuario)))
            .andExpect(status().isCreated());

        // Validate the TipoUsuario in the database
        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeCreate + 1);
        TipoUsuario testTipoUsuario = tipoUsuarioList.get(tipoUsuarioList.size() - 1);
        assertThat(testTipoUsuario.getVarNomTpusuario()).isEqualTo(DEFAULT_VAR_NOM_TPUSUARIO);
        assertThat(testTipoUsuario.getVarDescTpusuario()).isEqualTo(DEFAULT_VAR_DESC_TPUSUARIO);
        assertThat(testTipoUsuario.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testTipoUsuario.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testTipoUsuario.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the TipoUsuario in Elasticsearch
        TipoUsuario tipoUsuarioEs = tipoUsuarioSearchRepository.findOne(testTipoUsuario.getId());
        assertThat(tipoUsuarioEs).isEqualToComparingFieldByField(testTipoUsuario);
    }

    @Test
    @Transactional
    public void createTipoUsuarioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoUsuarioRepository.findAll().size();

        // Create the TipoUsuario with an existing ID
        tipoUsuario.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoUsuarioMockMvc.perform(post("/api/tipo-usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoUsuario)))
            .andExpect(status().isBadRequest());

        // Validate the TipoUsuario in the database
        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoUsuarioRepository.findAll().size();
        // set the field null
        tipoUsuario.setVarUsuarioLog(null);

        // Create the TipoUsuario, which fails.

        restTipoUsuarioMockMvc.perform(post("/api/tipo-usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoUsuario)))
            .andExpect(status().isBadRequest());

        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoUsuarioRepository.findAll().size();
        // set the field null
        tipoUsuario.setDatFechaLog(null);

        // Create the TipoUsuario, which fails.

        restTipoUsuarioMockMvc.perform(post("/api/tipo-usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoUsuario)))
            .andExpect(status().isBadRequest());

        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoUsuarioRepository.findAll().size();
        // set the field null
        tipoUsuario.setNumEliminar(null);

        // Create the TipoUsuario, which fails.

        restTipoUsuarioMockMvc.perform(post("/api/tipo-usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoUsuario)))
            .andExpect(status().isBadRequest());

        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipoUsuarios() throws Exception {
        // Initialize the database
        tipoUsuarioRepository.saveAndFlush(tipoUsuario);

        // Get all the tipoUsuarioList
        restTipoUsuarioMockMvc.perform(get("/api/tipo-usuarios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoUsuario.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomTpusuario").value(hasItem(DEFAULT_VAR_NOM_TPUSUARIO.toString())))
            .andExpect(jsonPath("$.[*].varDescTpusuario").value(hasItem(DEFAULT_VAR_DESC_TPUSUARIO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getTipoUsuario() throws Exception {
        // Initialize the database
        tipoUsuarioRepository.saveAndFlush(tipoUsuario);

        // Get the tipoUsuario
        restTipoUsuarioMockMvc.perform(get("/api/tipo-usuarios/{id}", tipoUsuario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoUsuario.getId().intValue()))
            .andExpect(jsonPath("$.varNomTpusuario").value(DEFAULT_VAR_NOM_TPUSUARIO.toString()))
            .andExpect(jsonPath("$.varDescTpusuario").value(DEFAULT_VAR_DESC_TPUSUARIO.toString()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingTipoUsuario() throws Exception {
        // Get the tipoUsuario
        restTipoUsuarioMockMvc.perform(get("/api/tipo-usuarios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoUsuario() throws Exception {
        // Initialize the database
        tipoUsuarioRepository.saveAndFlush(tipoUsuario);
        tipoUsuarioSearchRepository.save(tipoUsuario);
        int databaseSizeBeforeUpdate = tipoUsuarioRepository.findAll().size();

        // Update the tipoUsuario
        TipoUsuario updatedTipoUsuario = tipoUsuarioRepository.findOne(tipoUsuario.getId());
        updatedTipoUsuario
            .varNomTpusuario(UPDATED_VAR_NOM_TPUSUARIO)
            .varDescTpusuario(UPDATED_VAR_DESC_TPUSUARIO)
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restTipoUsuarioMockMvc.perform(put("/api/tipo-usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipoUsuario)))
            .andExpect(status().isOk());

        // Validate the TipoUsuario in the database
        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeUpdate);
        TipoUsuario testTipoUsuario = tipoUsuarioList.get(tipoUsuarioList.size() - 1);
        assertThat(testTipoUsuario.getVarNomTpusuario()).isEqualTo(UPDATED_VAR_NOM_TPUSUARIO);
        assertThat(testTipoUsuario.getVarDescTpusuario()).isEqualTo(UPDATED_VAR_DESC_TPUSUARIO);
        assertThat(testTipoUsuario.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testTipoUsuario.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testTipoUsuario.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the TipoUsuario in Elasticsearch
        TipoUsuario tipoUsuarioEs = tipoUsuarioSearchRepository.findOne(testTipoUsuario.getId());
        assertThat(tipoUsuarioEs).isEqualToComparingFieldByField(testTipoUsuario);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoUsuario() throws Exception {
        int databaseSizeBeforeUpdate = tipoUsuarioRepository.findAll().size();

        // Create the TipoUsuario

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipoUsuarioMockMvc.perform(put("/api/tipo-usuarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoUsuario)))
            .andExpect(status().isCreated());

        // Validate the TipoUsuario in the database
        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipoUsuario() throws Exception {
        // Initialize the database
        tipoUsuarioRepository.saveAndFlush(tipoUsuario);
        tipoUsuarioSearchRepository.save(tipoUsuario);
        int databaseSizeBeforeDelete = tipoUsuarioRepository.findAll().size();

        // Get the tipoUsuario
        restTipoUsuarioMockMvc.perform(delete("/api/tipo-usuarios/{id}", tipoUsuario.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipoUsuarioExistsInEs = tipoUsuarioSearchRepository.exists(tipoUsuario.getId());
        assertThat(tipoUsuarioExistsInEs).isFalse();

        // Validate the database is empty
        List<TipoUsuario> tipoUsuarioList = tipoUsuarioRepository.findAll();
        assertThat(tipoUsuarioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipoUsuario() throws Exception {
        // Initialize the database
        tipoUsuarioRepository.saveAndFlush(tipoUsuario);
        tipoUsuarioSearchRepository.save(tipoUsuario);

        // Search the tipoUsuario
        restTipoUsuarioMockMvc.perform(get("/api/_search/tipo-usuarios?query=id:" + tipoUsuario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoUsuario.getId().intValue())))
            .andExpect(jsonPath("$.[*].varNomTpusuario").value(hasItem(DEFAULT_VAR_NOM_TPUSUARIO.toString())))
            .andExpect(jsonPath("$.[*].varDescTpusuario").value(hasItem(DEFAULT_VAR_DESC_TPUSUARIO.toString())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoUsuario.class);
        TipoUsuario tipoUsuario1 = new TipoUsuario();
        tipoUsuario1.setId(1L);
        TipoUsuario tipoUsuario2 = new TipoUsuario();
        tipoUsuario2.setId(tipoUsuario1.getId());
        assertThat(tipoUsuario1).isEqualTo(tipoUsuario2);
        tipoUsuario2.setId(2L);
        assertThat(tipoUsuario1).isNotEqualTo(tipoUsuario2);
        tipoUsuario1.setId(null);
        assertThat(tipoUsuario1).isNotEqualTo(tipoUsuario2);
    }
}
