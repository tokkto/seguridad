package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.MenuPerfil;
import pe.gob.trabajo.repository.MenuPerfilRepository;
import pe.gob.trabajo.repository.search.MenuPerfilSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MenuPerfilResource REST controller.
 *
 * @see MenuPerfilResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class MenuPerfilResourceIntTest {

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private MenuPerfilRepository menuPerfilRepository;

    @Autowired
    private MenuPerfilSearchRepository menuPerfilSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMenuPerfilMockMvc;

    private MenuPerfil menuPerfil;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MenuPerfilResource menuPerfilResource = new MenuPerfilResource(menuPerfilRepository, menuPerfilSearchRepository);
        this.restMenuPerfilMockMvc = MockMvcBuilders.standaloneSetup(menuPerfilResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MenuPerfil createEntity(EntityManager em) {
        MenuPerfil menuPerfil = new MenuPerfil()
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return menuPerfil;
    }

    @Before
    public void initTest() {
        menuPerfilSearchRepository.deleteAll();
        menuPerfil = createEntity(em);
    }

    @Test
    @Transactional
    public void createMenuPerfil() throws Exception {
        int databaseSizeBeforeCreate = menuPerfilRepository.findAll().size();

        // Create the MenuPerfil
        restMenuPerfilMockMvc.perform(post("/api/menu-perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuPerfil)))
            .andExpect(status().isCreated());

        // Validate the MenuPerfil in the database
        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeCreate + 1);
        MenuPerfil testMenuPerfil = menuPerfilList.get(menuPerfilList.size() - 1);
        assertThat(testMenuPerfil.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testMenuPerfil.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testMenuPerfil.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the MenuPerfil in Elasticsearch
        MenuPerfil menuPerfilEs = menuPerfilSearchRepository.findOne(testMenuPerfil.getId());
        assertThat(menuPerfilEs).isEqualToComparingFieldByField(testMenuPerfil);
    }

    @Test
    @Transactional
    public void createMenuPerfilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = menuPerfilRepository.findAll().size();

        // Create the MenuPerfil with an existing ID
        menuPerfil.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMenuPerfilMockMvc.perform(post("/api/menu-perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuPerfil)))
            .andExpect(status().isBadRequest());

        // Validate the MenuPerfil in the database
        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuPerfilRepository.findAll().size();
        // set the field null
        menuPerfil.setVarUsuarioLog(null);

        // Create the MenuPerfil, which fails.

        restMenuPerfilMockMvc.perform(post("/api/menu-perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuPerfil)))
            .andExpect(status().isBadRequest());

        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuPerfilRepository.findAll().size();
        // set the field null
        menuPerfil.setDatFechaLog(null);

        // Create the MenuPerfil, which fails.

        restMenuPerfilMockMvc.perform(post("/api/menu-perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuPerfil)))
            .andExpect(status().isBadRequest());

        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = menuPerfilRepository.findAll().size();
        // set the field null
        menuPerfil.setNumEliminar(null);

        // Create the MenuPerfil, which fails.

        restMenuPerfilMockMvc.perform(post("/api/menu-perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuPerfil)))
            .andExpect(status().isBadRequest());

        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMenuPerfils() throws Exception {
        // Initialize the database
        menuPerfilRepository.saveAndFlush(menuPerfil);

        // Get all the menuPerfilList
        restMenuPerfilMockMvc.perform(get("/api/menu-perfils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menuPerfil.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getMenuPerfil() throws Exception {
        // Initialize the database
        menuPerfilRepository.saveAndFlush(menuPerfil);

        // Get the menuPerfil
        restMenuPerfilMockMvc.perform(get("/api/menu-perfils/{id}", menuPerfil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(menuPerfil.getId().intValue()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingMenuPerfil() throws Exception {
        // Get the menuPerfil
        restMenuPerfilMockMvc.perform(get("/api/menu-perfils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMenuPerfil() throws Exception {
        // Initialize the database
        menuPerfilRepository.saveAndFlush(menuPerfil);
        menuPerfilSearchRepository.save(menuPerfil);
        int databaseSizeBeforeUpdate = menuPerfilRepository.findAll().size();

        // Update the menuPerfil
        MenuPerfil updatedMenuPerfil = menuPerfilRepository.findOne(menuPerfil.getId());
        updatedMenuPerfil
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restMenuPerfilMockMvc.perform(put("/api/menu-perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMenuPerfil)))
            .andExpect(status().isOk());

        // Validate the MenuPerfil in the database
        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeUpdate);
        MenuPerfil testMenuPerfil = menuPerfilList.get(menuPerfilList.size() - 1);
        assertThat(testMenuPerfil.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testMenuPerfil.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testMenuPerfil.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the MenuPerfil in Elasticsearch
        MenuPerfil menuPerfilEs = menuPerfilSearchRepository.findOne(testMenuPerfil.getId());
        assertThat(menuPerfilEs).isEqualToComparingFieldByField(testMenuPerfil);
    }

    @Test
    @Transactional
    public void updateNonExistingMenuPerfil() throws Exception {
        int databaseSizeBeforeUpdate = menuPerfilRepository.findAll().size();

        // Create the MenuPerfil

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMenuPerfilMockMvc.perform(put("/api/menu-perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(menuPerfil)))
            .andExpect(status().isCreated());

        // Validate the MenuPerfil in the database
        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMenuPerfil() throws Exception {
        // Initialize the database
        menuPerfilRepository.saveAndFlush(menuPerfil);
        menuPerfilSearchRepository.save(menuPerfil);
        int databaseSizeBeforeDelete = menuPerfilRepository.findAll().size();

        // Get the menuPerfil
        restMenuPerfilMockMvc.perform(delete("/api/menu-perfils/{id}", menuPerfil.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean menuPerfilExistsInEs = menuPerfilSearchRepository.exists(menuPerfil.getId());
        assertThat(menuPerfilExistsInEs).isFalse();

        // Validate the database is empty
        List<MenuPerfil> menuPerfilList = menuPerfilRepository.findAll();
        assertThat(menuPerfilList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMenuPerfil() throws Exception {
        // Initialize the database
        menuPerfilRepository.saveAndFlush(menuPerfil);
        menuPerfilSearchRepository.save(menuPerfil);

        // Search the menuPerfil
        restMenuPerfilMockMvc.perform(get("/api/_search/menu-perfils?query=id:" + menuPerfil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(menuPerfil.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MenuPerfil.class);
        MenuPerfil menuPerfil1 = new MenuPerfil();
        menuPerfil1.setId(1L);
        MenuPerfil menuPerfil2 = new MenuPerfil();
        menuPerfil2.setId(menuPerfil1.getId());
        assertThat(menuPerfil1).isEqualTo(menuPerfil2);
        menuPerfil2.setId(2L);
        assertThat(menuPerfil1).isNotEqualTo(menuPerfil2);
        menuPerfil1.setId(null);
        assertThat(menuPerfil1).isNotEqualTo(menuPerfil2);
    }
}
