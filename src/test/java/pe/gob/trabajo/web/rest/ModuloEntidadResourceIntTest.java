package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.SeguridadApp;

import pe.gob.trabajo.domain.ModuloEntidad;
import pe.gob.trabajo.repository.ModuloEntidadRepository;
import pe.gob.trabajo.repository.search.ModuloEntidadSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModuloEntidadResource REST controller.
 *
 * @see ModuloEntidadResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeguridadApp.class)
public class ModuloEntidadResourceIntTest {

    private static final String DEFAULT_VAR_USUARIO_LOG = "AAAAAAAAAA";
    private static final String UPDATED_VAR_USUARIO_LOG = "BBBBBBBBBB";

    private static final Instant DEFAULT_DAT_FECHA_LOG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAT_FECHA_LOG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUM_ELIMINAR = 0;
    private static final Integer UPDATED_NUM_ELIMINAR = 1;

    @Autowired
    private ModuloEntidadRepository moduloEntidadRepository;

    @Autowired
    private ModuloEntidadSearchRepository moduloEntidadSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModuloEntidadMockMvc;

    private ModuloEntidad moduloEntidad;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModuloEntidadResource moduloEntidadResource = new ModuloEntidadResource(moduloEntidadRepository, moduloEntidadSearchRepository);
        this.restModuloEntidadMockMvc = MockMvcBuilders.standaloneSetup(moduloEntidadResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModuloEntidad createEntity(EntityManager em) {
        ModuloEntidad moduloEntidad = new ModuloEntidad()
            .varUsuarioLog(DEFAULT_VAR_USUARIO_LOG)
            .datFechaLog(DEFAULT_DAT_FECHA_LOG)
            .numEliminar(DEFAULT_NUM_ELIMINAR);
        return moduloEntidad;
    }

    @Before
    public void initTest() {
        moduloEntidadSearchRepository.deleteAll();
        moduloEntidad = createEntity(em);
    }

    @Test
    @Transactional
    public void createModuloEntidad() throws Exception {
        int databaseSizeBeforeCreate = moduloEntidadRepository.findAll().size();

        // Create the ModuloEntidad
        restModuloEntidadMockMvc.perform(post("/api/modulo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(moduloEntidad)))
            .andExpect(status().isCreated());

        // Validate the ModuloEntidad in the database
        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeCreate + 1);
        ModuloEntidad testModuloEntidad = moduloEntidadList.get(moduloEntidadList.size() - 1);
        assertThat(testModuloEntidad.getVarUsuarioLog()).isEqualTo(DEFAULT_VAR_USUARIO_LOG);
        assertThat(testModuloEntidad.getDatFechaLog()).isEqualTo(DEFAULT_DAT_FECHA_LOG);
        assertThat(testModuloEntidad.getNumEliminar()).isEqualTo(DEFAULT_NUM_ELIMINAR);

        // Validate the ModuloEntidad in Elasticsearch
        ModuloEntidad moduloEntidadEs = moduloEntidadSearchRepository.findOne(testModuloEntidad.getId());
        assertThat(moduloEntidadEs).isEqualToComparingFieldByField(testModuloEntidad);
    }

    @Test
    @Transactional
    public void createModuloEntidadWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = moduloEntidadRepository.findAll().size();

        // Create the ModuloEntidad with an existing ID
        moduloEntidad.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModuloEntidadMockMvc.perform(post("/api/modulo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(moduloEntidad)))
            .andExpect(status().isBadRequest());

        // Validate the ModuloEntidad in the database
        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVarUsuarioLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = moduloEntidadRepository.findAll().size();
        // set the field null
        moduloEntidad.setVarUsuarioLog(null);

        // Create the ModuloEntidad, which fails.

        restModuloEntidadMockMvc.perform(post("/api/modulo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(moduloEntidad)))
            .andExpect(status().isBadRequest());

        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatFechaLogIsRequired() throws Exception {
        int databaseSizeBeforeTest = moduloEntidadRepository.findAll().size();
        // set the field null
        moduloEntidad.setDatFechaLog(null);

        // Create the ModuloEntidad, which fails.

        restModuloEntidadMockMvc.perform(post("/api/modulo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(moduloEntidad)))
            .andExpect(status().isBadRequest());

        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumEliminarIsRequired() throws Exception {
        int databaseSizeBeforeTest = moduloEntidadRepository.findAll().size();
        // set the field null
        moduloEntidad.setNumEliminar(null);

        // Create the ModuloEntidad, which fails.

        restModuloEntidadMockMvc.perform(post("/api/modulo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(moduloEntidad)))
            .andExpect(status().isBadRequest());

        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModuloEntidads() throws Exception {
        // Initialize the database
        moduloEntidadRepository.saveAndFlush(moduloEntidad);

        // Get all the moduloEntidadList
        restModuloEntidadMockMvc.perform(get("/api/modulo-entidads?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(moduloEntidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void getModuloEntidad() throws Exception {
        // Initialize the database
        moduloEntidadRepository.saveAndFlush(moduloEntidad);

        // Get the moduloEntidad
        restModuloEntidadMockMvc.perform(get("/api/modulo-entidads/{id}", moduloEntidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(moduloEntidad.getId().intValue()))
            .andExpect(jsonPath("$.varUsuarioLog").value(DEFAULT_VAR_USUARIO_LOG.toString()))
            .andExpect(jsonPath("$.datFechaLog").value(DEFAULT_DAT_FECHA_LOG.toString()))
            .andExpect(jsonPath("$.numEliminar").value(DEFAULT_NUM_ELIMINAR));
    }

    @Test
    @Transactional
    public void getNonExistingModuloEntidad() throws Exception {
        // Get the moduloEntidad
        restModuloEntidadMockMvc.perform(get("/api/modulo-entidads/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModuloEntidad() throws Exception {
        // Initialize the database
        moduloEntidadRepository.saveAndFlush(moduloEntidad);
        moduloEntidadSearchRepository.save(moduloEntidad);
        int databaseSizeBeforeUpdate = moduloEntidadRepository.findAll().size();

        // Update the moduloEntidad
        ModuloEntidad updatedModuloEntidad = moduloEntidadRepository.findOne(moduloEntidad.getId());
        updatedModuloEntidad
            .varUsuarioLog(UPDATED_VAR_USUARIO_LOG)
            .datFechaLog(UPDATED_DAT_FECHA_LOG)
            .numEliminar(UPDATED_NUM_ELIMINAR);

        restModuloEntidadMockMvc.perform(put("/api/modulo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedModuloEntidad)))
            .andExpect(status().isOk());

        // Validate the ModuloEntidad in the database
        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeUpdate);
        ModuloEntidad testModuloEntidad = moduloEntidadList.get(moduloEntidadList.size() - 1);
        assertThat(testModuloEntidad.getVarUsuarioLog()).isEqualTo(UPDATED_VAR_USUARIO_LOG);
        assertThat(testModuloEntidad.getDatFechaLog()).isEqualTo(UPDATED_DAT_FECHA_LOG);
        assertThat(testModuloEntidad.getNumEliminar()).isEqualTo(UPDATED_NUM_ELIMINAR);

        // Validate the ModuloEntidad in Elasticsearch
        ModuloEntidad moduloEntidadEs = moduloEntidadSearchRepository.findOne(testModuloEntidad.getId());
        assertThat(moduloEntidadEs).isEqualToComparingFieldByField(testModuloEntidad);
    }

    @Test
    @Transactional
    public void updateNonExistingModuloEntidad() throws Exception {
        int databaseSizeBeforeUpdate = moduloEntidadRepository.findAll().size();

        // Create the ModuloEntidad

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restModuloEntidadMockMvc.perform(put("/api/modulo-entidads")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(moduloEntidad)))
            .andExpect(status().isCreated());

        // Validate the ModuloEntidad in the database
        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteModuloEntidad() throws Exception {
        // Initialize the database
        moduloEntidadRepository.saveAndFlush(moduloEntidad);
        moduloEntidadSearchRepository.save(moduloEntidad);
        int databaseSizeBeforeDelete = moduloEntidadRepository.findAll().size();

        // Get the moduloEntidad
        restModuloEntidadMockMvc.perform(delete("/api/modulo-entidads/{id}", moduloEntidad.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean moduloEntidadExistsInEs = moduloEntidadSearchRepository.exists(moduloEntidad.getId());
        assertThat(moduloEntidadExistsInEs).isFalse();

        // Validate the database is empty
        List<ModuloEntidad> moduloEntidadList = moduloEntidadRepository.findAll();
        assertThat(moduloEntidadList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchModuloEntidad() throws Exception {
        // Initialize the database
        moduloEntidadRepository.saveAndFlush(moduloEntidad);
        moduloEntidadSearchRepository.save(moduloEntidad);

        // Search the moduloEntidad
        restModuloEntidadMockMvc.perform(get("/api/_search/modulo-entidads?query=id:" + moduloEntidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(moduloEntidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].varUsuarioLog").value(hasItem(DEFAULT_VAR_USUARIO_LOG.toString())))
            .andExpect(jsonPath("$.[*].datFechaLog").value(hasItem(DEFAULT_DAT_FECHA_LOG.toString())))
            .andExpect(jsonPath("$.[*].numEliminar").value(hasItem(DEFAULT_NUM_ELIMINAR)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModuloEntidad.class);
        ModuloEntidad moduloEntidad1 = new ModuloEntidad();
        moduloEntidad1.setId(1L);
        ModuloEntidad moduloEntidad2 = new ModuloEntidad();
        moduloEntidad2.setId(moduloEntidad1.getId());
        assertThat(moduloEntidad1).isEqualTo(moduloEntidad2);
        moduloEntidad2.setId(2L);
        assertThat(moduloEntidad1).isNotEqualTo(moduloEntidad2);
        moduloEntidad1.setId(null);
        assertThat(moduloEntidad1).isNotEqualTo(moduloEntidad2);
    }
}
